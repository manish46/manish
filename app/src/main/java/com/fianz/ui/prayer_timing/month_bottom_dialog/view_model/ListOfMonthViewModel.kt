package com.fianz.ui.prayer_timing.month_bottom_dialog.view_model

import android.app.Application
import android.app.Dialog
import androidx.lifecycle.AndroidViewModel
import com.fianz.interfaces.SendSingleValueInterface
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.model.PrayerMonthWithNumberModel
import com.fianz.ui.prayer_timing.month_bottom_dialog.adapter.ListOfMonthAdapter

class ListOfMonthViewModel(application: Application) : AndroidViewModel(application) {

    var listOfMonthAdapter: ListOfMonthAdapter = ListOfMonthAdapter()

    fun storeDataInAdapter(
        listOfMonth: ArrayList<PrayerMonthWithNumberModel>,
        sendSingleValueInterface: SendSingleValueInterface,
        dialog: Dialog?
    ) {
        listOfMonthAdapter.addList(listOfMonth, object : SendThreeValueInterface {
            override fun actionFire(position: Any, firstValue: Any, secondValue: Any) {
                listOfMonthAdapter.refreshData(position as Int, firstValue as Int)
                dialog?.dismiss()
                sendSingleValueInterface.sendPosition(position)
            }
        })
    }

    fun seeListOfData(): ListOfMonthAdapter {
        return listOfMonthAdapter
    }
}