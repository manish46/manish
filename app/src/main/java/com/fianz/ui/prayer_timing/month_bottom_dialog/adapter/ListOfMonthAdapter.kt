package com.fianz.ui.prayer_timing.month_bottom_dialog.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowListOfMonthDialogBinding
import com.fianz.interfaces.SendSingleValueInterface
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.model.PrayerMonthWithNumberModel

class ListOfMonthAdapter : RecyclerView.Adapter<ListOfMonthAdapter.ViewHolder>() {

    var listOfMonth = ArrayList<PrayerMonthWithNumberModel>()
    lateinit var sendThreeValueInterface: SendThreeValueInterface

    companion object {
        var currentPos = 0
        var prePos = 0
    }

    fun addList(
        listOfMonth: ArrayList<PrayerMonthWithNumberModel>,
        sendThreeValueInterface: SendThreeValueInterface
    ) {
        this.listOfMonth = listOfMonth
        this.sendThreeValueInterface = sendThreeValueInterface
    }

    fun refreshData(currentPos: Int, prePos: Int) {
        listOfMonth[prePos].selectedFlag = false
        listOfMonth[currentPos].selectedFlag = true

        notifyDataSetChanged()
    }

    class ViewHolder(
        val binding: RowListOfMonthDialogBinding,
        var sendThreeValueInterface: SendThreeValueInterface
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: PrayerMonthWithNumberModel) {
            binding.pryerMonthModel = data
            binding.executePendingBindings()

            if (!binding.pryerMonthModel!!.selectedFlag) {
                binding.imgTick.visibility = View.INVISIBLE
            } else {
                binding.imgTick.visibility = View.VISIBLE
            }

            binding.txtTitleText.setOnClickListener {
                prePos = currentPos
                currentPos = layoutPosition

                sendThreeValueInterface.actionFire(currentPos, prePos, false)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: RowListOfMonthDialogBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_list_of_month_dialog,
            parent,
            false
        )

        return ViewHolder(binding, sendThreeValueInterface)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listOfMonth[position])
    }

    override fun getItemCount(): Int {
        return listOfMonth.size
    }
}