package com.fianz.ui.prayer_timing.bottom_dialog

//import com.fianz.ui.prayer_timing.bottom_dialog.factory.LocationBottomFactory
import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.fianz.R
import com.fianz.databinding.DropDownLocationOptionBinding
import com.fianz.interfaces.SendSingleValueInterface
import com.fianz.model.prayer_model.CentresIdAndTitle
import com.fianz.ui.prayer_timing.bottom_dialog.view_model.CentreBottomFragViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class CentreBottomFrag(
    var centresWithId: ArrayList<CentresIdAndTitle>,
    var txtPTLocation: TextView,
    var sendSingleValueInterface: SendSingleValueInterface
) : BottomSheetDialogFragment(),
    View.OnClickListener {

    lateinit var binding: DropDownLocationOptionBinding
    private lateinit var locationVM: CentreBottomFragViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(
                LayoutInflater.from(requireActivity()),
                R.layout.drop_down_location_option,
                container,
                false
            )
        init()
        setListener()
        observer()
        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = BottomSheetDialog(requireActivity())
        dialog.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            val parentLayout =
                bottomSheetDialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
            parentLayout?.let {
                val displayMetrics: DisplayMetrics = requireActivity().resources.displayMetrics
                val height: Int = displayMetrics.heightPixels
                val maxHeight = (height * 0.75).toInt()

                val behavior = BottomSheetBehavior.from(it)
                val layoutPar = it.layoutParams
                layoutPar.height = maxHeight
                it.layoutParams = layoutPar
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
        return dialog

    }

    private fun init() {
        locationVM =
            ViewModelProvider(
                requireActivity()
            ).get(CentreBottomFragViewModel::class.java)
        locationVM.storeLocationData(centresWithId)
        binding.locationVM = locationVM

        binding.edtAHSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                locationVM.searchData(s.toString())
            }
        })
    }

    private fun setListener() {
        binding.crdPTDOk.setOnClickListener(this)
        binding.cvCancel.setOnClickListener(this)
    }

    fun observer() {
        locationVM.selectedItem.observe(this, object : Observer<Int> {
            override fun onChanged(t: Int?) {
                txtPTLocation.text = centresWithId[t!!].title
                sendSingleValueInterface.sendPosition(centresWithId[t].id)
            }
        })
    }

    override fun onStart() {
        super.onStart()
        dialog?.show()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.crdPTDOk -> {
                locationVM.getSendingId()

                dialog?.dismiss()
            }
            R.id.cvCancel -> {
                locationVM.removeCurrentSelection()
                dialog?.dismiss()
            }
        }
    }
}