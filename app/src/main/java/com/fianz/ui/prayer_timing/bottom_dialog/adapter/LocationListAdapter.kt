package com.fianz.ui.prayer_timing.bottom_dialog.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowLocationOptionBinding
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.model.prayer_model.CentresIdAndTitle

class LocationListAdapter : RecyclerView.Adapter<LocationListAdapter.ViewHolder>() {
    private var centresWithId: ArrayList<CentresIdAndTitle> = arrayListOf()
    private lateinit var sendThreeValueInterface: SendThreeValueInterface

    companion object {
        private var previousPos = 0
        private var currentPos = 0
    }

    fun addList(
        centresWithId: ArrayList<CentresIdAndTitle>,
        sendThreeValueInterface: SendThreeValueInterface
    ) {
        this.centresWithId = centresWithId
        this.sendThreeValueInterface = sendThreeValueInterface
    }

    fun filterData(tempList:ArrayList<CentresIdAndTitle>){
        this.centresWithId = tempList
        notifyDataSetChanged()
    }

    fun refreshAdapter(previousPos: Int, currentPos: Int) {
        centresWithId[previousPos].flag = false
        centresWithId[currentPos].flag = true
        notifyItemChanged(previousPos)
        notifyItemChanged(currentPos)
    }

    class ViewHolder(
        val binding: RowLocationOptionBinding,
        private val sendThreeValueInterface: SendThreeValueInterface
    ) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        fun bind(data: CentresIdAndTitle) {
            binding.locationM = data
            binding.executePendingBindings()

            init(binding)
            setListener(binding)
        }

        private fun init(binding: RowLocationOptionBinding) {
            if (binding.locationM!!.flag) {
                currentPos = layoutPosition
                binding.imgRLOTick.visibility = View.VISIBLE
            } else {
                binding.imgRLOTick.visibility = View.INVISIBLE
            }
        }

        private fun setListener(binding: RowLocationOptionBinding) {
            binding.lnrRLOParent.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.lnrRLOParent -> {
                    if (!binding.locationM!!.flag) {
                        previousPos = currentPos
                        currentPos = layoutPosition

                        sendThreeValueInterface.actionFire(
                            binding.locationM!!.title, previousPos,
                            currentPos
                        )
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: RowLocationOptionBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_location_option,
            parent,
            false
        )
        return ViewHolder(view, sendThreeValueInterface)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(centresWithId[position])
    }

    override fun getItemCount(): Int {
        return centresWithId.size
    }
}