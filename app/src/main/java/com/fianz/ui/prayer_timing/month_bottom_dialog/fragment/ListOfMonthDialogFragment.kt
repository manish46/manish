package com.fianz.ui.prayer_timing.month_bottom_dialog.fragment

import android.app.Dialog
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.fianz.R
import com.fianz.databinding.FragmentListOfMonthDialogBinding
import com.fianz.interfaces.SendSingleValueInterface
import com.fianz.model.PrayerMonthWithNumberModel
import com.fianz.ui.prayer_timing.month_bottom_dialog.view_model.ListOfMonthViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class ListOfMonthDialogFragment(
    var listOfMonth: ArrayList<PrayerMonthWithNumberModel>,
    var sendSingleValueInterface: SendSingleValueInterface
) : BottomSheetDialogFragment() {

    lateinit var binding: FragmentListOfMonthDialogBinding
    lateinit var listOfMonthDialogVM: ListOfMonthViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(requireActivity()),
            R.layout.fragment_list_of_month_dialog,
            container,
            false
        )

        init()
        setListener()
        return binding.root
    }

    fun init() {
        listOfMonthDialogVM = ViewModelProvider(this).get(ListOfMonthViewModel::class.java)
        listOfMonthDialogVM.storeDataInAdapter(listOfMonth, sendSingleValueInterface, dialog)
        binding.listOfMonthDialogVM = listOfMonthDialogVM
    }

    fun setListener() {

    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = BottomSheetDialog(requireActivity())
        dialog.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            val parentLayout =
                bottomSheetDialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
            parentLayout?.let {
                val displayMetrics: DisplayMetrics = requireActivity().resources.displayMetrics
                val height: Int = displayMetrics.heightPixels
                val maxHeight = (height * 0.55).toInt()

                val behavior = BottomSheetBehavior.from(it)
                val layoutPar = it.layoutParams
                layoutPar.height = maxHeight
                it.layoutParams = layoutPar
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
        return dialog

    }
}