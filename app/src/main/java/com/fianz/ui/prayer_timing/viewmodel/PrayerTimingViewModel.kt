package com.fianz.ui.prayer_timing.viewmodel

import android.app.Application
import android.content.Context
import android.content.Intent
import android.location.Location
import android.net.Uri
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.api.repositories.PrayerTimingRepository
import com.fianz.api.web_url.ApiKey
import com.fianz.interfaces.AllApiResponse
import com.fianz.interfaces.SendSingleValueInterface
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.model.PrayerMonthWithNumberModel
import com.fianz.model.WeatherInPrayerTimeModel
import com.fianz.model.get_centers_list.GetCenterListResponse
import com.fianz.model.prayer_model.CentresIdAndTitle
import com.fianz.model.prayer_model.PrayerTimesItem
import com.fianz.model.prayer_model.PrayerTimingResponse
import com.fianz.ui.home.HomeActivity
import com.fianz.ui.prayer_timing.adapter.PrayerTimeMonthAdapter
import com.fianz.ui.prayer_timing.adapter.WeatherInPrayerTimeAdapter
import com.fianz.ui.prayer_timing.adapter.YearInPrayerTimeAdapter
import com.fianz.ui.prayer_timing.bottom_dialog.CentreBottomFrag
import com.fianz.ui.prayer_timing.month_bottom_dialog.fragment.ListOfMonthDialogFragment
import com.fianz.ui.prayer_timing_dialog.bottom_sheet_frag_dialog.PrayerTimingFragmentDialog
import com.fianz.utils.CommonUtils
import com.fianz.utils.ProgressLoader
import com.google.android.material.tabs.TabLayout
import retrofit2.Response
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class PrayerTimingViewModel(application: Application) : AndroidViewModel(application) {

    private lateinit var listOfWeathers: ArrayList<WeatherInPrayerTimeModel>

    private lateinit var listOfMonth: ArrayList<PrayerMonthWithNumberModel>
    private lateinit var listOfDropMonth: ArrayList<PrayerMonthWithNumberModel>

    private var weatherInPrayerTimeAdapter: WeatherInPrayerTimeAdapter =
        WeatherInPrayerTimeAdapter()

    private var yearInPrayerTimeAdapter: YearInPrayerTimeAdapter = YearInPrayerTimeAdapter()
    private var context = application.resources
    private lateinit var setCentresWithId: ArrayList<CentresIdAndTitle>
    private var prayerTimeMonthAdapter: PrayerTimeMonthAdapter = PrayerTimeMonthAdapter()

    private var centerId = 2
    private var selectedMonthNumber = 1
    private var storeSelectedMonth: ArrayList<Int> = arrayListOf()
    private var prayerTimes: ArrayList<PrayerTimesItem?>? = null
    var getSelecteMonthItme = context.getString(R.string.jan)

    lateinit var listOfMonthFrag: ListOfMonthDialogFragment

    private lateinit var txtPTLocation: TextView
    private lateinit var imgBg: ImageView
    private lateinit var imgIcon: AppCompatImageView
    private lateinit var txtPTTitle: AppCompatTextView
    private lateinit var txtPTTime: AppCompatTextView
    private lateinit var txtPTAmPm: AppCompatTextView

    var scrollInAdapter: MutableLiveData<Int> = MutableLiveData()
    lateinit var listOfImages: ArrayList<Int>

    fun setValues(
        txtPTLocation: TextView,
        imgBg: ImageView,
        imgIcon: AppCompatImageView,
        txtPTTitle: AppCompatTextView,
        txtPTTime: AppCompatTextView,
        txtPTAmPm: AppCompatTextView
    ) {
        this.txtPTLocation = txtPTLocation
        this.imgBg = imgBg
        this.imgIcon = imgIcon
        this.txtPTTitle = txtPTTitle
        this.txtPTTime = txtPTTime
        this.txtPTAmPm = txtPTAmPm

        listOfImages = arrayListOf()
        listOfImages.add(R.drawable.fajr)
        listOfImages.add(R.drawable.sunrise)
        listOfImages.add(R.drawable.zohr)
        listOfImages.add(R.drawable.asr)
        listOfImages.add(R.drawable.maghrib)
        listOfImages.add(R.drawable.isha)

    }

    fun showListOfCentre(view: View, txtNoData: TextView, txtPTLocation: TextView) {
        val showDialog =
            CentreBottomFrag(setCentresWithId, txtPTLocation, object : SendSingleValueInterface {
                override fun sendPosition(position: Any) {
                    centerId = position as Int
                    callWeeklyPrayerTimingApi(view.context, txtNoData, true)
                    prayerTimes = arrayListOf()
                }
            })
        showDialog.show((view.context as HomeActivity).supportFragmentManager, "")
    }

    fun storeMonthList(context: Context, txtPTNoData: TextView) {
        listOfMonth = arrayListOf()

        listOfMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.jan), 1, true))
        listOfMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.feb), 2, false))
        listOfMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.mar), 3, false))
        listOfMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.apr), 4, false))
        listOfMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.may), 5, false))
        listOfMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.jun), 6, false))
        listOfMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.jul), 7, false))
        listOfMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.aug), 8, false))
        listOfMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.sep), 9, false))
        listOfMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.oct), 10, false))
        listOfMonth.add(PrayerMonthWithNumberModel("Nav", 11, false))
        listOfMonth.add(PrayerMonthWithNumberModel("Dec", 12, false))

        listOfDropMonth = arrayListOf()

        listOfDropMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.jan), 1, true))
        listOfDropMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.feb), 2, false))
        listOfDropMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.mar), 3, false))
        listOfDropMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.apr), 4, false))
        listOfDropMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.may), 5, false))
        listOfDropMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.jun), 6, false))
        listOfDropMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.jul), 7, false))
        listOfDropMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.aug), 8, false))
        listOfDropMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.sep), 9, false))
        listOfDropMonth.add(PrayerMonthWithNumberModel(context.getString(R.string.oct), 10, false))
        listOfDropMonth.add(PrayerMonthWithNumberModel("Nav", 11, false))
        listOfDropMonth.add(PrayerMonthWithNumberModel("Dec", 12, false))

        setYearData(listOfMonth, context, txtPTNoData)
        storeSelectedMonth.add(listOfMonth[0].monthNumber)
    }

    fun tabCurrentSelectedTab(
        tablayout: TabLayout,
        relCalenderView: RelativeLayout,
        tvDate: AppCompatTextView,
        ivCalendar: AppCompatImageView,
        relPTZoomView: RelativeLayout,
        rvDates: RecyclerView,
        txtPTNoData: TextView
    ) {
        tablayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                when (tab?.position) {
                    0 -> {
                        relCalenderView.isClickable = false
                        relCalenderView.isFocusable = false

                        tvDate.visibility = View.GONE
                        ivCalendar.visibility = View.GONE
                        relPTZoomView.visibility = View.GONE

                        rvDates.visibility = View.GONE
                        callWeeklyPrayerTimingApi(
                            tablayout.context,
                            txtPTNoData,
                            true
                        )
                    }
                    1 -> {
                        relCalenderView.isClickable = true
                        relCalenderView.isFocusable = true

                        tvDate.visibility = View.VISIBLE
                        ivCalendar.visibility = View.VISIBLE
                        relPTZoomView.visibility = View.VISIBLE

                        rvDates.visibility = View.GONE

                        callMonthlyPrayerTimingApi(
                            tablayout.context,
                            relCalenderView,
                            txtPTNoData,
                            tvDate,
                        )
                    }
                    2 -> {
                        relCalenderView.isClickable = false
                        relCalenderView.isFocusable = false

                        tvDate.visibility = View.VISIBLE
                        ivCalendar.visibility = View.VISIBLE

                        rvDates.visibility = View.VISIBLE
                        relPTZoomView.visibility = View.GONE
                        callYearlyPrayerTimingApi(
                            tablayout.context,
                            txtPTNoData,
                        )
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}
            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })
    }

    fun nestedScrollView(nsParentView: NestedScrollView, context: Context) {
        nsParentView.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { _, _, scrollY, _, _ ->
            val multiplyY = scrollY * 2
            val divY = (multiplyY / 100).toFloat()
            (context as HomeActivity).binding.viewCover.alpha = divY / 10
            if (context.binding.viewCover.alpha > 0.6f) {
                (context as HomeActivity).binding.txtActionTitle.setTextColor(
                    context.getColor(
                        R.color.black
                    )
                )
            } else {
                (context).binding.txtActionTitle.setTextColor(
                    context.getColor(
                        R.color.white
                    )
                )
            }
        })
    }

    private fun setYearData(
        listOfMonth: ArrayList<PrayerMonthWithNumberModel>,
        context: Context,
        txtPTNoData: TextView
    ) {
        yearInPrayerTimeAdapter.addData(listOfMonth, object : SendThreeValueInterface {
            override fun actionFire(position: Any, firstValue: Any, secondValue: Any) {
                if (storeSelectedMonth.size != 0) {
                    storeSelectedMonth.forEachIndexed { i, t ->
                        if (listOfMonth[position as Int].monthNumber == t) {
                            storeSelectedMonth.removeAt(i)
                            callYearlyPrayerTimingApi(context, txtPTNoData)
                            return
                        }
                    }
                }
                storeSelectedMonth.add(listOfMonth[position as Int].monthNumber)
                callYearlyPrayerTimingApi(context, txtPTNoData)
            }
        })
        yearInPrayerTimeAdapter.notifyDataSetChanged()
    }

    fun seeYearData(): YearInPrayerTimeAdapter {
        return yearInPrayerTimeAdapter
    }

    fun openLocation(view: View) {
        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse("http://maps.google.com/maps?saddr=23.0225,72.5714&daddr=41.32042328818911  174.79395298520288")
        )
        view.context.startActivity(intent)
    }

    fun callCentreList(
        context: Context,
        txtNoData: TextView,
        latitude: Double,
        longitude: Double,
        flag: Boolean
    ) {
        if (CommonUtils.isOnlinne(context)) {
            ProgressLoader.showLoader(context)
            PrayerTimingRepository(context, object : AllApiResponse {
                override fun sendResponse(response: Response<*>?) {
                    setCentresWithId = arrayListOf()
                    try {
                        if (response != null) {
                            val mainResponse = response.body() as GetCenterListResponse
                            val centerList = mainResponse.data?.centres
                            if (centerList?.size != 0) {
                                for (i in 0 until centerList?.size!!) {
                                    val subCenterList = centerList[i]?.centers
                                    if (subCenterList?.size != 0) {
                                        subCenterList!!.forEachIndexed { j, getCentersCentersItem ->

                                            var distance = 0f
                                            val crntLocation = Location("crntlocation")
                                            crntLocation.latitude = latitude
                                            crntLocation.longitude = longitude

                                            val newLocation = Location("newlocation")
                                            newLocation.latitude =
                                                subCenterList[j]?.latitude!!.toDouble()
                                            newLocation.longitude =
                                                subCenterList[j]?.longitude!!.toDouble()

                                            distance = crntLocation.distanceTo(newLocation) / 1000

                                            setCentresWithId.add(
                                                CentresIdAndTitle(
                                                    subCenterList[j]?.id!!,
                                                    subCenterList[j]?.title!!,
                                                    distance.toDouble()
                                                )
                                            )
                                        }
                                    }
                                }
                                if (flag) {
                                    setCentresWithId.sortByDescending { list -> list.distanceKM }

                                    Log.e("data", setCentresWithId.toString())
                                    Collections.reverse(setCentresWithId)
                                }
                                centerId = setCentresWithId[0].id
                                setCentresWithId[0].flag = true
                                txtPTLocation.text = setCentresWithId[0].title
                                callWeeklyPrayerTimingApi(context, txtNoData, false)
                            }
                        }
                        ProgressLoader.closeLoader()
                    } catch (e: Exception) {
                        ProgressLoader.closeLoader()
                        e.printStackTrace()
                    }
                }
            }).getCentreTypeList()
        } else
            CommonUtils.showToast(context, context.getString(R.string.checkInternet))
    }

    fun showFullMonth(v: View) {
        if (prayerTimes != null && prayerTimes?.size != 0) {
            val prayerDialog = PrayerTimingFragmentDialog(prayerTimes!!, getSelecteMonthItme)
            prayerDialog.show((v.context as HomeActivity).supportFragmentManager, "pryerDialog")

        } else if (prayerTimes?.size == 0) {
            CommonUtils.showToast(v.context, context.getString(R.string.sorryNoDataAvailable))
        }
    }

    fun callWeeklyPrayerTimingApi(context: Context, txtNoData: TextView, flag: Boolean) {
        if (CommonUtils.isOnlinne(context)) {
            if (flag) ProgressLoader.showLoader(context)
            val c1: Calendar = Calendar.getInstance()
            val maplist = HashMap<String, String>()

            maplist[ApiKey.centre_id] = centerId.toString()
            maplist["filter_dates[0][0]"] = CommonUtils.getCurrentWeekStartDate(c1)
            maplist["filter_dates[0][1]"] = CommonUtils.getCurrentWeekLastDate(c1)

            Log.e("println", "____$maplist")

            PrayerTimingRepository(context, object : AllApiResponse {
                override fun sendResponse(response: Response<*>?) {
                    ProgressLoader.closeLoader()
                    try {
                        if (response != null) {
                            val getResponse = response.body() as PrayerTimingResponse
                            if (getResponse.data?.prayerTimes!!.size != 0) {
                                txtNoData.visibility = View.GONE

                                storeWeatherData(getResponse.data.prayerTimes)
                                val filterPrayerMonthList = ArrayList<ArrayList<PrayerTimesItem?>>()
                                filterPrayerMonthList.add(0, getResponse.data.prayerTimes)

                                if (filterPrayerMonthList.size != 0) {
                                    prayerTimeMonthAdapter.addList(filterPrayerMonthList, 1)
                                    prayerTimeMonthAdapter.notifyDataSetChanged()
                                }
                            } else {
                                txtNoData.visibility = View.VISIBLE
                            }
                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }
            }).getPrayerTimingList(maplist)
        } else
            CommonUtils.showToast(context, context.getString(R.string.checkInternet))
    }

    private fun storeWeatherData(prayerTimes: ArrayList<PrayerTimesItem?>) {
        prayerTimes.forEachIndexed { index, prayerTimesItem ->
            if (prayerTimesItem!!.prayerDate!!.split("-")[2].equals(CommonUtils.getOnlyCurrentDate())) {
                listOfWeathers = arrayListOf()
                listOfWeathers.add(
                    WeatherInPrayerTimeModel(
                        (R.drawable.ic_dawn),
                        context.getString(R.string.fajr),
                        CommonUtils.setHourWithTimeFormate(prayerTimesItem.fajrPrayerTime!!),
                        context.getString(R.string.am),
                        false
                    )
                )

                listOfWeathers.add(
                    WeatherInPrayerTimeModel(
                        (R.drawable.ic_sunrise),
                        context.getString(R.string.sunrise),
                        CommonUtils.setHourWithTimeFormate(prayerTimesItem.sunrisePrayerTime!!),
                        context.getString(R.string.am),
                        false
                    )
                )

                listOfWeathers.add(
                    WeatherInPrayerTimeModel(
                        (R.drawable.ic_sun),
                        context.getString(R.string.zohr),
                        CommonUtils.setHourWithTimeFormate(prayerTimesItem.zohrPrayerTime!!),
                        context.getString(R.string.pM),
                        false
                    )
                )

                listOfWeathers.add(
                    WeatherInPrayerTimeModel(
                        (R.drawable.ic_cloudy_sun),
                        context.getString(R.string.asr),
                        CommonUtils.setHourWithTimeFormate(prayerTimesItem.asrPrayerTime!!),
                        context.getString(R.string.pM),
                        false
                    )
                )

                listOfWeathers.add(
                    WeatherInPrayerTimeModel(
                        (R.drawable.ic_night_cloud),
                        context.getString(R.string.maghrib),
                        CommonUtils.setHourWithTimeFormate(prayerTimesItem.maghribPrayerTime!!),
                        context.getString(R.string.pM),
                        false
                    )
                )
                listOfWeathers.add(
                    WeatherInPrayerTimeModel(
                        (R.drawable.ic_night_star),
                        context.getString(R.string.isha),
                        CommonUtils.setHourWithTimeFormate(prayerTimesItem.ishaPrayerTime!!),
                        context.getString(R.string.pM),
                        false
                    )
                )
                setWeatherData(listOfWeathers)
            }
        }
    }

    private fun setWeatherData(listOfWeathers: ArrayList<WeatherInPrayerTimeModel>) {
        val fmt = SimpleDateFormat("hh:mm a")
        fmt.timeZone = TimeZone.getTimeZone("UTC")
        val currentMillis = fmt.parse(CommonUtils.getCurrentTime()).time
        var bestMillis: Long = 0
        var minMillis: Long = 0
        var bestTime: String? = null
        var minTime: String? = null

        for (time in listOfWeathers) {
            val millis = fmt.parse(time.time).time
            if (millis >= currentMillis && (bestTime == null || millis < bestMillis)) {
                bestMillis = millis
                bestTime = time.time
            }
            if (minTime == null || millis < minMillis) {
                minMillis = millis
                minTime = time.time
            }
        }
        val ans = bestTime ?: minTime


        listOfWeathers.forEachIndexed { index, weatherInPrayerTimeModel ->
            if (listOfWeathers[index].time.equals(ans)) {
                setDataOnTopView(index)

                listOfWeathers[index].flag = true
            }
        }

        weatherInPrayerTimeAdapter.addData(listOfWeathers, object : SendThreeValueInterface {
            override fun actionFire(position: Any, firstValue: Any, secondValue: Any) {
                setDataOnTopView(position as Int)
                scrollInAdapter.postValue(position)
                if (secondValue as Boolean) {
                    weatherInPrayerTimeAdapter.refreshAdapter(position as Int, firstValue as Int)
                }

            }
        })
        weatherInPrayerTimeAdapter.notifyDataSetChanged()
    }

    fun seeWeatherViewData(): WeatherInPrayerTimeAdapter {
        return weatherInPrayerTimeAdapter
    }

    fun setDataOnTopView(position: Int) {
        imgIcon.setImageResource(listOfWeathers[position as Int].image)
        txtPTTitle.text = listOfWeathers[position].name
        txtPTTime.text = listOfWeathers[position].time
        txtPTAmPm.text = listOfWeathers[position].amPm
        imgBg.setImageResource(listOfImages[position])
    }

    fun callMonthlyPrayerTimingApi(
        context: Context,
        relCalenderView: RelativeLayout,
        txtPTNoData: TextView,
        tvDate: TextView
    ) {
        if (CommonUtils.isOnlinne(context)) {
            ProgressLoader.showLoader(context)

            setPopUpMenu(relCalenderView, txtPTNoData, tvDate)
            val maplist = HashMap<String, String>()
            maplist[ApiKey.centre_id] = centerId.toString()
            maplist["filter_dates[0][0]"] = CommonUtils.getMonthStartDate(selectedMonthNumber - 1)
            maplist["filter_dates[0][1]"] = CommonUtils.getMonthEndDate(selectedMonthNumber - 1)
            Log.e("println", "____$maplist")

            PrayerTimingRepository(context, object : AllApiResponse {
                override fun sendResponse(response: Response<*>?) {
                    try {
                        if (response != null) {
                            val getResponse = response.body() as PrayerTimingResponse
                            ProgressLoader.closeLoader()
                            if (getResponse.data?.prayerTimes!!.size != 0) {
                                txtPTNoData.visibility = View.GONE
                                prayerTimes = arrayListOf()
                                prayerTimes = getResponse.data.prayerTimes

                                val filterPrayerMonthList = ArrayList<ArrayList<PrayerTimesItem?>>()
                                filterPrayerMonthList.add(0, prayerTimes!!)

                                if (filterPrayerMonthList.size != 0) {
                                    prayerTimeMonthAdapter.addList(filterPrayerMonthList, 1)
                                    prayerTimeMonthAdapter.notifyDataSetChanged()
                                }
                            } else {
                                txtPTNoData.visibility = View.VISIBLE
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }).getPrayerTimingList(maplist)
        } else
            CommonUtils.showToast(context, context.getString(R.string.checkInternet))
    }

    fun callYearlyPrayerTimingApi(context: Context, txtPTNoData: TextView) {
        if (CommonUtils.isOnlinne(context)) {
            ProgressLoader.showLoader(context)

            val maplist = HashMap<String, String>()
            maplist[ApiKey.centre_id] = centerId.toString()

            storeSelectedMonth.forEachIndexed { i, t ->
                maplist["filter_dates[$i][0]"] =
                    CommonUtils.getMonthStartDate(storeSelectedMonth[i] - 1)
                maplist["filter_dates[$i][1]"] =
                    CommonUtils.getMonthEndDate(storeSelectedMonth[i] - 1)
            }
            Log.e("println", "____$maplist")
            PrayerTimingRepository(context, object : AllApiResponse {
                override fun sendResponse(response: Response<*>?) {
                    ProgressLoader.closeLoader()
                    if (response != null) {
                        val getResponse = response.body() as PrayerTimingResponse
                        try {
                            if (getResponse.data?.prayerTimes!!.size != 0) {
                                txtPTNoData.visibility = View.GONE
                                prayerTimeMonthAdapter.addList(
                                    getMonthViseFilter(getResponse.data.prayerTimes),
                                    storeSelectedMonth.size
                                )
                                prayerTimeMonthAdapter.notifyDataSetChanged()
                            }
                        } catch (e: java.lang.Exception) {
                            e.printStackTrace()
                        }
                    } else {
                        txtPTNoData.visibility = View.VISIBLE
                    }
                }
            }).getPrayerTimingList(maplist)
        } else
            CommonUtils.showToast(context, context.getString(R.string.checkInternet))
    }

    private fun getMonthViseFilter(prayerTimes: ArrayList<PrayerTimesItem?>): ArrayList<ArrayList<PrayerTimesItem?>> {
        val filterPrayerMonthList = ArrayList<ArrayList<PrayerTimesItem?>>()
        var tempFilterPrayerMonthList = ArrayList<PrayerTimesItem?>()

        var currentIndex = 0

        prayerTimes.sortBy { it!!.prayerDate }

        prayerTimes.mapIndexed { index, prayerTimesItem ->
            if (index != 0 && prayerTimesItem!!.prayerDate!!.split("-")[1].toInt()
                > prayerTimes[index - 1]!!.prayerDate!!.split("-")[1].toInt()
            ) {
                filterPrayerMonthList.add(currentIndex, tempFilterPrayerMonthList)
                currentIndex += 1
                tempFilterPrayerMonthList = arrayListOf()
            }
            tempFilterPrayerMonthList.add(prayerTimesItem!!)
            if (index == prayerTimes.size - 1) {
                filterPrayerMonthList.add(currentIndex, tempFilterPrayerMonthList)
            }
        }

        return filterPrayerMonthList
    }

    fun seeTimesInTimeTable(): PrayerTimeMonthAdapter {
        return prayerTimeMonthAdapter
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun setCurrentHajriDate(): String {
        return CommonUtils.setCurrentHajriDates()
    }

    fun setCurrentDate(): String {
        val dateFormatter: DateFormat = SimpleDateFormat("dd MMM yyyy")
        dateFormatter.setLenient(false)
        val today = Date()
        val date: String = dateFormatter.format(today)

        val weekday_name =
            SimpleDateFormat("EEEE", Locale.ENGLISH).format(System.currentTimeMillis())

        return date + " (" + weekday_name + ")"
    }

    private fun setPopUpMenu(
        relCalenderView: RelativeLayout,
        txtPTNoData: TextView,
        tvDate: TextView
    ) {
        listOfMonthFrag =
            ListOfMonthDialogFragment(listOfDropMonth, object : SendSingleValueInterface {
                override fun sendPosition(position: Any) {
                    selectedMonthNumber = listOfMonth[position as Int].monthNumber
                    getSelecteMonthItme = listOfMonth[position].text

                    tvDate.text = getSelecteMonthItme

                    callMonthlyPrayerTimingApi(
                        relCalenderView.context,
                        relCalenderView,
                        txtPTNoData,
                        tvDate
                    )

                }
            })
    }

    fun openCalender(v: View) {
        listOfMonthFrag.show(
            (v.context as HomeActivity).supportFragmentManager,
            "List Of Month"
        )
    }
}