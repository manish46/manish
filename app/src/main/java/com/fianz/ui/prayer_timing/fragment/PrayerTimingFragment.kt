package com.fianz.ui.prayer_timing.fragment

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.fianz.R
import com.fianz.databinding.FragmentPrayerTimingBinding

import com.fianz.ui.prayer_timing.viewmodel.PrayerTimingViewModel
import com.fianz.utils.CenterZoomLayoutManager
import com.fianz.utils.CommonUtils
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import java.lang.Exception
import android.R.attr.data
import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.IntentSender
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts


class PrayerTimingFragment : Fragment() {

    lateinit var binding: FragmentPrayerTimingBinding
    private lateinit var prayerTimingVM: PrayerTimingViewModel
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    lateinit var dialog: Dialog
    lateinit var mLauncher: ActivityResultLauncher<IntentSenderRequest>
    lateinit var mActivityResult: ActivityResultLauncher<Array<String>>

    var type = 0
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(requireActivity()),
            R.layout.fragment_prayer_timing,
            container,
            false
        )

        init()

        return binding.root
    }

    private fun init() {
        dialog = Dialog(requireActivity())

        prayerTimingVM = ViewModelProvider(requireActivity()).get(PrayerTimingViewModel::class.java)

        prayerTimingVM.setValues(
            binding.txtPTLocation,
            binding.imgBg,
            binding.imgIcon,
            binding.txtPTTitle,
            binding.txtPTTime,
            binding.txtPTAmPm
        )

        prayerTimingVM.storeMonthList(requireActivity(), binding.txtPTNoData)
        prayerTimingVM.tabCurrentSelectedTab(
            binding.tabLayout,
            binding.relCalenderView,
            binding.tvDate,
            binding.ivCalendar,
            binding.relPTZoomView,
            binding.rvDates,
            binding.txtPTNoData
        )

        prayerTimingVM.nestedScrollView(binding.nsParentView, requireActivity())
        binding.prayerTimingVM = prayerTimingVM

        binding.rcyWetherView.layoutManager =
            CenterZoomLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)

        fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(requireActivity())
        checkLocationEnableOrNot()
    }

    private fun checkLocationEnableOrNot() {
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 30 * 1000
        locationRequest.fastestInterval = 5 * 1000

        val builder = LocationSettingsRequest.Builder()
        builder.setAlwaysShow(true)
        builder.addLocationRequest(locationRequest)

        val client = LocationServices.getSettingsClient(requireActivity())
        val task = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener(requireActivity(),
            object : OnSuccessListener<LocationSettingsResponse> {
                override fun onSuccess(p0: LocationSettingsResponse?) {
                    if (!checkPermissions()) {
                        requestPermission()
                    } else {
                        getLastLocations("checkLocationEnableOrNot")
                    }
                }
            })
        task.addOnFailureListener(requireActivity(), object : OnFailureListener {
            override fun onFailure(p0: Exception) {
                try {
                    // Show the dialog by calling startResolutionForResult(),  and check the result in onActivityResult().
                    /* val resolvable = p0 as ResolvableApiException
                     resolvable.startResolutionForResult(
                         requireActivity(),
                         12
                     )*/
                    val resolvable = p0 as ResolvableApiException
                    val intentSenderRequest =
                        IntentSenderRequest.Builder(resolvable.resolution).build()
                    mLauncher.launch(intentSenderRequest)

                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mLauncher =
            registerForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) { activityResult ->
                if (activityResult.resultCode == RESULT_OK) {
                    if (!checkPermissions()) {
                        requestPermission()
                    } else {
                        getLastLocations("onAttach")
                    }
                } else {
                    checkLocationEnableOrNot()
                }
            }
        mActivityResult =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permission ->
                permission.entries.forEach {
                    Log.e("DEBUG", "${it.key} = ${it.value}")
                    if (permission[ACCESS_FINE_LOCATION] != true && permission[ACCESS_COARSE_LOCATION] != true) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(
                                requireActivity(),
                                ACCESS_FINE_LOCATION
                            ) && ActivityCompat.shouldShowRequestPermissionRationale(
                                requireActivity(),
                                ACCESS_COARSE_LOCATION
                            )
                        ) {
                            type = 1
                            requestPermission()
                        } else {
                            type = 2
                            CommonUtils.showAlertPermision(requireActivity(), dialog)
                        }
                    } else {
                        getLastLocations("registerForActivityResult")
                        return@registerForActivityResult
                    }
                }
            }
    }

    private fun checkPermissions(): Boolean {
        val location = ContextCompat.checkSelfPermission(
            requireActivity(),
            ACCESS_FINE_LOCATION
        )
        val secLocation = ContextCompat.checkSelfPermission(
            requireActivity(),
            ACCESS_COARSE_LOCATION
        )
        return location == PackageManager.PERMISSION_GRANTED && secLocation == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        mActivityResult.launch(
            arrayOf(
                ACCESS_FINE_LOCATION,
                ACCESS_COARSE_LOCATION
            )
        )
    }

    fun getLastLocations(msg:String) {
        Log.e("msg",msg)
        CommonUtils.dissmissDialog(dialog)
        if (ActivityCompat.checkSelfPermission(
                requireActivity(),
                ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireActivity(),
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationProviderClient.getCurrentLocation(LocationRequest.PRIORITY_HIGH_ACCURACY, null)
        fusedLocationProviderClient.lastLocation.addOnCompleteListener(requireActivity()) { task ->
            if (task.isSuccessful && task.result != null) {
                val lastlocation = task.result

                prayerTimingVM.callCentreList(
                    requireActivity(),
                    binding.txtPTNoData,
                    lastlocation.latitude,
                    lastlocation.longitude,
                    true
                )
            } else {
                prayerTimingVM.callCentreList(
                    requireActivity(),
                    binding.txtPTNoData,
                    0.0,
                    0.0,
                    false
                )
            }
        }
    }

    /*override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            100 -> {
                if (grantResults.size > 0) {
                    if (grantResults[0] != PackageManager.PERMISSION_GRANTED && grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(
                                requireActivity(),
                                ACCESS_FINE_LOCATION
                            )
                        ) {
                            type = 1
                            requestPermission()
                        } else {
                            type = 2
                            CommonUtils.showAlertPermision(requireActivity(), dialog)
                        }
                    } else {
                        getLastLocations()
                    }
                }
            }
        }
    }*/

    override fun onResume() {
        super.onResume()
        binding.nsParentView.fullScroll(NestedScrollView.FOCUS_UP)
        when (type) {
            1 -> {
                requestPermission()
            }
            2 -> {
                if (!checkPermissions()) {
                    CommonUtils.showAlertPermision(requireActivity(), dialog)
                } else {
                    getLastLocations("onResume")
                }
            }
        }
    }
}