package com.fianz.ui.prayer_timing.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowPrayerYearBinding
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.model.PrayerMonthWithNumberModel

class YearInPrayerTimeAdapter : RecyclerView.Adapter<YearInPrayerTimeAdapter.ViewHolder>() {

    private var listOfYear = ArrayList<PrayerMonthWithNumberModel>()
    private lateinit var sendThreeValueInterface: SendThreeValueInterface

    fun addData(
        listOfYear: ArrayList<PrayerMonthWithNumberModel>,
        sendThreeValueInterface: SendThreeValueInterface
    ) {
        this.listOfYear = listOfYear
        this.sendThreeValueInterface = sendThreeValueInterface
    }

    class ViewHolder(
        var binding: RowPrayerYearBinding,
        var sendThreeValueInterface: SendThreeValueInterface
    ) : RecyclerView.ViewHolder(binding.root),

        View.OnClickListener {
        fun bind(data: PrayerMonthWithNumberModel) {
            binding.prayerYearM = data
            binding.executePendingBindings()

            init(data)
            setListner()
        }

        private fun init(data: PrayerMonthWithNumberModel) {
            if (data.selectedFlag) {
                setTextColor(R.color.blue_indicator_05d, R.color.white)
            } else {
                setTextColor(R.color.gray_8d8, R.color.blue_indicator_05d)
            }
        }

        private fun setTextColor(bgColor: Int, textColor: Int) {
            binding.txtYearText.setBackgroundColor(
                ContextCompat.getColor(
                    itemView.context,
                    bgColor
                )
            )
            binding.txtYearText.setTextColor(
                ContextCompat.getColor(
                    itemView.context,
                    textColor
                )
            )
        }

        private fun setListner() {
            binding.txtYearText.setOnClickListener(this@ViewHolder)
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.txtYearText -> {
                    if (!binding.prayerYearM!!.selectedFlag) {
                        setTextColor(R.color.blue_indicator_05d, R.color.white)
                        binding.prayerYearM!!.selectedFlag = true
                        sendThreeValueInterface.actionFire(layoutPosition,"remove","")
                    } else {
                        setTextColor(R.color.gray_8d8, R.color.blue_indicator_05d)
                        binding.prayerYearM!!.selectedFlag = false
                        sendThreeValueInterface.actionFire(layoutPosition,"add","")
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: RowPrayerYearBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_prayer_year, parent, false
        )

        return ViewHolder(binding, sendThreeValueInterface)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listOfYear[position])
    }

    override fun getItemCount(): Int {
        return listOfYear.size
    }
}