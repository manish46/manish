package com.fianz.ui.prayer_timing.adapter

import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowMonthItemPtBinding
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.model.prayer_model.PrayerTimesItem
import com.fianz.utils.CommonUtils
import java.text.SimpleDateFormat

class MonthInPrayerTimeAdapter :
    RecyclerView.Adapter<MonthInPrayerTimeAdapter.ViewHolder>() {

    var listOfYear = ArrayList<PrayerTimesItem?>()
    lateinit var sendThreeValueInterface: SendThreeValueInterface

    var currentPos = 0
    var prePos = 1

    fun addList(
        listOfYear: ArrayList<PrayerTimesItem?>,
        sendThreeValueInterface: SendThreeValueInterface
    ) {
        this.listOfYear = listOfYear
        this.sendThreeValueInterface = sendThreeValueInterface
    }

    fun refreshView(currentPos: Int, prePos: Int) {
        listOfYear[prePos]!!.flag = false
        listOfYear[currentPos]!!.flag = true

        notifyItemChanged(currentPos)
        notifyItemChanged(prePos)
    }

    class ViewHolder(
        var binding: RowMonthItemPtBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        var txtYearText: TextView

        init {
            txtYearText = binding.txtYearText
        }

        /*companion object {
            lateinit var preView: TextView
        }

        fun bind(data: PrayerTimesItem) {
            binding.prayerYearM = data
            binding.executePendingBindings()

            init()
            setListener()
        }

        private fun init() {
            if (binding.prayerYearM!!.flag) {
                binding.prayerYearM!!.flag = true
                setTextColor(R.color.blue_indicator_05d, R.color.white)
//                preView = binding.txtYearText
                currentPos = layoutPosition

                sendThreeValueInterface.actionFire(currentPos, prePos, false)
            } else {
                binding.prayerYearM!!.flag = false
                setTextColor(R.color.gray_8d8, R.color.blue_indicator_05d)
            }
        }

        fun setListener() {
            binding.txtYearText.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.txtYearText -> {
                    if ((binding.txtYearText.background as ColorDrawable).color != -15708067) {
                                        preView.setBackgroundColor(
                                            ContextCompat.getColor(
                                                itemView.context,
                                                R.color.gray_8d8
                                            )
                                        )
                                        preView.setTextColor(
                                            ContextCompat.getColor(
                                                itemView.context,
                                                R.color.blue_indicator_05d
                                            )
                                        )

                                        preView = binding.txtYearText

                                        preView.setBackgroundColor(
                                            ContextCompat.getColor(
                                                itemView.context,
                                                R.color.blue_indicator_05d
                                            )
                                        )
                                        preView.setTextColor(
                                            ContextCompat.getColor(
                                                itemView.context,
                                                R.color.white
                                            )
                                        )
                                        sendSingleValueInterface.sendPosition(layoutPosition)
                                    }
                    if (!binding.prayerYearM!!.flag) {
                        prePos = currentPos
                        currentPos = layoutPosition

                        sendThreeValueInterface.actionFire(currentPos, prePos, true)
                    }
                }
            }
        }

        private fun setTextColor(bgColor: Int, textColor: Int) {
            binding.txtYearText.setBackgroundColor(
                ContextCompat.getColor(
                    itemView.context,
                    bgColor
                )
            )
            binding.txtYearText.setTextColor(
                ContextCompat.getColor(
                    itemView.context,
                    textColor
                )
            )
        }*/

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: RowMonthItemPtBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_month_item_pt, parent, false
        )

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //        holder.bind(listOfYear[position]!!)

        holder.txtYearText.text = CommonUtils.setDateAndMonth(listOfYear[position]!!.prayerDate!!)

        if (listOfYear[position]!!.flag) {
            setTextColor(holder, R.color.blue_indicator_05d, R.color.white)
            currentPos = position
            sendThreeValueInterface.actionFire(position, 0, false)
        } else {
            setTextColor(holder, R.color.gray_8d8, R.color.blue_indicator_05d)
        }

        holder.txtYearText.setOnClickListener {
            /*if ((holder.txtYearText.background as ColorDrawable).color != -15708067) {
                holder.txtYearText?.setBackgroundColor(
                    ContextCompat.getColor(
                        holder.itemView.context,
                        R.color.gray_8d8
                    )
                )
                holder.txtYearText?.setTextColor(
                    ContextCompat.getColor(
                        holder.itemView.context,
                        R.color.blue_indicator_05d
                    )
                )

//                preHoler = holder

                holder.txtYearText.setBackgroundColor(
                    ContextCompat.getColor(
                        holder.itemView.context!!,
                        R.color.blue_indicator_05d
                    )
                )
                holder.txtYearText.setTextColor(
                    ContextCompat.getColor(
                        holder.itemView.context!!,
                        R.color.white
                    )
                )
                sendThreeValueInterface.actionFire(position, 0, "")
            }*/
            prePos = currentPos
            currentPos = position

            sendThreeValueInterface.actionFire(currentPos, prePos, true)
        }
    }

    private fun setTextColor(holder: ViewHolder, bgColor: Int, textColor: Int) {
        holder.txtYearText.setBackgroundColor(
            ContextCompat.getColor(
                holder.itemView.context,
                bgColor
            )
        )
        holder.txtYearText.setTextColor(
            ContextCompat.getColor(
                holder.itemView.context,
                textColor
            )
        )
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return listOfYear.size
    }
}