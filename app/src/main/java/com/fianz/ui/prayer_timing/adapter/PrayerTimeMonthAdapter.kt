package com.fianz.ui.prayer_timing.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowPrayerDayTimeBinding
import com.fianz.interfaces.SendSingleValueInterface
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.model.prayer_model.PrayerTimesItem

class PrayerTimeMonthAdapter : RecyclerView.Adapter<PrayerTimeMonthAdapter.ViewHolder>() {

    private var listOfYear = ArrayList<ArrayList<PrayerTimesItem?>>()
    private var size = 0

    fun addList(
        listOfYear:ArrayList<ArrayList<PrayerTimesItem?>>,
        size: Int
    ) {
        this.listOfYear = listOfYear
        this.size = size
    }

    class ViewHolder(val binding: RowPrayerDayTimeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(prayerTimeModel: ArrayList<PrayerTimesItem?>) {
            val monthInPrayerTimeAdapter = MonthInPrayerTimeAdapter()

            binding.rcyDateList.layoutManager = LinearLayoutManager(
                binding.rcyDateList.context,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            prayerTimeModel[0]!!.flag = true
            monthInPrayerTimeAdapter.addList(prayerTimeModel, object : SendThreeValueInterface {
                override fun actionFire(position: Any, firstValue: Any, secondValue: Any) {
                    if (secondValue as Boolean) {
                        monthInPrayerTimeAdapter.refreshView(position as Int, firstValue as Int)
                    }
                    setTextValues(prayerTimeModel, position as Int)
                }
            })
            monthInPrayerTimeAdapter.notifyDataSetChanged()
            binding.rcyDateList.adapter = monthInPrayerTimeAdapter

        }

        private fun setTextValues(prayerTimeModel: ArrayList<PrayerTimesItem?>, position: Int) {
            binding.txtFajrTime.text = prayerTimeModel[position]!!.fajrPrayerTime
            binding.txtSunriseTime.text = prayerTimeModel[position]!!.sunrisePrayerTime
            binding.txtZohrTime.text = prayerTimeModel[position]!!.zohrPrayerTime
            binding.txtAsrTime.text = prayerTimeModel[position]!!.asrPrayerTime
            binding.txtMaghribTime.text = prayerTimeModel[position]!!.maghribPrayerTime
            binding.txtIshaTime.text = prayerTimeModel[position]!!.ishaPrayerTime
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val bind: RowPrayerDayTimeBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_prayer_day_time,
            parent,
            false
        )
        return ViewHolder(bind)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listOfYear[position])
    }

    override fun getItemCount(): Int {
        return size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}