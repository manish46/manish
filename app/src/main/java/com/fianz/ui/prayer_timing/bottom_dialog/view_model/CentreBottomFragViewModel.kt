package com.fianz.ui.prayer_timing.bottom_dialog.view_model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.fianz.R
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.model.prayer_model.CentresIdAndTitle
import com.fianz.ui.prayer_timing.bottom_dialog.adapter.LocationListAdapter
import java.util.*
import kotlin.collections.ArrayList

class CentreBottomFragViewModel(appApplication: Application) : AndroidViewModel(appApplication) {

    private var locationAdapter: LocationListAdapter = LocationListAdapter()
    var selectedItem: MutableLiveData<Int> = MutableLiveData()

    var context = appApplication.resources!!
    lateinit var centresWithId: ArrayList<CentresIdAndTitle>
    var currentPos = 0
    var previousPos = 0
    var sendingId = 0

    fun storeLocationData(centresWithId: ArrayList<CentresIdAndTitle>) {
        this.centresWithId = centresWithId
        locationAdapter.addList(centresWithId, object : SendThreeValueInterface {
            override fun actionFire(position: Any, firstValue: Any, secondValue: Any) {
                currentPos = firstValue as Int
                previousPos = secondValue as Int

                sendingId = previousPos

                locationAdapter.refreshAdapter(firstValue, secondValue)
            }
        })
        locationAdapter.notifyDataSetChanged()
    }

    fun seeListOfLocation(): LocationListAdapter {
        return locationAdapter
    }

    fun searchData(text: String) {
        val tempList = ArrayList<CentresIdAndTitle>()
        for (data in centresWithId) {
            if (data.title.toString().lowercase(Locale.getDefault()).contains(text)) {
                tempList.add(data)
            }
        }
        locationAdapter.filterData(tempList)
    }

    fun removeCurrentSelection() {
        sendingId = currentPos
        locationAdapter.refreshAdapter(previousPos, currentPos)
    }

    fun getSendingId() {
        selectedItem.postValue(sendingId)
    }
}