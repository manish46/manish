package com.fianz.ui.prayer_timing.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowTodaysTimeBinding
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.model.WeatherInPrayerTimeModel
import com.fianz.utils.CommonUtils

class WeatherInPrayerTimeAdapter : RecyclerView.Adapter<WeatherInPrayerTimeAdapter.ViewHolder>() {

    private var listOfDayWithTime = ArrayList<WeatherInPrayerTimeModel>()
    private lateinit var sendThreeValueInterface: SendThreeValueInterface


    companion object {
        var previousPos = 0
        var currentPos = 0
    }

    fun addData(
        listOfDayWithTime: ArrayList<WeatherInPrayerTimeModel>,
        sendThreeValueInterface: SendThreeValueInterface
    ) {
        this.listOfDayWithTime = listOfDayWithTime
        this.sendThreeValueInterface = sendThreeValueInterface
    }

    fun refreshAdapter(currentPos: Int, previousPos: Int) {
        listOfDayWithTime[currentPos].flag = true
        listOfDayWithTime[previousPos].flag = false

        notifyItemChanged(currentPos)
        notifyItemChanged(previousPos)
    }

    class ViewHolder(
        private val binding: RowTodaysTimeBinding,
        private val sendThreeValueInterface: SendThreeValueInterface
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(listOfDayWithTime: ArrayList<WeatherInPrayerTimeModel>, position: Int) {

            binding.todayPrayerVM = listOfDayWithTime[position]
            binding.executePendingBindings()

            init(listOfDayWithTime, position)
            setListener(listOfDayWithTime, position)
        }

        private fun init(listOfDayWithTime: ArrayList<WeatherInPrayerTimeModel>, position: Int) {
            binding.tvPrayerTime.text =binding.todayPrayerVM!!.time
            if (listOfDayWithTime[position].flag) {
                currentPos = position
                changeViewColor(
                    binding,
                    R.color.dark_green,
                    R.color.white,
                    R.color.white,
                    R.color.white
                )
                sendThreeValueInterface.actionFire(currentPos, previousPos, false)
            } else {
                changeViewColor(
                    binding,
                    R.color.white,
                    R.color.blue_8c3,
                    R.color.blue_0db,
                    R.color.black
                )
            }
        }

        private fun setListener(
            listOfDayWithTime: ArrayList<WeatherInPrayerTimeModel>,
            position: Int
        ) {
            binding.relMainTodaysTimeRow.setOnClickListener {
                if (!listOfDayWithTime[position].flag) {
                    previousPos = currentPos
                    currentPos = layoutPosition

                    sendThreeValueInterface.actionFire(currentPos, previousPos, true)
                }
            }
        }

        private fun changeViewColor(
            binding: RowTodaysTimeBinding,
            bgDrawable: Int,
            iconColor: Int,
            nameColor: Int,
            timeColor: Int
        ) {
            binding.imgWhether.setColorFilter(
                ContextCompat.getColor(
                    binding.root.context,
                    iconColor
                ), android.graphics.PorterDuff.Mode.SRC_IN
            )

            binding.crdRTTParent.setCardBackgroundColor(
                ContextCompat.getColor(
                    binding.crdRTTParent.context,
                    bgDrawable
                )
            )
            binding.tvPrayerName.setTextColor(
                ContextCompat.getColor(
                    binding.root.context,
                    nameColor
                )
            )
            binding.tvPrayeram.setTextColor(
                ContextCompat.getColor(
                    binding.root.context,
                    nameColor
                )
            )
            binding.tvPrayerTime.setTextColor(
                ContextCompat.getColor(
                    binding.root.context,
                    timeColor
                )
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val bind: RowTodaysTimeBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_todays_time,
            parent,
            false
        )
        return ViewHolder(bind, sendThreeValueInterface)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listOfDayWithTime, position)
    }

    override fun getItemCount(): Int {
        return listOfDayWithTime.size
    }
}