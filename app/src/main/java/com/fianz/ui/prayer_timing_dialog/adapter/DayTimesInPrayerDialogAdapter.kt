package com.fianz.ui.prayer_timing_dialog.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowDayWithTimePrayerBinding
import com.fianz.model.PrayerDayWithTimeModel
import com.fianz.model.prayer_model.PrayerTimesItem

class DayTimesInPrayerDialogAdapter :
    RecyclerView.Adapter<DayTimesInPrayerDialogAdapter.ViewHolder>() {
    private var listOfDayWithTime = ArrayList<PrayerTimesItem?>()

    fun addData(listOfDayWithTime: ArrayList<PrayerTimesItem?>) {
        this.listOfDayWithTime = listOfDayWithTime
    }

    class ViewHolder(val binding: RowDayWithTimePrayerBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: PrayerTimesItem) {
            binding.prayerDayTimeVM = data
            
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val bind: RowDayWithTimePrayerBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_day_with_time_prayer,
            parent,
            false
        )
        return ViewHolder(bind)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(listOfDayWithTime[position]!!)

    override fun getItemCount(): Int {
        return listOfDayWithTime.size
    }
}