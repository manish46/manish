package com.fianz.ui.prayer_timing_dialog.view_model

import android.app.Application
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.AndroidViewModel
import com.fianz.model.PrayerDayWithTimeModel
import com.fianz.model.TimesInPrayerTimeModel
import com.fianz.model.prayer_model.PrayerTimesItem
import com.fianz.ui.prayer_timing_dialog.adapter.DayTimesInPrayerDialogAdapter
import com.fianz.utils.CommonUtils

class PrayerTimingFragmentDialogViewModel(application: Application) :
    AndroidViewModel(application) {

    private var prayerDayWithTimeAdapter: DayTimesInPrayerDialogAdapter =
        DayTimesInPrayerDialogAdapter()

    fun storeDayWithTimeData(context: Context, prayerTimes: ArrayList<PrayerTimesItem?>) {
        prayerDayWithTimeAdapter.addData(prayerTimes)
        prayerDayWithTimeAdapter.notifyDataSetChanged()
    }

    fun getDayWithTimeAdapter(): DayTimesInPrayerDialogAdapter {
        return prayerDayWithTimeAdapter
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun setCurrentHajriDate(): String {
        return CommonUtils.setCurrentHajriDates()
    }
}