package com.fianz.ui.prayer_timing_dialog.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowPrayerTimeDialogBinding
import com.fianz.model.TimesInPrayerTimeModel

class TimingPrayerDialogAdapter(private var listOfTime: ArrayList<TimesInPrayerTimeModel>) :

    RecyclerView.Adapter<TimingPrayerDialogAdapter.ViewHolder>() {

    class ViewHolder(val binding: RowPrayerTimeDialogBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(prayerTimeModel: TimesInPrayerTimeModel) {
            binding.prayerTimeVM = prayerTimeModel
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val bind: RowPrayerTimeDialogBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_prayer_time_dialog,
            parent,
            false
        )
        return ViewHolder(bind)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listOfTime[position])
    }

    override fun getItemCount(): Int {
        return listOfTime.size
    }
}