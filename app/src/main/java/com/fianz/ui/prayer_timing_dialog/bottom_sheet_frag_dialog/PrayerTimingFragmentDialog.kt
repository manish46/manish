package com.fianz.ui.prayer_timing_dialog.bottom_sheet_frag_dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.fianz.R
import com.fianz.databinding.FragmentPrayerTimingDialogBinding
import com.fianz.model.prayer_model.PrayerTimesItem
import com.fianz.ui.prayer_timing_dialog.view_model.PrayerTimingFragmentDialogViewModel

class PrayerTimingFragmentDialog(var prayerTimes: ArrayList<PrayerTimesItem?>,var getSelecteMonthItme:String) : DialogFragment(), View.OnClickListener {

    lateinit var binding: FragmentPrayerTimingDialogBinding
    private lateinit var prayerDayWithTimeModel: PrayerTimingFragmentDialogViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_prayer_timing_dialog,
            container,
            false
        )

        init()
        setListener()
        return binding.root
    }

    private fun init() {
        prayerDayWithTimeModel =
            ViewModelProvider(
                requireActivity()
            ).get(PrayerTimingFragmentDialogViewModel::class.java)
        prayerDayWithTimeModel.storeDayWithTimeData(requireActivity(),prayerTimes)
        binding.txtFPMCalText.text = getSelecteMonthItme
        binding.prayerDayWithTimeVM = prayerDayWithTimeModel
    }

    private fun setListener() {
        binding.cvCancel.setOnClickListener(this)
        binding.crdPTDOk.setOnClickListener(this)
        binding.imgClose.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            context?.resources?.displayMetrics?.widthPixels!!,
            context?.resources?.displayMetrics?.heightPixels!!
        )
        dialog?.show()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.cvCancel -> {
                dialog?.dismiss()
            }
            R.id.crdPTDOk -> {
                dialog?.dismiss()
            }
            R.id.imgClose -> {
                dialog?.dismiss()
            }
        }
    }


}