package com.fianz.ui.contact_us.view_model

import android.app.Application
import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import com.fianz.utils.CommonUtils

class ContactUsViewModel(application: Application) : AndroidViewModel(application) {

    fun clickOnAddress(v: View) {
        CommonUtils.openGoogleMap(
            v.context,
            (v as TextView).text.toString()
        )
        /* val intent = Intent(
             Intent.ACTION_VIEW,
             Uri.parse("http://maps.google.com/maps?saddr=20.344,34.34&daddr=20.5666,45.345")
         )
         v.context.startActivity(intent)*/
    }

    fun clickOnPhone(v: View) {
        val phone = "+64 4 387 8023"
        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
        v.context.startActivity(intent)
    }

    fun clickOnEmail(v: View) {
        val emailIntent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:abc@xyz.com")
        }
        v.context.startActivity(Intent.createChooser(emailIntent, "Send feedback"))
    }
}