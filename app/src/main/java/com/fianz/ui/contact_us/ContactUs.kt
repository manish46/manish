package com.fianz.ui.contact_us

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.fianz.R
import com.fianz.databinding.ActivityContactUsBinding
import com.fianz.ui.contact_us.view_model.ContactUsViewModel

class ContactUs : AppCompatActivity() {
    lateinit var binding: ActivityContactUsBinding
    lateinit var contactVM: ContactUsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_contact_us)

        init()
        setListener()
    }

    fun init() {
        contactVM = ViewModelProvider(this).get(ContactUsViewModel::class.java)
        binding.contactusVM = contactVM
    }

    fun setListener() {
        binding.imgOnBack.setOnClickListener {
            onBackPressed()
        }
    }
}