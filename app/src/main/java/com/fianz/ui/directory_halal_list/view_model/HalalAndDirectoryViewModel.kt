package com.fianz.ui.directory_halal_list.view_model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.fianz.R
import com.fianz.model.directory_model.DirectoryHeaderModel
import com.fianz.model.directory_model.DirectoryItemModel
import com.fianz.ui.directory_halal_list.adapter.DirectoryAdapters

class HalalAndDirectoryViewModel(application: Application) : AndroidViewModel(application) {

    private lateinit var listOfHeaderData: ArrayList<DirectoryHeaderModel>
    private lateinit var listOfItemData: ArrayList<DirectoryItemModel>
    private lateinit var listOfItemDatas: ArrayList<DirectoryItemModel>
    private var directoryAdapter: DirectoryAdapters = DirectoryAdapters()

    private var context = application.resources

    fun storeData() {
        listOfHeaderData = arrayListOf()
        listOfItemData = arrayListOf()
        listOfItemDatas = arrayListOf()

        listOfItemData.add(
            DirectoryItemModel(
                context.getString(R.string.sensientTechnologies),
                context.getString(R.string._05_place),
                context.getString(R.string._9429039356789_1)
            )
        )
        listOfItemData.add(
            DirectoryItemModel(
                context.getString(R.string.mainlandLimited),
                context.getString(R.string._02_auckland),
                context.getString(R.string._9429038059292_1)
            )
        )
        listOfItemDatas.add(
            DirectoryItemModel(
                context.getString(R.string.liquidLimited),
                context.getString(R.string._buildingAuckland),
                context.getString(R.string._9429036729135_1)
            )
        )

        listOfHeaderData.add(
            DirectoryHeaderModel(
                context.getString(R.string.auc_region),
                listOfItemData,
                true
            )
        )
        listOfHeaderData.add(
            DirectoryHeaderModel(
                context.getString(R.string.auc_region),
                listOfItemDatas,
                false
            )
        )

        setAdapter(listOfHeaderData)
    }

    private fun setAdapter(listOfHeaderData: ArrayList<DirectoryHeaderModel>) {
//        directoryAdapter.addList(listOfHeaderData)
//        directoryAdapter.notifyDataSetChanged()
    }

    fun seeHeaderList(): DirectoryAdapters {
        return directoryAdapter
    }
}