package com.fianz.ui.directory_halal_list.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowHeaderInDirectoryBinding
import com.fianz.model.directory_model.DirectoryHeaderModel
import com.fianz.model.get_centers_list.GetCentersCentresItem

class DirectoryAdapters : RecyclerView.Adapter<DirectoryAdapters.ViewHolder>() {
    private var listOfHeaderData = ArrayList<GetCentersCentresItem?>()

    fun addList(listOfHeaderData: ArrayList<GetCentersCentresItem?>) {
        this.listOfHeaderData = listOfHeaderData
    }

    class ViewHolder(var binding: RowHeaderInDirectoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ArrayList<GetCentersCentresItem?>, position: Int) {
//            binding.directoryHeaderM = data[position]
//            binding.executePendingBindings()

//            hideSubItemOrNot(data)

//            binding.rcyItemList.adapter = DirectoryItemAdapter(data)

            binding.relTitleView.setOnClickListener {
                /*if (!data.hideShow) {
                    binding.imgView.setImageResource(R.drawable.minus_icon)
                    binding.rcyItemList.visibility = View.VISIBLE
                    data.hideShow = true
                } else {
                    binding.imgView.setImageResource(R.drawable.thick_plus_icon)
                    binding.rcyItemList.visibility = View.GONE
                    data.hideShow = false
                }*/
            }
        }

        private fun hideSubItemOrNot(data: DirectoryHeaderModel) {
            if (data.hideShow) {
                binding.imgView.setImageResource(R.drawable.minus_icon)
                binding.rcyItemList.visibility = View.VISIBLE
            } else {
                binding.imgView.setImageResource(R.drawable.thick_plus_icon)
                binding.rcyItemList.visibility = View.GONE
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val headerBinding: RowHeaderInDirectoryBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_header_in_directory,
            parent,
            false
        )

        return ViewHolder(headerBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listOfHeaderData,position)
    }

    override fun getItemCount(): Int {
        return 2
    }
}