package com.fianz.ui.directory_halal_list.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.fianz.R
import com.fianz.databinding.FragmentHalalAndDirectoryListBinding
import com.fianz.ui.directory_halal_list.view_model.HalalAndDirectoryViewModel

class HalalAndDirectoryList : Fragment() {

    private lateinit var binding: FragmentHalalAndDirectoryListBinding
    private lateinit var hADViewModel: HalalAndDirectoryViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(requireActivity()),
            R.layout.fragment_halal_and_directory_list,
            container,
            false
        )

        init()
        return binding.root
    }

    fun init() {
        hADViewModel =
            ViewModelProvider(requireActivity()).get(HalalAndDirectoryViewModel::class.java)
        hADViewModel.storeData()
        binding.directoryAndHalalVM = hADViewModel
    }
}