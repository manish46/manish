package com.fianz.ui.directory_halal_list.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowHeaderInDirectoryBinding
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.model.get_centers_list.GetCentersCentresItem
import com.fianz.model.get_halal_list.GetHalalCentresItem
import com.fianz.ui.directory.adapter.DirectorySubCenterAdapter

class DirectoryItemAdapter :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var listOfHeaderData = ArrayList<GetCentersCentresItem?>()
    private var listOfHalal = ArrayList<GetHalalCentresItem?>()
    private var type = 0
    private lateinit var sendThreeValueInterface: SendThreeValueInterface

    fun addList(
        listOfHeaderData: ArrayList<GetCentersCentresItem?>,
        listOfHalal: ArrayList<GetHalalCentresItem?>,
        type: Int,
        sendThreeValueInterface: SendThreeValueInterface
    ) {
        this.listOfHeaderData = listOfHeaderData
        this.listOfHalal = listOfHalal
        this.type = type
        this.sendThreeValueInterface = sendThreeValueInterface
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (type == 0) {
            val headerBinding: RowHeaderInDirectoryBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.row_header_in_directory,
                parent,
                false
            )

            return DirectoryViewHolder(headerBinding, sendThreeValueInterface)
        } else {
            val headerBinding: RowHeaderInDirectoryBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.row_header_in_directory,
                parent,
                false
            )

            return HalalViewHolder(headerBinding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is DirectoryViewHolder) {
            holder.bind(listOfHeaderData[position]!!)
        } else if (holder is HalalViewHolder) {
            holder.bind(listOfHalal[position]!!)
        }

    }

    override fun getItemCount(): Int {
        return when (type) {
            0 -> listOfHeaderData.size
            1 -> listOfHalal.size
            else -> 0
        }
    }


    class DirectoryViewHolder(
        private val binding: RowHeaderInDirectoryBinding,
        private val sendThreeValueInterface: SendThreeValueInterface
    ) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        fun bind(data: GetCentersCentresItem) {
            init(data)
            setListener(data)
        }

        private fun init(data: GetCentersCentresItem) {
            binding.txtHIDTitle.text = data.title
            binding.rcyItemList.adapter =
                DirectorySubCenterAdapter(data.centers!!, arrayListOf(), 0)

            if(data.flag){
                binding.rcyItemList.visibility = View.VISIBLE
                binding.imgView.setImageResource(R.drawable.minus_icon)
            }else{
                binding.rcyItemList.visibility = View.GONE
                binding.imgView.setImageResource(R.drawable.pluse_icon)
            }
        }

        private fun setListener(data: GetCentersCentresItem) {
            binding.relTitleView.setOnClickListener{
                if (!data.flag) {
                    binding.rcyItemList.visibility = View.VISIBLE
                    binding.imgView.setImageResource(R.drawable.minus_icon)
                    data.flag = true
                } else {
                    binding.rcyItemList.visibility = View.GONE
                    binding.imgView.setImageResource(R.drawable.pluse_icon)
                    data.flag = false
                }
            }
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.relTitleView -> {

                }
            }
        }
    }

    class HalalViewHolder(private val binding: RowHeaderInDirectoryBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        fun bind(data: GetHalalCentresItem) {
            init(data)
            setListener(data)
        }

        private fun init(data: GetHalalCentresItem) {
            binding.txtHIDTitle.text = data.title
            binding.rcyItemList.adapter =
                DirectorySubCenterAdapter(arrayListOf(), data.centers!!, 1)

            if(data.flag){
                binding.rcyItemList.visibility = View.VISIBLE
                binding.imgView.setImageResource(R.drawable.minus_icon)
            }else{
                binding.rcyItemList.visibility = View.GONE
                binding.imgView.setImageResource(R.drawable.pluse_icon)
            }
        }

        private fun setListener(data: GetHalalCentresItem) {
            binding.relTitleView.setOnClickListener{
                if (!data.flag) {
                    binding.rcyItemList.visibility = View.VISIBLE
                    binding.imgView.setImageResource(R.drawable.minus_icon)
                    data.flag = true
                } else {
                    binding.rcyItemList.visibility = View.GONE
                    binding.imgView.setImageResource(R.drawable.pluse_icon)
                    data.flag = false
                }
            }
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.relTitleView -> {

                }
            }
        }

    }
}