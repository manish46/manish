package com.fianz.ui.faq.view_model

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import com.fianz.R
import com.fianz.api.repositories.FaqRepository
import com.fianz.interfaces.AllApiResponse
import com.fianz.model.get_faq.FaqDataItem
import com.fianz.model.get_faq.FaqResponse
import com.fianz.model.get_halal_list.GetHalalHalalTypesItem
import com.fianz.ui.faq.adapter.FAQAdapter
import com.fianz.utils.CommonUtils
import retrofit2.Response
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class FAQViewModel(application: Application) : AndroidViewModel(application) {

    private var faqAdapter = FAQAdapter()
    private lateinit var listOfData: ArrayList<FaqDataItem?>
    fun callFaqApi(context: Context) {
        if (CommonUtils.isOnlinne(context)) {
            FaqRepository(context, object : AllApiResponse {
                override fun sendResponse(response: Response<*>?) {
                    try {
                        val getResponse = response!!.body() as FaqResponse
                        listOfData = arrayListOf()
                        listOfData = getResponse.data!!
                        setAdapter(listOfData)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }).getFaq()
        } else
            CommonUtils.showToast(context, context.getString(R.string.checkInternet))
    }

    private fun setAdapter(listOfData: ArrayList<FaqDataItem?>) {
        faqAdapter.addList(listOfData)
        faqAdapter.notifyDataSetChanged()
    }

    fun loadPreviousList() {
        if (listOfData.size != 0) {
            setAdapter(listOfData)
        }
    }

    fun seeAdapter(): FAQAdapter {
        return faqAdapter
    }

    fun searchList(text: String) {
        val tempList = arrayListOf<FaqDataItem?>()
        for (d in listOfData) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d?.question!!.toString().lowercase(Locale.getDefault()).contains(text)) {
                tempList.add(d)
            }
        }
        faqAdapter.filterData(tempList)
    }
}