package com.fianz.ui.faq.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.fianz.R
import com.fianz.databinding.FragmentFAQBinding
import com.fianz.ui.faq.view_model.FAQViewModel
import com.fianz.ui.home.HomeActivity
import com.fianz.utils.CommonUtils
import com.fianz.utils.ProgressLoader

class FAQFragment : Fragment() {

    private lateinit var binding: FragmentFAQBinding
    private lateinit var faqVM: FAQViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(requireActivity()),
            R.layout.fragment_f_a_q,
            container,
            false
        )
        init()
        return binding.root
    }

    fun init() {
        faqVM = ViewModelProvider(requireActivity()).get(FAQViewModel::class.java)
        ProgressLoader.showLoader(requireActivity())
        faqVM.callFaqApi(requireActivity())
        binding.faqVM = faqVM

        (requireActivity() as HomeActivity).binding.crdSearchView.setOnClickListener {
            (requireActivity() as HomeActivity).binding.edtAHSearch.setText("")
            (requireActivity() as HomeActivity).binding.edtAHSearch.hint =
                getString(R.string.searchFAQ)
            if ((requireActivity() as HomeActivity).binding.edtAHSearch.visibility == View.GONE) {
                (requireActivity() as HomeActivity).binding.edtAHSearch.visibility = View.VISIBLE
                (requireActivity() as HomeActivity).binding.imgCloseIcon.visibility = View.VISIBLE

                (requireActivity() as HomeActivity).binding.txtActionTitle.visibility = View.GONE
            } else {
                //go in view model

                (requireActivity() as HomeActivity).binding.edtAHSearch.visibility = View.GONE
                (requireActivity() as HomeActivity).binding.imgCloseIcon.visibility = View.GONE
                (requireActivity() as HomeActivity).binding.txtActionTitle.visibility = View.VISIBLE
            }
        }
        (requireActivity() as HomeActivity).binding.imgCloseIcon.setOnClickListener { view ->
            (requireActivity() as HomeActivity).binding.edtAHSearch.visibility = View.GONE
            (requireActivity() as HomeActivity).binding.imgCloseIcon.visibility = View.GONE
            (requireActivity() as HomeActivity).binding.txtActionTitle.visibility = View.VISIBLE
            faqVM.loadPreviousList()
            CommonUtils.hideKeyboard(view)
        }
        (requireActivity() as HomeActivity).binding.edtAHSearch.addTextChangedListener(object :
            TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                faqVM.searchList(s.toString())
            }
        })
    }
}