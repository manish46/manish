package com.fianz.ui.faq.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowFaqBinding
import com.fianz.model.get_faq.FaqDataItem
import com.fianz.model.get_halal_list.GetHalalHalalTypesItem

class FAQAdapter : RecyclerView.Adapter<FAQAdapter.ViewHolder>() {
    private var listOfData = ArrayList<FaqDataItem?>()

    fun addList(listOfData: ArrayList<FaqDataItem?>) {
        this.listOfData = listOfData
    }

    fun filterData(listOf: ArrayList<FaqDataItem?>) {
        this.listOfData = listOf
        notifyDataSetChanged()
    }

    class ViewHolder(val binding: RowFaqBinding) : RecyclerView.ViewHolder(binding.root),
        View.OnClickListener {

        fun bind(data: FaqDataItem) {
            binding.faqM = data
            binding.executePendingBindings()

            init(data)
            setListener()
        }

        private fun init(data: FaqDataItem) = if (!data.hideFlag) {
            showAndHideView(
                binding, true, 270f,
                View.GONE, View.GONE, false
            )
        } else {
            showAndHideView(
                binding, false, 0f,
                View.VISIBLE, View.VISIBLE, true
            )
        }

        private fun setListener() {
            binding.imgFAQArrow.setOnClickListener(this)
            binding.txtFaqQuestion.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.imgFAQArrow -> {
                    binding.txtFaqQuestion.performClick()
                }
                R.id.txtFaqQuestion -> {
                    if (binding.faqM!!.hideFlag)
                        showAndHideView(
                            binding, true, 270f,
                            View.GONE, View.GONE, false
                        )
                    else
                        showAndHideView(
                            binding, false, 0f,
                            View.VISIBLE, View.VISIBLE, true
                        )
                }
            }
        }

        private fun showAndHideView(
            binding: RowFaqBinding,
            singleLineFlag: Boolean,
            imgRotate: Float,
            viewLineVG: Int,
            txtDescriptionVG: Int,
            hideFlag: Boolean
        ) {
            binding.txtFaqQuestion.isSingleLine = singleLineFlag
            binding.imgFAQArrow.rotation = imgRotate
            binding.viewLine.visibility = viewLineVG
            binding.txtFAQDescription.visibility = txtDescriptionVG
            binding.faqM!!.hideFlag = hideFlag
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: RowFaqBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_faq,
            parent,
            false
        )

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listOfData[position]!!)
    }

    override fun getItemCount(): Int {
        return listOfData.size
    }
}