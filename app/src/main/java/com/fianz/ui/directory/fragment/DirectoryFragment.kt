package com.fianz.ui.directory.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.fianz.R
import com.fianz.databinding.FragmentDirectoryBinding
import com.fianz.ui.directory.view_model.DirectoryViewModel
import com.fianz.ui.home.HomeActivity
import com.fianz.utils.CommonUtils

class DirectoryFragment : Fragment() {

    private lateinit var binding: FragmentDirectoryBinding
    private lateinit var directoryVM: DirectoryViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(requireActivity()),
            R.layout.fragment_directory,
            container,
            false
        )

        init()
        setListener()
        return binding.root
    }

    fun init() {
        directoryVM = ViewModelProvider(requireActivity()).get(DirectoryViewModel::class.java)
        directoryVM.getDirectoryData(requireContext(), binding.txtFDTitle)
        binding.directoryVM = directoryVM
        binding.lifecycleOwner = requireActivity()
    }

    fun setListener() {
        (requireActivity() as HomeActivity).binding.sideMenuView.crdFSMApply.setOnClickListener { v ->
            directoryVM.callFilterDirectoryApi(requireActivity())
            (requireActivity() as HomeActivity).binding.drawerHome.closeDrawer(GravityCompat.END)
        }

        (requireActivity() as HomeActivity).binding.sideMenuView.txtFSMReset.setOnClickListener {
            directoryVM.resetFilter(requireActivity())
        }

        (requireActivity() as HomeActivity).binding.sideMenuView.crdFSMCancel.setOnClickListener {
            directoryVM.cancelSelection(requireActivity())
            (requireActivity() as HomeActivity).binding.drawerHome.closeDrawer(GravityCompat.END)
        }

        (requireActivity() as HomeActivity).binding.crdSearchView.setOnClickListener {
            (requireActivity() as HomeActivity).binding.edtAHSearch.setText("")
            (requireActivity() as HomeActivity).binding.edtAHSearch.hint =
                getString(R.string.searchDirectory)
            if ((requireActivity() as HomeActivity).binding.edtAHSearch.visibility == View.GONE) {
                (requireActivity() as HomeActivity).binding.edtAHSearch.visibility = View.VISIBLE
                (requireActivity() as HomeActivity).binding.imgCloseIcon.visibility = View.VISIBLE

                (requireActivity() as HomeActivity).binding.txtActionTitle.visibility = View.GONE
            } else {
                //go in view model
                directoryVM.callSearchApi(
                    requireActivity(),
                    (requireActivity() as HomeActivity).binding.edtAHSearch.text.toString(),
                    binding.txtFDTitle
                )
            }
        }
        (requireActivity() as HomeActivity).binding.imgCloseIcon.setOnClickListener { view ->
            (requireActivity() as HomeActivity).binding.edtAHSearch.visibility = View.GONE
            (requireActivity() as HomeActivity).binding.imgCloseIcon.visibility = View.GONE
            (requireActivity() as HomeActivity).binding.txtActionTitle.visibility = View.VISIBLE
            directoryVM.loadPreviosData()
            CommonUtils.hideKeyboard(view)
        }
    }
}