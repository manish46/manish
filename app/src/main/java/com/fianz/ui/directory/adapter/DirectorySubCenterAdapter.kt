package com.fianz.ui.directory.adapter

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowExpandSubItemDirectoryBinding
import com.fianz.model.get_centers_list.GetCentersCentersItem
import com.fianz.model.get_halal_list.GetHalalCentersItem
import com.fianz.model.get_halal_list.GetHalalCentresItem
import com.fianz.ui.google_map.GoogleMapActivity
import com.squareup.picasso.Picasso
import androidx.core.content.ContextCompat.startActivity
import com.fianz.utils.CommonUtils

class DirectorySubCenterAdapter(
    var listOfData: ArrayList<GetCentersCentersItem?>,
    var listOfHalal: ArrayList<GetHalalCentersItem?>,
    var type: Int
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class DirectoryViewHolder(val binding: RowExpandSubItemDirectoryBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        lateinit var data: GetCentersCentersItem

        fun bind(data: GetCentersCentersItem) {
            this.data = data

            init(data)
            setListener()
        }

        private fun init(data: GetCentersCentersItem) {
            binding.txtESTitleText.text = data.title
            binding.txtESDescText.text = "Client Name :" + data.title
            binding.txtESLocationText.text =
                data.street1 + " " + data.street2 + " " + data.city + " " + data.state + " " + data.country + " " + data.zip
            binding.txtESContactAns.text = data.phone

            if (data.email != null && !data.email.equals("NA")) {
                binding.txtESGmailText.visibility = View.VISIBLE
                binding.imgGmail.visibility = View.VISIBLE
                binding.txtESGmailText.text = data.email
            } else {
                binding.txtESGmailText.visibility = View.GONE
                binding.imgGmail.visibility = View.GONE
            }

            if (data.product != null && !data.product.equals("NA")) {
                binding.txtESGiftText.visibility = View.VISIBLE
                binding.imgGift.visibility = View.VISIBLE
                binding.txtESGiftText.text = data.product
            } else {
                binding.txtESGiftText.visibility = View.GONE
                binding.imgGift.visibility = View.GONE
            }


            if (data.image != null) {
                binding.imgIcon.visibility = View.VISIBLE
                Picasso.get().load(data.image.toString()).into(binding.imgIcon)
            } else {
                binding.imgIcon.visibility = View.GONE
            }
        }

        private fun setListener() {
            binding.txtESContactAns.setOnClickListener(this)
            binding.txtESContactText.setOnClickListener(this)
            binding.crdGetDirection.setOnClickListener(this)
            binding.txtESGmailText.setOnClickListener(this)
            binding.txtESLocationText.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.txtESContactAns -> {
                    val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:09 4412493"))
                    binding.root.context.startActivity(intent)
                }
                R.id.txtESContactText -> {
                    binding.txtESContactAns.performClick()
                }
                R.id.txtESLocationText -> {
                    /* val map =
                         "http://maps.google.co.in/maps?q=${binding.txtESLocationText.text.toString()}"
                     val i = Intent(Intent.ACTION_VIEW, Uri.parse(map))
                     binding.crdGetDirection.context.startActivity(i)*/

                    CommonUtils.openGoogleMap(
                        binding.crdGetDirection.context,
                        binding.txtESLocationText.text.toString()
                    )
                }
                R.id.crdGetDirection -> {
                    /*val intent = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=" + data.latitude + "," + data.longitude + "&daddr=" + data.latitude + "," + data.longitude)
                    )
                    binding.root.context.startActivity(intent)*/
                    CommonUtils.openGoogleMap(
                        binding.crdGetDirection.context,
                        binding.txtESLocationText.text.toString()
                    )
                }
                R.id.txtESGmailText -> {
                    val emailIntent = Intent(Intent.ACTION_SEND).apply {
                        data = Uri.parse("mailto :" + binding.txtESGmailText.text.toString())
                    }
                    v.context.startActivity(Intent.createChooser(emailIntent, "Send feedback"))
//                    CommonUtils.goToGmail(binding.txtESGmailText.context,binding.txtESGmailText.text.toString())
                }
            }
        }
    }

    class HalalViewHolder(val binding: RowExpandSubItemDirectoryBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        lateinit var data: GetHalalCentersItem
        fun bind(data: GetHalalCentersItem) {
            this.data = data

            init(data)
            setListener(data)
        }

        private fun init(data: GetHalalCentersItem) {
            binding.txtESTitleText.text = data.title
            binding.txtESLocationText.text =
                data.street1 + " " + data.street2 + " " + data.city + " " + data.state + " " + data.country + " " + data.zip
            binding.txtESContactAns.text = data.phone

            if (data.email != null && !data.email.equals("NA")) {
                binding.txtESGmailText.visibility = View.VISIBLE
                binding.imgGmail.visibility = View.VISIBLE
                binding.txtESGmailText.text = data.email
            } else {
                binding.txtESGmailText.visibility = View.GONE
                binding.imgGmail.visibility = View.GONE
            }

            if (data.product != null && !data.product.equals("NA")) {
                binding.txtESGiftText.visibility = View.VISIBLE
                binding.imgGift.visibility = View.VISIBLE
                binding.txtESGiftText.text = data.product.toString()
            } else {
                binding.txtESGiftText.visibility = View.GONE
                binding.imgGift.visibility = View.GONE
            }

            if (data.image != null) {
                binding.imgIcon.visibility = View.VISIBLE
                Picasso.get().load(data.image).into(binding.imgIcon)
            } else {
                binding.imgIcon.visibility = View.GONE
            }
        }

        private fun setListener(data: GetHalalCentersItem) {
            binding.txtESContactAns.setOnClickListener(this)
            binding.txtESContactText.setOnClickListener(this)
            binding.crdGetDirection.setOnClickListener(this)
            binding.txtESGmailText.setOnClickListener(this)
            binding.txtESLocationText.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.txtESContactAns -> {
                    val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:09 4412493"))
                    binding.root.context.startActivity(intent)
                }
                R.id.txtESContactText -> {
                    binding.txtESContactAns.performClick()
                }
                R.id.txtESLocationText -> {
                    CommonUtils.openGoogleMap(
                        binding.crdGetDirection.context,
                        binding.txtESLocationText.text.toString()
                    )
                    /*val map =
                        "http://maps.google.co.in/maps?q=${binding.txtESLocationText.text.toString()}"
                    val i = Intent(Intent.ACTION_VIEW, Uri.parse(map))
                    binding.crdGetDirection.context.startActivity(i)*/
                }
                R.id.crdGetDirection -> {
                    CommonUtils.openGoogleMap(
                        binding.crdGetDirection.context,
                        binding.txtESLocationText.text.toString()
                    )
                    /*val intent = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=" + data.latitude + "," + data.longitude + "&daddr=" + data.latitude + "," + data.longitude)
                    )
                    binding.root.context.startActivity(intent)*/
                }
                R.id.txtESGmailText -> {
                    val emailIntent = Intent(Intent.ACTION_SENDTO).apply {
                        data = Uri.parse("mailto" + binding.txtESGmailText.text.toString())
                    }
                    v.context.startActivity(Intent.createChooser(emailIntent, "Send feedback"))
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (type == 0) {
            val binding: RowExpandSubItemDirectoryBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.row_expand_sub_item_directory,
                parent,
                false
            )

            return DirectoryViewHolder(binding)
        } else {
            val binding: RowExpandSubItemDirectoryBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.row_expand_sub_item_directory,
                parent,
                false
            )

            return HalalViewHolder(binding)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is DirectoryViewHolder) {
            holder.bind(listOfData[position]!!)
        } else if (holder is HalalViewHolder) {
            holder.bind(listOfHalal[position]!!)
        }
    }

    override fun getItemCount(): Int {
        return when (type) {
            0 -> listOfData.size
            1 -> listOfHalal.size
            else -> 0
        }
    }
}