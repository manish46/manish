package com.fianz.ui.directory.dialog_fragment.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.fianz.R
import com.fianz.databinding.FragmentDirectoryOptionBinding
import com.fianz.interfaces.SendSingleValueInterface
import com.fianz.model.get_centers_list.GetCentersCentreTypesItem
import com.fianz.ui.directory.dialog_fragment.view_model.DirectoryOptionViewModel
import com.fianz.utils.CommonUtils

class DirectoryOptionFragment(
    var centreTypes: ArrayList<GetCentersCentreTypesItem?>,
    var sendSingleValueInterface: SendSingleValueInterface
) : DialogFragment() {

    private lateinit var binding: FragmentDirectoryOptionBinding
    private lateinit var directoryVM: DirectoryOptionViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(requireActivity()),
            R.layout.fragment_directory_option,
            container,
            false
        )

        init()
        setListener()
        return binding.root
    }

    fun init() {
        directoryVM = ViewModelProvider(requireActivity()).get(DirectoryOptionViewModel::class.java)
        directoryVM.storeData(dialog!!, centreTypes, sendSingleValueInterface)
        binding.directoryOptionVM = directoryVM

        binding.edtFDTitle.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                directoryVM.takeCharacter(s.toString())
            }

        })
    }

    fun setListener() {
        binding.imgClose.setOnClickListener {
            dialog?.dismiss()
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
        CommonUtils.setCustomeWidhtAndHeightDialog(requireActivity(), dialog!!, 0.90f, 0.50f)
    }
}