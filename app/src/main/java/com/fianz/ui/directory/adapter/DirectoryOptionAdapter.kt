package com.fianz.ui.directory.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.binder_class.loadWithPlaceHolderImages
import com.fianz.databinding.RowDirectoryOptionBinding
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.model.get_centers_list.GetCentersCentreTypesItem
import com.squareup.picasso.Picasso
import java.lang.Exception

class DirectoryOptionAdapter : RecyclerView.Adapter<DirectoryOptionAdapter.ViewHolder>() {

    private var centreTypes = ArrayList<GetCentersCentreTypesItem?>()
    private lateinit var sendThreeValueInterface: SendThreeValueInterface

    companion object {
        private var previousPos = 0
        private var currentPos = 0
    }

    fun refreshAdapter(previousPos: Int, currentPos: Int) {
        centreTypes[previousPos]!!.flag = false
        centreTypes[currentPos]!!.flag = true
        notifyItemChanged(previousPos)
        notifyItemChanged(currentPos)
    }

    fun addList(
        centreTypes: ArrayList<GetCentersCentreTypesItem?>,
        sendThreeValueInterface: SendThreeValueInterface
    ) {
        this.centreTypes = centreTypes
        this.sendThreeValueInterface = sendThreeValueInterface
    }

    fun filterData(listOf: ArrayList<GetCentersCentreTypesItem?>) {
        centreTypes = listOf
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val bind: RowDirectoryOptionBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_directory_option, parent, false
        )

        return ViewHolder(bind, sendThreeValueInterface)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(centreTypes[position]!!, position)
    }

    override fun getItemCount(): Int {
        return centreTypes.size
    }

    class ViewHolder(
        var binding: RowDirectoryOptionBinding,
        private var sendThreeValueInterface: SendThreeValueInterface
    ) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        fun bind(data: GetCentersCentreTypesItem, position: Int) {

            binding.directoryOptionM = data
            binding.executePendingBindings()

            init(binding, position)
            setListener(binding)
        }

        private fun init(binding: RowDirectoryOptionBinding, position: Int) {

            if (binding.directoryOptionM!!.flag) {
                binding.imgDownArrow.visibility = View.VISIBLE
                currentPos = position
            } else {
                binding.imgDownArrow.visibility = View.INVISIBLE
            }

            try{
                if (!binding.directoryOptionM!!.image!!.isEmpty()) {
                    Picasso.get().load(binding.directoryOptionM!!.image!!)
                        .placeholder(R.drawable.christ_img).into(binding.imgSchool)
                } else {
                    binding.imgSchool.setImageResource(R.drawable.all_icon)
                }
            }catch (e:Exception){
                e.printStackTrace()
            }
        }

        private fun setListener(binding: RowDirectoryOptionBinding) {
            binding.imgSchool.setOnClickListener(this)
            binding.txtRDOTitle.setOnClickListener(this)
            binding.crdRDOParentView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.imgSchool -> {
                    if (!binding.directoryOptionM!!.flag) {
                        previousPos = currentPos
                        currentPos = layoutPosition

                        sendThreeValueInterface.actionFire(layoutPosition, previousPos, currentPos)
                    }
                }
                R.id.txtRDOTitle -> {
                    binding.imgSchool.performClick()
                }
                R.id.crdRDOParentView -> {
                    binding.imgSchool.performClick()
                }
            }
        }
    }
}