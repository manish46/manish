package com.fianz.ui.directory.view_model

import android.app.Application
import android.content.Context
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.GravityCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.fianz.R
import com.fianz.api.repositories.DirectoryRepository
import com.fianz.interfaces.AllApiResponse
import com.fianz.interfaces.SendSingleValueInterface
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.model.get_centers_list.*
import com.fianz.model.store_selected_item.SelectedItems
import com.fianz.ui.directory.dialog_fragment.fragment.DirectoryOptionFragment
import com.fianz.ui.directory_halal_list.adapter.DirectoryItemAdapter
import com.fianz.ui.home.HomeActivity
import com.fianz.ui.side_menu.adapter.FilterSideMenuAdapter
import com.fianz.utils.CommonUtils
import com.fianz.utils.ProgressLoader
import org.w3c.dom.Text
import retrofit2.Response

class DirectoryViewModel(application: Application) : AndroidViewModel(application) {

    lateinit var centreTypes: ArrayList<GetCentersCentreTypesItem?>
    lateinit var centresList: ArrayList<GetCentersCentresItem?>
    private var directoryAdapter: DirectoryItemAdapter = DirectoryItemAdapter()
    private var filterSideMenuAdapter: FilterSideMenuAdapter = FilterSideMenuAdapter()
    private lateinit var list: ArrayList<SelectedItems>
    private var centerTypeId = ""
    var visibleNoRecord: MutableLiveData<Int> = MutableLiveData()
    var visibleRecyclerView: MutableLiveData<Int> = MutableLiveData()
    lateinit var listOfRegions: ArrayList<GetCentersRegionsItem?>

    fun getDirectoryData(context: Context, txtFDTitle: TextView) {
        if (CommonUtils.isOnlinne(context)) {
            ProgressLoader.showLoader(context)
            DirectoryRepository(context, object : AllApiResponse {
                override fun sendResponse(response: Response<*>?) {
                    ProgressLoader.closeLoader()
                    try {
                        val getResponse = response!!.body() as GetCenterListResponse
                        centreTypes = getResponse.data!!.centreTypes!!
                        centresList = getResponse.data.centres!!

                        centreTypes.add(0, GetCentersCentreTypesItem(0, "All", "", true))
                        txtFDTitle.text = centreTypes[0]?.title
                        setAdapter(centresList)
                        setFilterData(context, getResponse.data.regions!!)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }).callDirectoryApi()
        } else
            CommonUtils.showToast(context, context.getString(R.string.checkInternet))
    }

    private fun setFilterData(context: Context, listOfRegions: ArrayList<GetCentersRegionsItem?>) {
        this.listOfRegions = listOfRegions
        list = arrayListOf()

        filterSideMenuAdapter.addList(
            listOfRegions,
            arrayListOf(),
            0,
            object : SendThreeValueInterface {
                override fun actionFire(position: Any, firstValue: Any, secondValue: Any) {

                    list.add(SelectedItems(position as Int, firstValue as Int))
                }
            })
        filterSideMenuAdapter.notifyDataSetChanged()

        (context as HomeActivity).binding.sideMenuView.rcyFSM.adapter =
            filterSideMenuAdapter
    }

    fun loadPreviosData() {
        if (centresList.size != 0) {
            setAdapter(centresList)
        }
    }

    fun showView(v: View, imgView: ImageView, txtView: TextView) {
        val directoryOptionFragment =
            DirectoryOptionFragment(centreTypes, object : SendSingleValueInterface {
                override fun sendPosition(position: Any) {
                    txtView.text = centreTypes[position as Int]!!.title
                    centerTypeId = centreTypes[position]!!.id.toString()
                    callApi(centreTypes[position]!!.id, v.context)
                }
            })
        directoryOptionFragment.show(
            (v.context as HomeActivity).supportFragmentManager,
            ""
        )
    }

    private fun callApi(id: Int?, context: Context) {
        if (CommonUtils.isOnlinne(context)) {
            ProgressLoader.showLoader(context)
            DirectoryRepository(context, object : AllApiResponse {
                override fun sendResponse(response: Response<*>?) {
                    ProgressLoader.closeLoader()
                    try {
                        val getResponse = response!!.body() as GetCenterListResponse
                        centresList = getResponse.data!!.centres!!

                        setAdapter(centresList)
                        setFilterData(context, getResponse.data.regions!!)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }).callCenterTypeDirectoryApi(id!!)
        } else
            CommonUtils.showToast(context, context.getString(R.string.checkInternet))
    }

    fun callFilterDirectoryApi(context: Context) {
        if (CommonUtils.isOnlinne(context)) {
            ProgressLoader.showLoader(context)

            getAllSelectedItem(context)

        } else
            CommonUtils.showToast(context, context.getString(R.string.checkInternet))
    }

    private fun getAllSelectedItem(context: Context) {
        val listOfData = arrayListOf<Int>()
        if (listOfRegions.size != 0) {
            listOfRegions.forEachIndexed { _, getCentersRegionsItem ->
                if (getCentersRegionsItem?.subregions != null) {
                    getCentersRegionsItem.subregions.forEachIndexed { j, getCentersSubregionsItem ->
                        if (getCentersSubregionsItem?.flag!!) {
                            listOfData.add(getCentersSubregionsItem.id!!)
                        }
                    }
                }
            }
        }
        callFilterApi(listOfData, context)
    }

    private fun callFilterApi(listOfData: ArrayList<Int>, context: Context) {
        list = arrayListOf()
        DirectoryRepository(context, object : AllApiResponse {
            override fun sendResponse(response: Response<*>?) {
                ProgressLoader.closeLoader()
                try {
                    val getResponse = response!!.body() as GetCenterListResponse
                    centresList = getResponse.data?.centres!!

                    setAdapter(centresList)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }).callFilterTypeDirectoryApi(listOfData, centerTypeId)
    }

    fun resetFilter(context: Context) {
        ProgressLoader.showLoader(context)
        list = arrayListOf()

        if (listOfRegions.size != 0) {
            for (i in 0 until listOfRegions.size) {
                if (listOfRegions[i]?.subregions != null) {
                    if (listOfRegions[i]?.subregions?.size != 0) {
                        for (j in 0 until listOfRegions[i]?.subregions?.size!!) {
                            if (listOfRegions[i]?.subregions?.get(j)?.flag!!) {
                                listOfRegions[i]?.subregions?.get(j)?.flag = false
                            }
                        }
                    }
                }
            }
            ProgressLoader.closeLoader()
        }
        filterSideMenuAdapter.notifyDataSetChanged()
    }

    fun cancelSelection(context: Context) {
        ProgressLoader.showLoader(context)

        list.forEachIndexed { index, selectedItems ->
            listOfRegions.get(selectedItems.regions)?.subregions?.get(selectedItems.subRegions)?.flag =
                !listOfRegions.get(selectedItems.regions)?.subregions?.get(selectedItems.subRegions)?.flag!!
        }
        list = arrayListOf()
        ProgressLoader.closeLoader()

        filterSideMenuAdapter.notifyDataSetChanged()
    }

    fun showFilterView(v: View) {
        if ((v.context as HomeActivity).binding.drawerHome.isDrawerOpen(GravityCompat.END))
            (v.context as HomeActivity).binding.drawerHome.closeDrawer(GravityCompat.END)
        else
            (v.context as HomeActivity).binding.drawerHome.openDrawer(GravityCompat.END)
    }

    private fun setAdapter(centresList: ArrayList<GetCentersCentresItem?>) {
        if (centresList.size != 0) {
            visibleNoRecord.value = View.GONE
            visibleRecyclerView.value = View.VISIBLE

            directoryAdapter.addList(
                centresList,
                arrayListOf(),
                0,
                object : SendThreeValueInterface {
                    override fun actionFire(position: Any, firstValue: Any, secondValue: Any) {
//                directoryAdapter.refreshList(position as Int, firstValue as Int)
                    }
                })
            directoryAdapter.notifyDataSetChanged()

        } else {
            visibleNoRecord.value = View.VISIBLE
            visibleRecyclerView.value = View.GONE
        }
    }

    fun seeList(): DirectoryItemAdapter {
        return directoryAdapter
    }

    fun callSearchApi(context: Context, text: String, txtFDTitle: TextView) {
        if (text.length < 3) {
            CommonUtils.showToast(context, context.getString(R.string.news))
            return
        }

        if (CommonUtils.isOnlinne(context)) {
            ProgressLoader.showLoader(context)
            DirectoryRepository(context, object : AllApiResponse {
                override fun sendResponse(response: Response<*>?) {
                    ProgressLoader.closeLoader()
                    try {
                        val getResponse = response!!.body() as GetCenterListResponse
                        centreTypes = getResponse.data!!.centreTypes!!
                        centresList = getResponse.data.centres!!

                        centreTypes.add(0, GetCentersCentreTypesItem(0, "All", "", true))
                        txtFDTitle.text = centreTypes[0]?.title
                        setAdapter(centresList)
                        setFilterData(context, getResponse.data.regions!!)

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }).callCenterList(text)
        } else
            CommonUtils.showToast(context, context.getString(R.string.checkInternet))
    }
}