package com.fianz.ui.directory.dialog_fragment.view_model

import android.app.Application
import android.app.Dialog
import androidx.lifecycle.AndroidViewModel
import com.fianz.interfaces.SendSingleValueInterface
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.model.get_centers_list.GetCentersCentreTypesItem
import com.fianz.ui.directory.adapter.DirectoryOptionAdapter
import java.util.*
import kotlin.collections.ArrayList


class DirectoryOptionViewModel(application: Application) : AndroidViewModel(application) {

    private var directoryOptionAdapter: DirectoryOptionAdapter = DirectoryOptionAdapter()
    private lateinit var centreTypes: ArrayList<GetCentersCentreTypesItem?>
    fun storeData(
        dialog: Dialog,
        centreTypes: ArrayList<GetCentersCentreTypesItem?>,
        sendSingleValueInterface: SendSingleValueInterface
    ) {
        setAdapter(dialog, centreTypes, sendSingleValueInterface)
    }

    private fun setAdapter(
        dialog: Dialog,
        centreTypes: ArrayList<GetCentersCentreTypesItem?>,
        sendSingleValueInterface: SendSingleValueInterface
    ) {
        this.centreTypes = arrayListOf()
        this.centreTypes = centreTypes

        directoryOptionAdapter.addList(centreTypes, object : SendThreeValueInterface {
            override fun actionFire(position: Any, firstValue: Any, secondValue: Any) {
                directoryOptionAdapter.refreshAdapter(firstValue as Int, secondValue as Int)
                sendSingleValueInterface.sendPosition(position as Int)
                dialog.dismiss()
            }
        })
        directoryOptionAdapter.notifyDataSetChanged()
    }

    fun seeList(): DirectoryOptionAdapter {
        return directoryOptionAdapter
    }

    fun takeCharacter(text: String) {
        val tempList = arrayListOf<GetCentersCentreTypesItem?>()
        for (d in centreTypes) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d?.title!!.toString().lowercase(Locale.getDefault()).contains(text)) {
                tempList.add(d)
            }
        }
        directoryOptionAdapter.filterData(tempList)
    }
    /*@BindingAdapter("onTextChange")
    fun onTextChange(edtView: EditText) {
        edtView.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                Log.e("kop", "kop")
            }

        })
    }*/
}