package com.fianz.ui.side_menu.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowFilterHeaderBinding
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.model.get_centers_list.GetCentersRegionsItem
import com.fianz.model.get_halal_list.GetHalalRegionsItem

class FilterSideMenuAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var listOfData = ArrayList<GetCentersRegionsItem?>()
    private var listOfHalalData = ArrayList<GetHalalRegionsItem?>()
    private var type = 0
    private lateinit var sendThreeValueInterface: SendThreeValueInterface

    fun addList(
        listOfData: ArrayList<GetCentersRegionsItem?>,
        listOfHalalData: ArrayList<GetHalalRegionsItem?>,
        type: Int,
        sendThreeValueInterface: SendThreeValueInterface
    ) {
        this.listOfData = listOfData
        this.listOfHalalData = listOfHalalData
        this.type = type
        this.sendThreeValueInterface = sendThreeValueInterface
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (type == 0) {
            val headerBind: RowFilterHeaderBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.row_filter_header,
                parent,
                false
            )
            return DirectoryViewHolder(headerBind, sendThreeValueInterface)
        } else {
            val headerBind: RowFilterHeaderBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.row_filter_header,
                parent,
                false
            )
            return HalalViewHolder(headerBind, sendThreeValueInterface)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is DirectoryViewHolder) {
            holder.bind(listOfData[position]!!)
        } else if (holder is HalalViewHolder) {
            holder.bind(listOfHalalData[position]!!)
        }
    }

    override fun getItemCount(): Int {
        return when (type) {
            0 -> listOfData.size
            1 -> listOfHalalData.size
            else -> 0
        }
    }

    class DirectoryViewHolder(
        val binding: RowFilterHeaderBinding,
        val sendThreeValueInterface: SendThreeValueInterface
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: GetCentersRegionsItem) {
            init(data)
            setListener(data)
        }

        private fun init(data: GetCentersRegionsItem) {
            binding.txtRFHTitle.text = data.title

            if (!data.flag) {
                binding.rcyFHSubRegion.visibility = View.GONE
                binding.imgRFHArrow.rotation = 180f
            } else {
                binding.rcyFHSubRegion.visibility = View.VISIBLE
                binding.imgRFHArrow.rotation = 270f
            }

            val filterSubRegion = FilterSubRegionAdapter()
            filterSubRegion.addList(
                data.subregions!!,
                arrayListOf(),
                0,
                object : SendThreeValueInterface {
                    override fun actionFire(position: Any, firstValue: Any, secondValue: Any) {
                        sendThreeValueInterface.actionFire(layoutPosition, firstValue as Int, "")
                    }
                })
            filterSubRegion.notifyDataSetChanged()
            binding.rcyFHSubRegion.adapter = filterSubRegion
        }

        private fun setListener(data: GetCentersRegionsItem) {
            binding.txtRFHTitle.setOnClickListener {
                if (!data.flag) {
                    data.flag = true
                    binding.rcyFHSubRegion.visibility = View.VISIBLE
                    binding.imgRFHArrow.rotation = 270f
                } else {
                    data.flag = false
                    binding.rcyFHSubRegion.visibility = View.GONE
                    binding.imgRFHArrow.rotation = 180f
                }
            }

        }
    }

    class HalalViewHolder(
        val binding: RowFilterHeaderBinding,
        val sendThreeValueInterface: SendThreeValueInterface
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: GetHalalRegionsItem) {
            init(data)
            setListener(data)
        }

        private fun init(data: GetHalalRegionsItem) {
            binding.txtRFHTitle.text = data.title

            binding.txtRFHTitle.text = data.title

            if (!data.flag) {
                binding.rcyFHSubRegion.visibility = View.GONE
                binding.imgRFHArrow.rotation = 180f
            } else {
                binding.rcyFHSubRegion.visibility = View.VISIBLE
                binding.imgRFHArrow.rotation = 270f
            }


            val filterSubRegion = FilterSubRegionAdapter()
            filterSubRegion.addList(
                arrayListOf(),
                data.subregions!!,
                1,
                object : SendThreeValueInterface {
                    override fun actionFire(position: Any, firstValue: Any, secondValue: Any) {
                        sendThreeValueInterface.actionFire(layoutPosition, firstValue as Int, "")
                    }
                })
            filterSubRegion.notifyDataSetChanged()
            binding.rcyFHSubRegion.adapter = filterSubRegion
        }

        private fun setListener(data: GetHalalRegionsItem) {
            binding.txtRFHTitle.setOnClickListener {
                if (!data.flag) {
                    data.flag = true
                    binding.rcyFHSubRegion.visibility = View.VISIBLE
                    binding.imgRFHArrow.rotation = 270f
                } else {
                    data.flag = false
                    binding.rcyFHSubRegion.visibility = View.GONE
                    binding.imgRFHArrow.rotation = 180f
                }
            }
        }
    }

}