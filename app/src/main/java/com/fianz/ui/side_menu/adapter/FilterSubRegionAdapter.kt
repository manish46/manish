package com.fianz.ui.side_menu.adapter

import android.content.res.ColorStateList
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowFilterItemBinding
import com.fianz.interfaces.SendSingleValueInterface
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.model.get_centers_list.GetCentersSubregionsItem
import com.fianz.model.get_halal_list.GetHalalSubregionsItem

class FilterSubRegionAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var listOfData = ArrayList<GetCentersSubregionsItem?>()
    var listOfSubRegionData = ArrayList<GetHalalSubregionsItem?>()
    private lateinit var sendThreeValueInterface: SendThreeValueInterface
    private var type = 0
    fun addList(
        listOfData: ArrayList<GetCentersSubregionsItem?>,
        listOfSubRegionData: ArrayList<GetHalalSubregionsItem?>,
        type: Int,
        sendSingleValueInterface: SendThreeValueInterface
    ) {
        this.listOfData = listOfData
        this.listOfSubRegionData = listOfSubRegionData
        this.type = type
        this.sendThreeValueInterface = sendSingleValueInterface
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (type) {
            0 -> {
                val binding: RowFilterItemBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context), R.layout.row_filter_item, parent, false
                )

                return DirectoryViewHolder(binding, sendThreeValueInterface)
            }
            else -> {
                val binding: RowFilterItemBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context), R.layout.row_filter_item, parent, false
                )

                return HalalViewHolder(binding, sendThreeValueInterface)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is DirectoryViewHolder) {
            holder.bind(listOfData[position]!!)
        } else if (holder is HalalViewHolder) {
            holder.bind(listOfSubRegionData[position]!!)
        }

    }

    override fun getItemCount(): Int {
        return when (type) {
            0 -> listOfData.size
            1 -> listOfSubRegionData.size
            else -> 0
        }
    }

    class DirectoryViewHolder(
        val binding: RowFilterItemBinding,
        val sendThreeValueInterface: SendThreeValueInterface
    ) : RecyclerView.ViewHolder(binding.root),
        View.OnClickListener {
        fun bind(data: GetCentersSubregionsItem) {
            init(data)
            setListner(data)
        }

        private fun init(data: GetCentersSubregionsItem) {
            binding.txtRFITitle.text = data.title
            binding.chkRFI.isChecked = data.flag

            binding.chkRFI.buttonTintList = ColorStateList.valueOf(
                ContextCompat.getColor(
                    binding.chkRFI.context,
                    R.color.border_gray
                )
            )
        }

        private fun setListner(data: GetCentersSubregionsItem) {
            binding.txtRFITitle.setOnClickListener(this)
            binding.chkRFI.setOnCheckedChangeListener(object :
                CompoundButton.OnCheckedChangeListener {
                override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                    data.flag = isChecked
                    sendThreeValueInterface.actionFire(0,layoutPosition,"")
                }
            })
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.txtRFITitle -> {
                    binding.chkRFI.performClick()
                }
            }
        }
    }

    class HalalViewHolder(
        val binding: RowFilterItemBinding,
        val sendThreeValueInterface: SendThreeValueInterface
    ) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        fun bind(data: GetHalalSubregionsItem) {

            init(data)
            setListner(data)
        }

        private fun init(data: GetHalalSubregionsItem) {
            binding.txtRFITitle.text = data.title
            binding.chkRFI.isChecked = data.flag

            binding.chkRFI.buttonTintList = ColorStateList.valueOf(
                ContextCompat.getColor(
                    binding.chkRFI.context,
                    R.color.border_gray
                )
            )
        }

        private fun setListner(data: GetHalalSubregionsItem) {
            binding.txtRFITitle.setOnClickListener(this)
            binding.chkRFI.setOnCheckedChangeListener(object :
                CompoundButton.OnCheckedChangeListener {
                override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                    data.flag = isChecked
                    sendThreeValueInterface.actionFire(0,layoutPosition,"")
                }
            })
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.txtRFITitle -> {
                    binding.chkRFI.performClick()
                }
            }
        }
    }
}