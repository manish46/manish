package com.fianz.ui.splash.view_model

import android.app.Application
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.fianz.R
import com.fianz.api.repositories.SplashActivityRepository
import com.fianz.interfaces.AllApiResponse
import com.fianz.ui.home.HomeActivity
import com.fianz.ui.splash.SplashActivity
import com.fianz.ui.walk_through.WalkThrough
import com.fianz.utils.CommonUtils
import com.fianz.utils.Preference
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import retrofit2.Response
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class SplashActivityViewModel(application: Application) : AndroidViewModel(application) {

    fun callDeviceApi(context: Context) {
        if (CommonUtils.isOnlinne(context)) {
            SplashActivityRepository(context, object : AllApiResponse {
                override fun sendResponse(response: Response<*>?) {
                    try {
                        if (response != null) {
                            if (Preference(context).getBoolen(Preference.WALK_THROUGH)) {
                                context.startActivity(Intent(context, HomeActivity::class.java))
                            } else {
                                context.startActivity(Intent(context, WalkThrough::class.java))
                            }
                            (context as SplashActivity).finish()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }).getAddDeviceInformation()
        } else
            CommonUtils.showToast(context, context.getString(R.string.checkInternet))
    }
}