package com.fianz.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.fianz.R
import com.fianz.databinding.ActivitySplashBinding
import com.fianz.ui.splash.view_model.SplashActivityViewModel
import com.fianz.ui.walk_through.WalkThrough

class SplashActivity : AppCompatActivity() {

    lateinit var binding: ActivitySplashBinding
    lateinit var splashVM: SplashActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)

        init()
    }

    fun init() {
        splashVM = ViewModelProvider(this).get(SplashActivityViewModel::class.java)
        splashVM.callDeviceApi(this)
        binding.splashVM = splashVM
    }
}