package com.fianz.ui.google_map.manage_response

interface TaskLoadedCallback {
    fun onTaskDone(vararg values: Any?)
}