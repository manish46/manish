package com.fianz.ui.google_map

import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.fianz.R
import com.fianz.ui.google_map.manage_response.FetchUrl
import com.fianz.ui.google_map.manage_response.TaskLoadedCallback
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import java.util.jar.Manifest

class GoogleMapActivity : AppCompatActivity(), OnMapReadyCallback, TaskLoadedCallback {

    lateinit var map: GoogleMap
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    var sourceLatitute = 0.0
    var sourceLogitute = 0.0

    var destinationLatitute = 0.0
    var destinationLogitute = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_google_map)

        init()
    }

    fun init() {
        destinationLatitute = intent.getDoubleExtra("latitute", 0.0)
        destinationLogitute = intent.getDoubleExtra("logitute", 0.0)

        Log.e("destinationLatitute", destinationLatitute.toString())
        Log.e("destinationLogitute", destinationLogitute.toString())
        val mapFrag: SupportMapFragment =
            supportFragmentManager.findFragmentById(R.id.frgMap) as SupportMapFragment
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        if (checkPermissions()) {
            getLastLocations()
        } else {
            requestPermission()
        }
        mapFrag.getMapAsync(this)
    }

    private fun checkPermissions(): Boolean {
        val location = ContextCompat.checkSelfPermission(
            this,
            android.Manifest.permission.ACCESS_FINE_LOCATION
        )
        return location == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
            100
        )
    }

    fun getLastLocations() {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }

        fusedLocationProviderClient.lastLocation.addOnCompleteListener(this) { task ->
            if (task.isSuccessful && task.result != null) {
                val lastlocation = task.result
                sourceLatitute = lastlocation.latitude
                sourceLogitute = lastlocation.longitude
                Log.e("sourceLatitute", sourceLatitute.toString())
                Log.e("sourceLogitute", sourceLogitute.toString())

                val sourceLatlong = LatLng(sourceLatitute, sourceLogitute)
                val destiLatlng = LatLng(destinationLatitute, destinationLogitute)

                val sourcePlace = MarkerOptions().position(sourceLatlong).title("souce")
                val destiPlace = MarkerOptions().position(destiLatlng).title("desti")

                map.addMarker(sourcePlace)
                map.addMarker(destiPlace)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(sourcePlace.position, 12f))
                map.isMyLocationEnabled = true
                map.uiSettings.isCompassEnabled = true
                map.uiSettings.isIndoorLevelPickerEnabled = true
                map.uiSettings.isMapToolbarEnabled = true
                map.uiSettings.isMyLocationButtonEnabled = true
                map.uiSettings.isRotateGesturesEnabled = true
                map.uiSettings.isScrollGesturesEnabled = true
                map.uiSettings.isScrollGesturesEnabledDuringRotateOrZoom = true
                map.uiSettings.isZoomControlsEnabled = true
                map.uiSettings.isZoomGesturesEnabled = true
                map.uiSettings.setAllGesturesEnabled(true)
                map.isIndoorEnabled = true
                map.isBuildingsEnabled = true
                map.isTrafficEnabled = true

                val url = getUrl(sourcePlace.position, destiPlace.position, "driving")
                FetchUrl(this).execute(url.toString(), "driving")
            }
        }
    }

    override fun onMapReady(p0: GoogleMap) {
        map = p0
    }

    private fun getUrl(sourcePos: LatLng, destiPlace: LatLng, msg: String): Any {
        val souce = "origin=" + sourcePos.latitude + "," + sourcePos.longitude
        val desti = "destination=" + destiPlace.latitude + "," + destiPlace.longitude

        val mode = "mode=" + msg
        val parameter = souce + "&" + desti + "&" + mode
        val output = "json"
        val url =
            "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameter + "&key=" + getString(
                R.string.google_map_key
            )

        return url
    }

    override fun onTaskDone(vararg values: Any?) {
        map.addPolyline(values[0] as PolylineOptions)
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            100 -> {
                if (grantResults.size > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        getLastLocations()
                    }
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}