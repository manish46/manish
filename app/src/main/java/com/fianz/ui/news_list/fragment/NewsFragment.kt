package com.fianz.ui.news_list.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.fianz.R
import com.fianz.databinding.FragmentNewsBinding
import com.fianz.ui.home.HomeActivity
import com.fianz.ui.news_list.view_model.NewsListViewModel

class NewsFragment : Fragment() {
    lateinit var binding: FragmentNewsBinding
    lateinit var newsViewModel: NewsListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(requireActivity()),
            R.layout.fragment_news,
            container,
            false
        )
        init()
        setListener()
        return binding.root
    }

    fun init() {
        newsViewModel = ViewModelProvider(this).get(NewsListViewModel::class.java)
        newsViewModel.getNewsList(requireActivity(),binding.pagerIndicator,binding.rcyBanner)
        newsViewModel.startTimer(binding.rcyBanner)
        newsViewModel.manageRecyclerViewTouch(binding.rcyBanner)

        binding.newsListVM = newsViewModel
    }

    fun setListener() {
        (requireActivity() as HomeActivity).binding.crdSearchView.setOnClickListener {
            newsViewModel.manageCardViewClick(requireActivity())

        }
        (requireActivity() as HomeActivity).binding.imgCloseIcon.setOnClickListener {
            newsViewModel.manageimgCloseClick(requireActivity(),binding.pagerIndicator,binding.rcyBanner)
        }
        (requireActivity() as HomeActivity).binding.edtAHSearch.addTextChangedListener(object :
            TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                newsViewModel.searchList(s.toString())
            }
        })
    }
}