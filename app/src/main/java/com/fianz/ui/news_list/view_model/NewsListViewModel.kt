package com.fianz.ui.news_list.view_model

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.content.Intent
import android.view.MotionEvent
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.api.repositories.NewsListRepository
import com.fianz.interfaces.AllApiResponse
import com.fianz.interfaces.SendSingleValueInterface
import com.fianz.model.get_news_list.GetNewsDataItem
import com.fianz.model.get_news_list.GetNewsResponse
import com.fianz.ui.home.HomeActivity
import com.fianz.ui.news_detail.NewsDetail
import com.fianz.ui.news_list.adapter.NewsAdapter
import com.fianz.ui.news_list.adapter.NewsViewPagerListAdapter
import com.fianz.utils.CommonUtils
import com.fianz.utils.ProgressLoader
import com.fianz.utils.pager_indicator.PageIndicator
import retrofit2.Response
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class NewsListViewModel(application: Application) : AndroidViewModel(application) {
    var newsAdapter = NewsAdapter()
    var newsViewPagerListAdapter = NewsViewPagerListAdapter()
    private lateinit var timers: Timer
    private var num = -1
    var listOfData = ArrayList<GetNewsDataItem?>()
    var touchFlag: Boolean = false

    fun getNewsList(context: Context, pagerIndicator: PageIndicator, rcyBanner: RecyclerView) {
        if (CommonUtils.isOnlinne(context)) {
            ProgressLoader.showLoader(context)
            NewsListRepository(context, object : AllApiResponse {
                override fun sendResponse(response: Response<*>?) {
                    ProgressLoader.closeLoader()
                    try {
                        val getResponse = response?.body() as GetNewsResponse
                        listOfData = getResponse.data!!
                        setAdapter(context, listOfData)
                        setNewsAdapter(listOfData,pagerIndicator,rcyBanner)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }).callNewsList(context)
        } else
            CommonUtils.showToast(context, context.getString(R.string.checkInternet))
    }

    private fun setAdapter(context: Context, listOfData: ArrayList<GetNewsDataItem?>) {
        newsAdapter.addList(listOfData, object : SendSingleValueInterface {
            override fun sendPosition(position: Any) {
                context.startActivity(
                    Intent(context, NewsDetail::class.java).putExtra(
                        "sendData", listOfData[position as Int]
                    )
                )
            }

        })
        newsAdapter.notifyDataSetChanged()
    }

    fun startTimer(view: RecyclerView) {
        timers = Timer()
        timers.schedule(object : TimerTask() {
            override fun run() {
                if (!touchFlag) {
                    num += 1
                    if (num >= listOfData.size) {
                        num = 0
                    }
                    view.smoothScrollToPosition(num)
                } else {
                    view.smoothScrollToPosition(0)
                    timers.cancel()
                }
            }
        }, 0, 2000)
    }

    @SuppressLint("ClickableViewAccessibility")
    fun manageRecyclerViewTouch(rcyBanner: RecyclerView) {
        rcyBanner.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        touchFlag = true
                    }
                }
                return v?.onTouchEvent(event) ?: true
            }
        })
    }

    override fun onCleared() {
        super.onCleared()
        timers.cancel()
    }

    fun manageCardViewClick(context: Context) {
        (context as HomeActivity).binding.edtAHSearch.hint =
            context.getString(R.string.searchNews)
        if (context.binding.edtAHSearch.visibility == View.GONE) {
            context.binding.edtAHSearch.visibility = View.VISIBLE
            context.binding.imgCloseIcon.visibility = View.VISIBLE
            context.binding.viewCover.alpha = 1F
            context.binding.imgAHLogo.visibility = View.GONE

            context.binding.txtActionTitle.visibility = View.GONE
        }
    }

    fun manageimgCloseClick(
        context: Context,
        pagerIndicator: PageIndicator,
        rcyBanner: RecyclerView
    ) {
        (context as HomeActivity).binding.edtAHSearch.setText("")
        context.binding.edtAHSearch.visibility = View.GONE
        context.binding.imgCloseIcon.visibility = View.GONE
        context.binding.txtActionTitle.visibility = View.GONE
        context.binding.imgAHLogo.visibility = View.VISIBLE
        context.binding.viewCover.alpha = 0F

        newsViewPagerListAdapter.addList(listOfData)
        newsViewPagerListAdapter.notifyDataSetChanged()
        CommonUtils.hideKeyboard(context.binding.edtAHSearch)
    }

    fun seeList(): NewsAdapter {
        return newsAdapter
    }

    private fun setNewsAdapter(
        listOfPager: ArrayList<GetNewsDataItem?>,
        pagerIndicator: PageIndicator,
        rcyBanner: RecyclerView
    ) {
        newsViewPagerListAdapter.addList(listOfPager)
        newsViewPagerListAdapter.notifyDataSetChanged()

        PagerSnapHelper().attachToRecyclerView(rcyBanner)
        pagerIndicator attachTo rcyBanner
    }

    fun searchList(text: String) {
        val tempList = arrayListOf<GetNewsDataItem?>()
        for (d in listOfData) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d?.title!!.toString().lowercase(Locale.getDefault()).contains(text)) {
                tempList.add(d)
            }
        }
        newsAdapter.filterData(tempList)
    }

    fun seeNewsPagerAdapter(): NewsViewPagerListAdapter {
        return newsViewPagerListAdapter
    }

}