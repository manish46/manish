package com.fianz.ui.news_list.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowNewsListBinding
import com.fianz.interfaces.SendSingleValueInterface
import com.fianz.model.get_news_list.GetNewsDataItem

class NewsAdapter :
    RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    var listOfData = ArrayList<GetNewsDataItem?>()
    private lateinit var sendSingleValueInterface: SendSingleValueInterface

    fun addList(
        listOfData: ArrayList<GetNewsDataItem?>,
        sendSingleValueInterface: SendSingleValueInterface
    ) {
        this.listOfData = listOfData
        this.sendSingleValueInterface = sendSingleValueInterface
    }

    fun filterData(listOf: ArrayList<GetNewsDataItem?>) {
        this.listOfData = listOf
        notifyDataSetChanged()
    }

    class ViewHolder(
        var binding: RowNewsListBinding,
        var sendSingleValueInterface: SendSingleValueInterface
    ) : RecyclerView.ViewHolder(binding.root),
        View.OnClickListener {
        fun bind(data: GetNewsDataItem) {
            binding.newsM = data
            binding.executePendingBindings()

            setListener()
        }

        fun setListener() {
            binding.txtRNLReadMore.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.txtRNLReadMore -> {
                    sendSingleValueInterface.sendPosition(layoutPosition)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val bind: RowNewsListBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_news_list, parent, false
        )

        return ViewHolder(bind, sendSingleValueInterface)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listOfData[position]!!)
    }

    override fun getItemCount(): Int {
        return listOfData.size
    }
}