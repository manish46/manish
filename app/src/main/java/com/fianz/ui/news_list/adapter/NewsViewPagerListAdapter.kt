package com.fianz.ui.news_list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowNewsHeaderListBinding
import com.fianz.model.get_news_list.GetNewsDataItem

class NewsViewPagerListAdapter() :
    RecyclerView.Adapter<NewsViewPagerListAdapter.ViewHolder>() {
    var listOfData = ArrayList<GetNewsDataItem?>()

    fun addList(listOfData: ArrayList<GetNewsDataItem?>) {
        this.listOfData = listOfData
    }

    class ViewHolder(val binding: RowNewsHeaderListBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(data: GetNewsDataItem) {
            binding.newsListVPM = data
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: RowNewsHeaderListBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(
                    parent.context
                ),
                R.layout.row_news_header_list,
                parent, false
            )

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listOfData[position]!!)
    }

    override fun getItemCount(): Int {
        return listOfData.size
    }
}