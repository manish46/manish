package com.fianz.ui.halal.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.fianz.R
import com.fianz.databinding.FragmentHalalBinding
import com.fianz.ui.halal.view_model.HalalViewModel
import com.fianz.ui.home.HomeActivity
import com.fianz.utils.CommonUtils

class HalalFragment : Fragment() {

    private lateinit var binding: FragmentHalalBinding
    private lateinit var halalVM: HalalViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(requireActivity()),
            R.layout.fragment_halal,
            container,
            false
        )

        init()
        return binding.root
    }

    private fun init() {
        halalVM = ViewModelProvider(requireActivity()).get(HalalViewModel::class.java)
        halalVM.callHalalList(requireActivity())
        binding.halalVM = halalVM

        (requireActivity() as HomeActivity).binding.crdSearchView.setOnClickListener {
            (requireActivity() as HomeActivity).binding.edtAHSearch.setText("")
            (requireActivity() as HomeActivity).binding.edtAHSearch.hint =
                getString(R.string.searchHalal)
            if ((requireActivity() as HomeActivity).binding.edtAHSearch.visibility == View.GONE) {
                (requireActivity() as HomeActivity).binding.edtAHSearch.visibility = View.VISIBLE
                (requireActivity() as HomeActivity).binding.imgCloseIcon.visibility = View.VISIBLE

                (requireActivity() as HomeActivity).binding.txtActionTitle.visibility = View.GONE
            } else {
                //go in view model
                (requireActivity() as HomeActivity).binding.edtAHSearch.visibility = View.GONE
                (requireActivity() as HomeActivity).binding.imgCloseIcon.visibility = View.GONE
                (requireActivity() as HomeActivity).binding.txtActionTitle.visibility = View.VISIBLE
            }
        }
        (requireActivity() as HomeActivity).binding.imgCloseIcon.setOnClickListener { v ->
            (requireActivity() as HomeActivity).binding.edtAHSearch.visibility = View.GONE
            (requireActivity() as HomeActivity).binding.imgCloseIcon.visibility = View.GONE
            (requireActivity() as HomeActivity).binding.txtActionTitle.visibility = View.VISIBLE
            halalVM.loadPreviousData(requireActivity())
            CommonUtils.hideKeyboard(v)
        }
        (requireActivity() as HomeActivity).binding.edtAHSearch.addTextChangedListener(object :
            TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                halalVM.searchList(s.toString())
            }
        })
    }
}