package com.fianz.ui.halal.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowHalalBinding
import com.fianz.interfaces.SendSingleValueInterface
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.model.get_centers_list.GetCentersCentreTypesItem
import com.fianz.model.get_halal_list.GetHalalHalalTypesItem

class HalalAdapter : RecyclerView.Adapter<HalalAdapter.ViewHolder>() {

    private var listOfHalal = ArrayList<GetHalalHalalTypesItem?>()
    private lateinit var sendThreeValueInterface: SendThreeValueInterface

    fun addList(
        listOfHalal: ArrayList<GetHalalHalalTypesItem?>,
        sendThreeValueInterface: SendThreeValueInterface
    ) {
        this.listOfHalal = listOfHalal
        this.sendThreeValueInterface = sendThreeValueInterface
    }

    fun filterData(listOf: ArrayList<GetHalalHalalTypesItem?>) {
        this.listOfHalal = listOf
        notifyDataSetChanged()
    }

    class ViewHolder(val binding: RowHalalBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: GetHalalHalalTypesItem, sendThreeValueInterface: SendThreeValueInterface) {
            binding.halalM = data
            binding.executePendingBindings()

            setListener(sendThreeValueInterface)
        }

        fun setListener(sendThreeValueInterface: SendThreeValueInterface) {
            binding.relhalalParent.setOnClickListener {
                sendThreeValueInterface.actionFire(binding.halalM!!.id.toString(),binding.halalM!!.title.toString(),"")
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: RowHalalBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_halal, parent, false
        )

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listOfHalal[position]!!, sendThreeValueInterface)
    }

    override fun getItemCount(): Int {
        return listOfHalal.size
    }
}