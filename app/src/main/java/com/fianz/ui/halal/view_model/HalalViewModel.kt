package com.fianz.ui.halal.view_model

import android.app.Application
import android.content.Context
import androidx.core.os.bundleOf
import androidx.lifecycle.AndroidViewModel
import com.fianz.R
import com.fianz.api.repositories.HalalListRepository
import com.fianz.interfaces.AllApiResponse
import com.fianz.interfaces.SendSingleValueInterface
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.model.get_centers_list.GetCentersCentreTypesItem
import com.fianz.model.get_halal_list.GetHalalHalalTypesItem
import com.fianz.model.get_halal_list.GetHalalResponse
import com.fianz.ui.halal.adapter.HalalAdapter
import com.fianz.ui.home.HomeActivity
import com.fianz.utils.CommonUtils
import com.fianz.utils.ProgressLoader
import retrofit2.Response
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class HalalViewModel(application: Application) : AndroidViewModel(application) {

    private var halalAdapter: HalalAdapter = HalalAdapter()
    private lateinit var halalTypes: ArrayList<GetHalalHalalTypesItem?>
    fun callHalalList(context: Context) {
        if (CommonUtils.isOnlinne(context)) {
            ProgressLoader.showLoader(context)
            HalalListRepository(context, object : AllApiResponse {
                override fun sendResponse(response: Response<*>?) {
                    ProgressLoader.closeLoader()
                    try {
                        parseHalalResponse(response, context)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }).callHalalList()
        } else
            CommonUtils.showToast(context, context.getString(R.string.checkInternet))
    }

    private fun parseHalalResponse(response: Response<*>?, context: Context) {
        val getResponse = response?.body() as GetHalalResponse
        if (getResponse != null) {
            this.halalTypes = arrayListOf()
            this.halalTypes = getResponse.data?.halalTypes!!
            setAdapter(halalTypes, context)
        }
    }

    private fun setAdapter(
        halalTypes: ArrayList<GetHalalHalalTypesItem?>?,
        activityContext: Context
    ) {
        halalAdapter.addList(halalTypes!!, object : SendThreeValueInterface {
            override fun actionFire(position: Any, firstValue: Any, secondValue: Any) {
                val bundle = bundleOf()
                bundle.putString("id", position.toString())
                bundle.putString("title", firstValue.toString())
                (activityContext as HomeActivity).navControll.navigate(R.id.fragHalalDetail, bundle)
            }

        })
        halalAdapter.notifyDataSetChanged()
    }

    fun seeHalalData(): HalalAdapter {
        return halalAdapter
    }

    fun loadPreviousData(context: Context) {
        if (halalTypes.size != 0) {
            setAdapter(halalTypes, context)
        }
    }

    fun searchList(text: String) {
        val tempList = arrayListOf<GetHalalHalalTypesItem?>()
        for (d in halalTypes) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d?.title!!.toString().lowercase(Locale.getDefault()).contains(text)) {
                tempList.add(d)
            }
        }
        halalAdapter.filterData(tempList)
    }
}