package com.fianz.ui.walk_through.view_model

import android.app.Application
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.fianz.R
import com.fianz.model.WalkThroughModel
import com.fianz.ui.home.HomeActivity
import com.fianz.ui.walk_through.WalkThrough
import com.fianz.ui.walk_through.adapter.WalkThroughAdapter
import com.fianz.utils.Preference
import java.util.*
import kotlin.collections.ArrayList

class WalkThroughViewModel(application: Application) : AndroidViewModel(application) {
    private lateinit var listOfData: ArrayList<WalkThroughModel>
    private var walkThroughAdapter: WalkThroughAdapter = WalkThroughAdapter()
    var timerCount: MutableLiveData<Int> = MutableLiveData()
    var text: MutableLiveData<String> = MutableLiveData()
    var textDescription: MutableLiveData<String> = MutableLiveData()
    private lateinit var timers: Timer
    private var num = -1

    fun storeData(context: Context) {
        listOfData = arrayListOf()

        listOfData.add(
            WalkThroughModel(
                R.drawable.rect_walk_through,
                context.getString(R.string.getPrayerTiming),
                context.getString(R.string.getLocalPrayer)
            )
        )
        listOfData.add(
            WalkThroughModel(
                R.drawable.halal_img,
                context.getString(R.string.halalDirectory),
                context.getString(R.string.informationDirection)
            )
        )
        listOfData.add(
            WalkThroughModel(
                R.drawable.news_img,
                context.getString(R.string.fianzLatestNews),
                context.getString(R.string.getLatestNews)
            )
        )
        storeDataInAdapter(listOfData)
    }

    fun storeDataInAdapter(listOfData: ArrayList<WalkThroughModel>) {
        walkThroughAdapter.addListOfWalk(listOfData)
        walkThroughAdapter.notifyDataSetChanged()
    }

    override fun onCleared() {
        super.onCleared()
        timers.cancel()
    }

    fun startTimer() {
        timers = Timer()
        timers.schedule(object : TimerTask() {
            override fun run() {
                num += 1
                if (num >= listOfData.size) {
                    num = 0
                }
                timerCount.postValue(num)
                text.postValue(listOfData[num].titleText)
                textDescription.postValue(listOfData[num].textDescription)
            }
        }, 0, 2000)
    }

    fun getDataInAdapter(): WalkThroughAdapter {
        return walkThroughAdapter
    }

    fun clickOnGetStart(view: View) {
        view.isEnabled = false
        Preference(view.context).setFlag(Preference.WALK_THROUGH, true)
        view.context.startActivity(Intent(view.context, HomeActivity::class.java))
        (view.context as WalkThrough).finish()
    }
}