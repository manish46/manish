package com.fianz.ui.walk_through.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowWalkThroughBinding
import com.fianz.model.WalkThroughModel

class WalkThroughAdapter:
    RecyclerView.Adapter<WalkThroughAdapter.ViewHolder>() {

    private var listOfData = ArrayList<WalkThroughModel>()
    fun addListOfWalk(listOfData: ArrayList<WalkThroughModel>) {
        this.listOfData = listOfData
    }

    class ViewHolder(var binding: RowWalkThroughBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(walkThroughModel: WalkThroughModel) {
            binding.walkThroughM = walkThroughModel
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: RowWalkThroughBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_walk_through, parent, false
        )

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listOfData[position])
    }

    override fun getItemCount(): Int {
        return listOfData.size
    }
}