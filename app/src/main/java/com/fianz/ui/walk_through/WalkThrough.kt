package com.fianz.ui.walk_through

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.fianz.R
import com.fianz.databinding.ActivityWalkThroughBinding
import com.fianz.ui.home.HomeActivity
import com.fianz.ui.walk_through.view_model.WalkThroughViewModel
import com.fianz.utils.Preference

class WalkThrough : AppCompatActivity() {

    lateinit var binding: ActivityWalkThroughBinding
    private lateinit var walkThroughViewModel: WalkThroughViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_walk_through)

        init()
        changeViewPager()
    }

    private fun changeViewPager() {
        walkThroughViewModel.timerCount.observe(this,
            { t -> binding.viewPg.setCurrentItem(t!!, true) })

        walkThroughViewModel.text.observe(this, object : Observer<String> {
            override fun onChanged(t: String?) {
                binding.txtTitleText.text = t.toString()
            }
        })
        walkThroughViewModel.textDescription.observe(this, object : Observer<String> {
            override fun onChanged(t: String?) {
                binding.txtDescription.text = t.toString()
            }
        })
    }

    private fun init() {
        walkThroughViewModel =
            ViewModelProvider(
                this
            ).get(WalkThroughViewModel::class.java)
        walkThroughViewModel.storeData(this)
        walkThroughViewModel.startTimer()

        binding.viewPg.isUserInputEnabled = false
        binding.walkThroughVM = walkThroughViewModel
    }
}