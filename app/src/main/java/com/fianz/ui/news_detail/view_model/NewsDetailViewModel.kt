package com.fianz.ui.news_detail.view_model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.fianz.model.get_news_list.GetNewsDataItem

class NewsDetailViewModel(application: Application) : AndroidViewModel(application) {

    var getImage: String = ""
    var getTitle: String = ""
    var getDes: String = ""

    fun getData(data: GetNewsDataItem) {
        getImage = data.image!!
        getTitle = data.title!!
        getDes = data.description!!
    }
}