package com.fianz.ui.news_detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.fianz.R
import com.fianz.databinding.ActivityNewsDetailBinding
import com.fianz.model.get_news_list.GetNewsDataItem
import com.fianz.ui.news_detail.view_model.NewsDetailViewModel

class NewsDetail : AppCompatActivity() {

    private lateinit var binding: ActivityNewsDetailBinding
    private lateinit var newsDetailVM: NewsDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_news_detail)

        init()
        setListener()
    }

    fun init() {
        newsDetailVM = ViewModelProvider(this).get(NewsDetailViewModel::class.java)
        val getData = intent?.getSerializableExtra("sendData")
        newsDetailVM.getData(getData as GetNewsDataItem)
        binding.newsDetailVM = newsDetailVM
    }

    fun setListener() {

    }
}