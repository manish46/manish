package com.fianz.ui.load_pdf

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.fianz.R
import com.fianz.databinding.ActivityLoadPdfImageBinding
import com.fianz.ui.load_pdf.view_model.LoadPdfImageViewModel

class LoadPdfImage : AppCompatActivity() {
    lateinit var binding: ActivityLoadPdfImageBinding
    lateinit var loadPdfInVM: LoadPdfImageViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_load_pdf_image)

        init()
    }

    fun init() {
        loadPdfInVM = ViewModelProvider(this).get(LoadPdfImageViewModel::class.java)
        val url = intent.getStringExtra("URL")
        loadPdfInVM.loadUrlInWebView(url!!,binding.webView,binding.proLoader)
        binding.loadPdfInVM = loadPdfInVM
    }
}