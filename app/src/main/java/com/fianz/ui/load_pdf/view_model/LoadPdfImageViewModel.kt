package com.fianz.ui.load_pdf.view_model

import android.app.Application
import android.view.View
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import androidx.lifecycle.AndroidViewModel

class LoadPdfImageViewModel(application: Application) : AndroidViewModel(application) {

    fun loadUrlInWebView(url: String, web_content: WebView, proLoader: ProgressBar) {

        web_content.settings.javaScriptEnabled = true
        web_content.settings.builtInZoomControls = false
        web_content.settings.displayZoomControls = false
        web_content.settings.builtInZoomControls = true
        web_content.settings.displayZoomControls = false
        web_content.isVerticalScrollBarEnabled = false
        web_content.isHorizontalScrollBarEnabled = false
        web_content.isScrollbarFadingEnabled = true
        web_content.scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY

        if (url.contains(".png") || url.contains(".jpg") || url.contains(".jpeg")) {
            web_content.loadUrl(url)
        } else
            web_content.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url=$url")

        var isPageShown = false

        val webViewClient: WebViewClient = object : WebViewClient() {
            override fun onPageFinished(webView: WebView, url1: String) {
                if (!(url.contains(".png") || url.contains(".jpg") || url.contains(".jpeg"))) {
                    /*web_content.loadUrl(
                        "javascript:(function() { " +
                                "document.querySelector('[role=\"toolbar\"]').remove();})()"
                    )*/

                    /*web_content.loadUrl("javascript:(function() { " +
                            "document.getElementsByClassName('ndfHFb-c4YZDc-GSQQnc-LgbsSe ndfHFb-c4YZDc-to915-LgbsSe VIpgJd-TzA9Ye-eEGnhe ndfHFb-c4YZDc-LgbsSe')[0].style.display='none'; })()")*/

                    web_content.loadUrl(
                        "javascript:(function() { " +
                                "document.getElementsByClassName('ndfHFb-c4YZDc-GSQQnc-LgbsSe ndfHFb-c4YZDc-to915-LgbsSe VIpgJd-TzA9Ye-eEGnhe ndfHFb-c4YZDc-LgbsSe')[0].style.display='none';" +
                                "document.getElementsByClassName('ndfHFb-c4YZDc-Wrql6b')[0].setAttribute('style','width:0px');})()")

                    proLoader.visibility = View.GONE
                    if (!isPageShown) {
                        web_content.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url=$url")
                    } else {
                        proLoader.visibility = View.GONE
                    }
                }
            }

            override fun onPageCommitVisible(view: WebView?, url: String?) {
                proLoader.visibility = View.GONE
                isPageShown = true
                super.onPageCommitVisible(view, url)
            }

            override fun onReceivedError(
                view: WebView?,
                request: WebResourceRequest?,
                error: WebResourceError?
            ) {
                if (!(url.contains(".png") || url.contains(".jpg") || url.contains(".jpeg"))) {
                    view!!.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url=$url")
                }
            }
        }
        web_content.webViewClient = webViewClient
    }
}