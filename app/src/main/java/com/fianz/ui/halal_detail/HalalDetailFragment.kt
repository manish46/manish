package com.fianz.ui.halal_detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.fianz.R
import com.fianz.databinding.FragmentHalalDetailBinding
import com.fianz.ui.halal_detail.view_model.HalalDetailViewModel
import com.fianz.ui.home.HomeActivity

class HalalDetailFragment : Fragment() {
    private lateinit var binding: FragmentHalalDetailBinding
    private lateinit var halalDetailVM: HalalDetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(requireActivity()),
            R.layout.fragment_halal_detail,
            container,
            false
        )
        init()
        setListener()

        return binding.root
    }

    fun init() {
        halalDetailVM = ViewModelProvider(this).get(HalalDetailViewModel::class.java)
        val id = arguments?.getString("id")
        val title = arguments?.getString("title")
        binding.txtHDTitle.text = title!!
        halalDetailVM.callHalalDetail(requireActivity(), id!!)
        binding.halalDetailVM = halalDetailVM
        binding.lifecycleOwner = this
    }

    fun setListener() {
        (requireActivity() as HomeActivity).binding.sideMenuView.crdFSMApply.setOnClickListener {
            halalDetailVM.callHalalFilterApi(requireActivity())
            (requireActivity() as HomeActivity).binding.drawerHome.closeDrawer(GravityCompat.END)
        }

        (requireActivity() as HomeActivity).binding.sideMenuView.txtFSMReset.setOnClickListener {
            halalDetailVM.resetFilter(requireActivity())
        }
        (requireActivity() as HomeActivity).binding.sideMenuView.crdFSMCancel.setOnClickListener {
            halalDetailVM.cancelSelection(requireActivity())
            (requireActivity() as HomeActivity).binding.drawerHome.closeDrawer(GravityCompat.END)
        }
      /*  binding.imgLeftArrow.setOnClickListener {
            requireActivity().onBackPressed()
        }*/
    }
}
/*
override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
): View? {
    binding = DataBindingUtil.inflate(
        LayoutInflater.from(requireActivity()),
        R.layout.activity_halal_detail,
        container,
        false
    )
    return binding.root
}*/
