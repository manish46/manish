package com.fianz.ui.halal_detail.view_model

import android.app.Application
import android.content.Context
import android.media.Image
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.fianz.R
import com.fianz.api.repositories.DirectoryRepository
import com.fianz.api.repositories.HalalDetailRepository
import com.fianz.interfaces.AllApiResponse
import com.fianz.interfaces.SendSingleValueInterface
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.model.get_centers_list.GetCenterListResponse
import com.fianz.model.get_halal_list.GetHalalCentresItem
import com.fianz.model.get_halal_list.GetHalalHalalTypesItem
import com.fianz.model.get_halal_list.GetHalalRegionsItem
import com.fianz.model.get_halal_list.GetHalalResponse
import com.fianz.model.store_selected_item.SelectedItems
import com.fianz.ui.directory_halal_list.adapter.DirectoryItemAdapter
import com.fianz.ui.home.HomeActivity
import com.fianz.ui.side_menu.adapter.FilterSideMenuAdapter
import com.fianz.utils.CommonUtils
import com.fianz.utils.ProgressLoader
import retrofit2.Response
import java.lang.Exception

class HalalDetailViewModel(application: Application) : AndroidViewModel(application) {
    var visibleHDNoRecord: MutableLiveData<Int> = MutableLiveData()
    var visibleHDRecyclerView: MutableLiveData<Int> = MutableLiveData()

    fun callHalalDetail(context: Context, id: String) {
        halalTypeId = id
        if (CommonUtils.isOnlinne(context)) {
            ProgressLoader.showLoader(context)
            HalalDetailRepository(context, object : AllApiResponse {
                override fun sendResponse(response: Response<*>?) {
                    ProgressLoader.closeLoader()
                    try {
                        listOfRegion = arrayListOf()
                        val getResponse = response?.body() as GetHalalResponse
                        listOfRegion = getResponse.data?.regions!!
                        listOfCenter = getResponse.data.centres!!
                        listOfCenterType = getResponse.data.halalTypes!!

                        setAdapter(context, listOfRegion)
                        setCentersData(listOfCenter)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }).callHalalDetailApi(id)
        } else
            CommonUtils.showToast(context, context.getString(R.string.checkInternet))
    }

    private fun setCentersData(listOfCenter: ArrayList<GetHalalCentresItem?>) {
        if (listOfCenter.size == 0) {
            visibleHDNoRecord.value = View.VISIBLE
            visibleHDRecyclerView.value = View.GONE

        } else {
            visibleHDNoRecord.value = View.GONE
            visibleHDRecyclerView.value = View.VISIBLE
            setCenterAdapter(listOfCenter)
        }
    }

    fun seeList(): DirectoryItemAdapter {
        return directoryITem
    }

    fun showFilter(v: View) {
        if ((v.context as HomeActivity).binding.drawerHome.isDrawerOpen(GravityCompat.END))
            (v.context as HomeActivity).binding.drawerHome.closeDrawer(GravityCompat.END)
        else
            (v.context as HomeActivity).binding.drawerHome.openDrawer(GravityCompat.END)
    }

    fun setAdapter(context: Context, listOfRegion: ArrayList<GetHalalRegionsItem?>) {
        list = arrayListOf()

        filterSideMenuAdapter.addList(
            arrayListOf(),
            listOfRegion,
            1,
            object : SendThreeValueInterface {
                override fun actionFire(position: Any, firstValue: Any, secondValue: Any) {
                    list.add(SelectedItems(position as Int, firstValue as Int))
                }
            })
        filterSideMenuAdapter.notifyDataSetChanged()
        (context as HomeActivity).binding.sideMenuView.rcyFSM.adapter = filterSideMenuAdapter
    }

    fun seeAdapter(): FilterSideMenuAdapter {
        return filterSideMenuAdapter
    }

    fun callHalalFilterApi(context: Context) {
        if (CommonUtils.isOnlinne(context)) {
            ProgressLoader.showLoader(context)
            getAllSelectedItem(context)
        } else
            CommonUtils.showToast(context, context.getString(R.string.checkInternet))
    }

    private fun getAllSelectedItem(context: Context) {
        val listOfData = arrayListOf<Int>()
        if (listOfRegion.size != 0) {
            listOfRegion.forEachIndexed { _, getCentersRegionsItem ->
                if (getCentersRegionsItem?.subregions != null) {
                    getCentersRegionsItem.subregions.forEachIndexed { j, getCentersSubregionsItem ->
                        if (getCentersSubregionsItem?.flag!!) {
                            listOfData.add(getCentersSubregionsItem.id!!)
                        }
                    }
                }
            }
        }
        callFilterApi(listOfData, context)
    }

    private fun callFilterApi(listOfData: ArrayList<Int>, context: Context) {
        list = arrayListOf()

        HalalDetailRepository(context, object : AllApiResponse {
            override fun sendResponse(response: Response<*>?) {
                ProgressLoader.closeLoader()
                try {
                    val getResponse = response!!.body() as GetHalalResponse
                    if (getResponse.data?.centres!!.size != 0) {
                        visibleHDNoRecord.value = View.GONE
                        visibleHDRecyclerView.value = View.VISIBLE

                        setCenterAdapter(getResponse.data.centres)
                    } else {
                        visibleHDNoRecord.value = View.VISIBLE
                        visibleHDRecyclerView.value = View.GONE
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }).callFilterHalalList(listOfData, halalTypeId)
    }

    fun resetFilter(context: Context) {
        ProgressLoader.showLoader(context)
        list = arrayListOf()

        if (listOfRegion.size != 0) {
            for (i in 0 until listOfRegion.size) {
                if (listOfRegion[i]?.subregions != null) {
                    if (listOfRegion[i]?.subregions?.size != 0) {
                        for (j in 0 until listOfRegion[i]?.subregions?.size!!) {
                            if (listOfRegion[i]?.subregions?.get(j)?.flag!!) {
                                listOfRegion[i]?.subregions?.get(j)?.flag = false
                            }
                        }
                    }
                }
            }
            ProgressLoader.closeLoader()
        }
        filterSideMenuAdapter.notifyDataSetChanged()
    }

    private fun setCenterAdapter(centres: ArrayList<GetHalalCentresItem?>) {
        directoryITem.addList(
            arrayListOf(),
            centres,
            1,
            object : SendThreeValueInterface {
                override fun actionFire(
                    position: Any,
                    firstValue: Any,
                    secondValue: Any
                ) {
                }
            })
        directoryITem.notifyDataSetChanged()
    }

    /* fun callHalalFilterApi(context: Context) {
         if (CommonUtils.isOnlinne(context)) {
             ProgressLoader.showLoader(context)
             HalalDetailRepository(context, object : AllApiResponse {
                 override fun sendResponse(response: Response<*>?) {
                     ProgressLoader.closeLoader()
                     try {
                         val getResponse = response!!.body() as GetHalalResponse
                         if (getResponse.data?.centres!!.size != 0) {
                             visibleHDNoRecord.value = View.GONE
                             visibleHDRecyclerView.value = View.VISIBLE

                             directoryITem.addList(
                                 arrayListOf(),
                                 getResponse.data.centres,
                                 1,
                                 object : SendThreeValueInterface {
                                     override fun actionFire(
                                         position: Any,
                                         firstValue: Any,
                                         secondValue: Any
                                     ) {
                                     }
                                 })
                             directoryITem.notifyDataSetChanged()

                         } else {
                             visibleHDNoRecord.value = View.VISIBLE
                             visibleHDRecyclerView.value = View.GONE
                         }

                     } catch (e: Exception) {
                         e.printStackTrace()
                     }
                 }
             }).callFilterHalalList(list, halalTypeId)
         } else
             CommonUtils.showToast(context, context.getString(R.string.checkInternet))
     }*/

    fun searchHalalDetail(
        v: View,
        textTitle: TextView,
        imgCloseIcon: ImageView,
        imgLeftArrow: ImageView,
        edtSearchView: EditText
    ) {
        if (edtSearchView.visibility == View.VISIBLE) {
            callSearchApi(v.context, edtSearchView.text.toString().trim())
        } else {
            edtSearchView.visibility = View.VISIBLE
            textTitle.visibility = View.GONE
            imgCloseIcon.visibility = View.VISIBLE
            imgLeftArrow.visibility = View.GONE
        }
        CommonUtils.hideKeyboard(v)
    }

    fun closeView(
        v: View,
        textTitle: TextView,
        imgLeftArrow: ImageView,
        edtSearchView: EditText
    ) {
        CommonUtils.hideKeyboard(v)
        edtSearchView.visibility = View.GONE
        textTitle.visibility = View.VISIBLE
        v.visibility = View.GONE
        imgLeftArrow.visibility = View.VISIBLE
    }

    fun cancelSelection(context: Context) {
        ProgressLoader.showLoader(context)

        list.forEachIndexed { index, selectedItems ->
            listOfRegion.get(selectedItems.regions)?.subregions?.get(selectedItems.subRegions)?.flag =
                !listOfRegion.get(selectedItems.regions)?.subregions?.get(selectedItems.subRegions)?.flag!!
        }
        list = arrayListOf()
        ProgressLoader.closeLoader()

        filterSideMenuAdapter.notifyDataSetChanged()
    }

    fun onBackPress(v: View) {
        (v.context as HomeActivity).onBackPressed()
    }

    private fun callSearchApi(context: Context, text: String) {
        if (text.length < 3) {
            CommonUtils.showToast(context, context.getString(R.string.pleaseEnterSearchData))
            return
        }
        if (CommonUtils.isOnlinne(context)) {
            ProgressLoader.showLoader(context)
            HalalDetailRepository(context, object : AllApiResponse {
                override fun sendResponse(response: Response<*>?) {
                    ProgressLoader.closeLoader()
                    try {
                        listOfRegion = arrayListOf()
                        val getResponse = response?.body() as GetHalalResponse
                        listOfRegion = getResponse.data?.regions!!
                        listOfCenter = getResponse.data.centres!!
                        listOfCenterType = getResponse.data.halalTypes!!

                        setAdapter(context, listOfRegion)
                        setCentersData(listOfCenter)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }).callHalalDetailSearchApi(halalTypeId, text)
        } else
            CommonUtils.showToast(context, context.getString(R.string.checkInternet))
    }

    companion object {
        private var filterSideMenuAdapter: FilterSideMenuAdapter = FilterSideMenuAdapter()
        private var listOfRegion = ArrayList<GetHalalRegionsItem?>()
        private var listOfCenter = ArrayList<GetHalalCentresItem?>()
        private var listOfCenterType = ArrayList<GetHalalHalalTypesItem?>()
        private var directoryITem: DirectoryItemAdapter = DirectoryItemAdapter()
        private lateinit var list: ArrayList<SelectedItems>
        var halalTypeId = ""
    }
}