package com.fianz.ui.home.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowSideMenuBinding
import com.fianz.model.HomeSideMenuModel
import com.fianz.interfaces.SendThreeValueInterface

class SideMenuAdapter : RecyclerView.Adapter<SideMenuAdapter.ViewHolder>() {

    private var homeSideMenuList = ArrayList<HomeSideMenuModel>()
    private lateinit var sendThreeValueInterface: SendThreeValueInterface

    companion object {
        private var currentPos = 0
        private var previousPos = 0
    }

    fun addListData(
        homeSideMenuList: ArrayList<HomeSideMenuModel>,
        sendThreeValueInterface: SendThreeValueInterface
    ) {
        this.homeSideMenuList = homeSideMenuList
        this.sendThreeValueInterface = sendThreeValueInterface
    }

    fun refreshAdapter(previousPos:Int,currentPos:Int){
        homeSideMenuList[previousPos].clickFlag = false
        homeSideMenuList[currentPos].clickFlag  = true

        notifyItemChanged(previousPos)
        notifyItemChanged(currentPos)
    }
    fun allFalse(previousPos:Int,currentPos:Int){
        homeSideMenuList[previousPos].clickFlag = false
        homeSideMenuList[currentPos].clickFlag  = false

        notifyItemChanged(previousPos)
        notifyItemChanged(currentPos)
    }

    class ViewHolder(
        val binding: RowSideMenuBinding,
        private val sendThreeValueInterface: SendThreeValueInterface
    ) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        fun bind(data: HomeSideMenuModel) {
            binding.homeSideMenuM = data
            binding.executePendingBindings()

            init(data)
            setListener()
        }

        private fun init(data: HomeSideMenuModel) {
            if (data.clickFlag) {
                currentPos = layoutPosition
                binding.relSMParent.setBackgroundResource(R.drawable.row_side_menu_bg)
                binding.txtSideMenuText.setTextColor(Color.WHITE)
            } else {
                binding.relSMParent.setBackgroundResource(0)
                binding.txtSideMenuText.setTextColor(
                    ContextCompat.getColor(
                        binding.imgRightSideIcon.context,
                        R.color.green_63f
                    )
                )
            }
        }

        private fun setListener() {
            binding.relSMParent.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.relSMParent -> {
                    if (!binding.homeSideMenuM!!.clickFlag) {

                        previousPos = currentPos
                        currentPos = layoutPosition

                        sendThreeValueInterface.actionFire(layoutPosition, previousPos, currentPos)
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: RowSideMenuBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_side_menu, parent, false
        )

        return ViewHolder(binding, sendThreeValueInterface)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(homeSideMenuList[position])
    }

    override fun getItemCount(): Int {
        return homeSideMenuList.size
    }
}