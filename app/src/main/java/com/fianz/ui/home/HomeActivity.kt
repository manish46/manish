package com.fianz.ui.home

import android.content.Intent
import android.content.IntentSender
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.fianz.R
import com.fianz.databinding.ActivityHomeBinding
import com.fianz.ui.home.view_model.HomeViewModel
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsResponse
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import java.lang.Exception


class HomeActivity : AppCompatActivity() {

    lateinit var binding: ActivityHomeBinding
    lateinit var homeViewModel: HomeViewModel
    lateinit var navControll: NavController
    lateinit var navHostFragment: NavHostFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)

        inits()
    }

    private fun inits() {
        navHostFragment =
            supportFragmentManager.findFragmentById(R.id.navHomeFrag) as NavHostFragment
        navControll = navHostFragment.navController

        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        homeViewModel.storeSideMenu(this)
        homeViewModel.sendNavController(navControll)
        homeViewModel.sendIndicator(
            binding.viewIndicator,
            binding.viewHalalIndicator,
            binding.viewDirectoryIndicator,
            binding.viewFAQIndicator
        )

        homeViewModel.addOnDestination(
            this,
            binding.viewCover,
            binding.txtActionTitle,
            binding.imgAHLogo,
            binding.relToolbar,
            binding.lnrBottomView,
            binding.imgLogo,
            binding.edtAHSearch,
            binding.imgCloseIcon,
            binding.crdSearchView
        )

        binding.drawerHome.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        binding.homeVM = homeViewModel

//        checkLocationEnableOrNot()

    }
    private fun checkLocationEnableOrNot() {
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 30 * 1000
        locationRequest.fastestInterval = 5 * 1000

        val builder = LocationSettingsRequest.Builder()
        builder.setAlwaysShow(true)
        builder.addLocationRequest(locationRequest)

        val client = LocationServices.getSettingsClient(this)
        val task = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener(this,
            object : OnSuccessListener<LocationSettingsResponse> {
                override fun onSuccess(p0: LocationSettingsResponse?) {

                }
            })
        task.addOnFailureListener(this, object : OnFailureListener {
            override fun onFailure(p0: Exception) {
                try {
                    // Show the dialog by calling startResolutionForResult(),  and check the result in onActivityResult().
                    val resolvable = p0 as ResolvableApiException
                    resolvable.startResolutionForResult(
                        this@HomeActivity,
                        12
                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }

        })
    }

    override fun onBackPressed() {
        if (binding.drawerHome.isDrawerOpen(GravityCompat.END)) {
            binding.drawerHome.closeDrawer(GravityCompat.END)
        }else{
            super.onBackPressed()
            homeViewModel.onCustomeBackPress(navControll.currentDestination!!.id,binding.drawerHome)
        }
    }

    override fun onResume() {
        super.onResume()
        homeViewModel.refresh(navControll.currentDestination!!.id)
    }


}