package com.fianz.ui.home.view_pager_adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class ViewPagerAdapter(var listOfFragment: ArrayList<Fragment>, fm: FragmentActivity) :
    FragmentStateAdapter(fm) {
    override fun getItemCount(): Int {
        return listOfFragment.size
    }

    override fun createFragment(position: Int): Fragment {
        if (position >= 0 && position < listOfFragment.size)
            return listOfFragment[position]
        return Fragment()
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }
}