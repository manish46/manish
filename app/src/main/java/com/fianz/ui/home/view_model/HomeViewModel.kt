package com.fianz.ui.home.view_model

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.animation.TranslateAnimation
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.AndroidViewModel
import androidx.navigation.NavController
import com.fianz.R
import com.fianz.model.HomeSideMenuModel
import com.fianz.ui.home.adapter.SideMenuAdapter
import com.fianz.interfaces.SendThreeValueInterface
import com.fianz.ui.about_us.AboutUs
import com.fianz.ui.christchurch.ChristchurchMosqesActivity
import com.fianz.ui.contact_us.ContactUs
import com.fianz.ui.home.HomeActivity
import com.fianz.utils.CommonUtils

@SuppressLint("StaticFieldLeak")
class HomeViewModel(application: Application) : AndroidViewModel(application) {

    private lateinit var homeSideMenuList: ArrayList<HomeSideMenuModel>
    private lateinit var sideMenuAdapter: SideMenuAdapter
    var sideMenuFlag = true
    var previousAnim = R.id.startPrayerIndicator

    private lateinit var motion: MotionLayout
    private lateinit var crdView: CardView

    private lateinit var viewIndicator: View
    private lateinit var viewHalalIndicator: View
    private lateinit var viewDirectoryIndicator: View
    private lateinit var viewFAQIndicator: View
    private lateinit var navController: NavController
    private var preSideMenuPos = 0
    var prevDestid = R.id.fragPrayer

    fun addOnDestination(
        context: Context,
        viewCover: View,
        txtActionTitle: TextView,
        imgAHLogo: ImageView,
        relToolbar: ConstraintLayout,
        lnrBottomView: LinearLayout,
        imgLogo: ImageView,
        edtAHSearch: EditText,
        imgCloseIcon: ImageView,
        crdSearchView: CardView
    ) {
        navController.addOnDestinationChangedListener { _, destination, _ ->
            prevDestid = destination.id
            allViewRefresh(
                context,
                relToolbar,
                lnrBottomView,
                imgLogo,
                imgAHLogo,
                txtActionTitle,
                edtAHSearch,
                imgCloseIcon,
                crdSearchView
            )

            when (destination.id) {
                R.id.fragPrayer -> {
                    txtActionTitle.text = context.getString(R.string.prayerTiming)
                    txtActionTitle.setTextColor(ContextCompat.getColor(context, R.color.white))
                    viewCover.alpha = 0F
                    viewIndicator.setBackgroundColor(
                        ContextCompat.getColor(
                            context,
                            R.color.dark_green
                        )
                    )
                    crdSearchView.visibility = View.GONE
                }
                R.id.fragHalalList -> {
                    txtActionTitle.text = context.getString(R.string.halal)
                    txtActionTitle.setTextColor(ContextCompat.getColor(context, R.color.black))
                    viewCover.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
                    viewCover.alpha = 1F
                    viewHalalIndicator.setBackgroundColor(
                        ContextCompat.getColor(
                            context,
                            R.color.dark_green
                        )
                    )
                }
                R.id.fragDirectory -> {
                    txtActionTitle.text =
                        context.getString(R.string.directory)
                    txtActionTitle.setTextColor(context.getColor(R.color.black))
                    viewCover.setBackgroundColor(context.getColor(R.color.white))
                    viewCover.alpha = 1F

                    viewDirectoryIndicator.setBackgroundColor(
                        ContextCompat.getColor(
                            context,
                            R.color.dark_green
                        )
                    )

                }
                R.id.fragFaq -> {
                    txtActionTitle.text = context.getString(R.string.faq_s)
                    txtActionTitle.setTextColor(context.getColor(R.color.black))
                    viewCover.setBackgroundColor(context.getColor(R.color.white))
                    viewCover.alpha = 1F
                    viewFAQIndicator.setBackgroundColor(
                        ContextCompat.getColor(
                            context,
                            R.color.dark_green
                        )
                    )
                }

                R.id.fragHalalDetail -> {
                    relToolbar.visibility = View.GONE
                    lnrBottomView.visibility = View.GONE
                    imgLogo.visibility = View.GONE
                }
                R.id.fragNewsList -> {
                    txtActionTitle.text = context.getString(R.string.news)
                    txtActionTitle.setTextColor(context.getColor(R.color.black))
                    viewCover.setBackgroundColor(context.getColor(R.color.white))
                    viewCover.alpha = 0F

                    imgAHLogo.visibility = View.VISIBLE
                    txtActionTitle.visibility = View.GONE

                    homeSideMenuList.forEachIndexed { index, homeSideMenuModel ->
                        if (homeSideMenuModel.clickFlag) {
                            preSideMenuPos = index
                        }
                    }
                    sideMenuAdapter.refreshAdapter(preSideMenuPos, 1)
                    preSideMenuPos = 1
                }
            }
        }
    }

    private fun allViewRefresh(
        context: Context,
        relToolbar: ConstraintLayout,
        lnrBottomView: LinearLayout,
        imgLogo: ImageView,
        imgAHLogo: ImageView,
        txtActionTitle: TextView,
        edtAHSearch: EditText,
        imgCloseIcon: ImageView,
        crdSearchView: CardView
    ) {
        relToolbar.visibility = View.VISIBLE
        lnrBottomView.visibility = View.VISIBLE
        imgLogo.visibility = View.VISIBLE
        txtActionTitle.visibility = View.VISIBLE
        imgAHLogo.visibility = View.GONE
        edtAHSearch.visibility = View.GONE
        imgCloseIcon.visibility = View.GONE
        crdSearchView.visibility = View.VISIBLE

        allRefresh(context)
    }

    private fun checkVisibity(lnrBottomView: LinearLayout) {
        if (lnrBottomView.visibility == View.GONE) {
            val transAnim = TranslateAnimation(0f, 0f, 0f, 60f)
            transAnim.duration = 300
            transAnim.fillAfter = true
            lnrBottomView.startAnimation(transAnim)
            lnrBottomView.visibility = View.VISIBLE
        }
    }

    fun clickOnPrayerTab(
        value: Int,
        v: View
    ) {
        when (value) {
            0 -> setNav(R.id.fragPrayer)
            1 -> setNav(R.id.fragHalalList)
            2 -> setNav(R.id.fragDirectory)
            3 -> setNav(R.id.fragFaq)
            4 -> setNav(R.id.fragNewsList)
        }
    }

    private fun setNav(fragId: Int) {
        if (prevDestid != fragId) {
            allRefresh(viewIndicator.context)
            if (fragId == R.id.fragPrayer)
                navController.popBackStack(R.id.fragPrayer, false)
            else if (fragId != 0) navController.navigate(fragId)
        }
    }

    private fun allRefresh(context: Context) {
        viewIndicator.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
        viewHalalIndicator.setBackgroundColor(
            ContextCompat.getColor(
                context,
                R.color.white
            )
        )
        viewDirectoryIndicator.setBackgroundColor(
            ContextCompat.getColor(
                context,
                R.color.white
            )
        )
        viewFAQIndicator.setBackgroundColor(
            ContextCompat.getColor(
                context,
                R.color.white
            )
        )
        sideMenuAdapter.allFalse(preSideMenuPos, 0)
    }
    
    fun storeSideMenu(context: Context) {
        homeSideMenuList = arrayListOf()

        sideMenuAdapter = SideMenuAdapter()

        homeSideMenuList.add(
            HomeSideMenuModel(
                context.getString(R.string.about_us),
                View.GONE,
                false
            )
        )

        /*homeSideMenuList.add(
            HomeSideMenuModel(
                context.getString(R.string.FIANZ),
                View.VISIBLE,
                false
            )
        )*/
        homeSideMenuList.add(
            HomeSideMenuModel(
                context.getString(R.string.news),
                View.GONE,
                false
            )
        )

        homeSideMenuList.add(
            HomeSideMenuModel(
                context.getString(R.string.christchurch),
                View.GONE,
                false
            )
        )

        /*homeSideMenuList.add(
            HomeSideMenuModel(
                context.getString(R.string.halalCertification),
                View.GONE, false
            )
        )*/
        homeSideMenuList.add(
            HomeSideMenuModel(
                context.getString(R.string.contactUs),
                View.GONE,
                false
            )
        )

        setDataInSideMenuAdapter(homeSideMenuList)
    }

    fun sendNavController(navController: NavController) {
        this.navController = navController
    }

    private fun setDataInSideMenuAdapter(homeSideMenuList: ArrayList<HomeSideMenuModel>) {
        sideMenuAdapter.addListData(homeSideMenuList, object : SendThreeValueInterface {
            override fun actionFire(position: Any, firstValue: Any, secondValue: Any) {
                sideMenuFlag = false
                preSideMenuPos = firstValue as Int
                sideMenuAdapter.refreshAdapter(preSideMenuPos, secondValue as Int)
                sideMenuAnimation(motion, crdView, crdView)

                when (secondValue) {
                    0 -> {
                        (viewIndicator.context as HomeActivity).startActivity(
                            Intent(
                                viewIndicator.context,
                                AboutUs::class.java
                            )
                        )
                    }
                    1 -> {
                        setNav(R.id.fragNewsList)
                    }
                    2 -> {
                        (viewIndicator.context as HomeActivity).startActivity(
                            Intent(
                                viewIndicator.context,
                                ChristchurchMosqesActivity::class.java
                            )
                        )
                    }
                    3 -> {
                        (viewIndicator.context as HomeActivity).startActivity(
                            Intent(
                                viewIndicator.context,
                                ContactUs::class.java
                            )
                        )
                    }
                }
            }
        })
        sideMenuAdapter.notifyDataSetChanged()
    }

    fun seeSideMenuList(): SideMenuAdapter {
        return sideMenuAdapter
    }

    fun sendIndicator(
        viewIndicator: View,
        viewHalalIndicator: View,
        viewDirectoryIndicator: View,
        viewFAQIndicator: View
    ) {
        this.viewIndicator = viewIndicator
        this.viewHalalIndicator = viewHalalIndicator
        this.viewDirectoryIndicator = viewDirectoryIndicator
        this.viewFAQIndicator = viewFAQIndicator
    }

    fun sideMenuAnimation(homeSideMenuMotion: MotionLayout, crdHomeView: CardView, v: View) {
        motion = homeSideMenuMotion
        crdView = crdHomeView

        if (sideMenuFlag) {
            startMotionAnimAnimation(
                homeSideMenuMotion,
                R.id.startHomeAnim,
                R.id.endHomeAnim,
                crdHomeView,
                20f
            )
            sideMenuFlag = false
        } else {
            startMotionAnimAnimation(
                homeSideMenuMotion,
                R.id.endHomeAnim,
                R.id.startHomeAnim,
                crdHomeView,
                0f
            )
            sideMenuFlag = true
        }
        CommonUtils.hideKeyboard(v)
    }

    /*fun showSearchDialog(viewPager: ViewPager2) {
        if (viewPager.currentItem == 2) {
            val showSearchDialog = SearchFragment()
            showSearchDialog.show((viewPager.context as HomeActivity).supportFragmentManager, "")
        }

        //IN XML
        android:onClick="@{(v)->homeVM.showSearchDialog(navHomeFrag)}"
    }*/

    private fun startMotionAnimAnimation(
        homeSideMenuMotion: MotionLayout,
        startAnim: Int,
        endAnim: Int,
        crdHomeView: CardView,
        value: Float
    ) {
        Handler(Looper.getMainLooper()).postDelayed({
            homeSideMenuMotion.setTransition(startAnim, endAnim)
            homeSideMenuMotion.setTransitionDuration(300)
            homeSideMenuMotion.transitionToEnd()

            homeSideMenuMotion.setTransitionListener(object : MotionLayout.TransitionListener {
                override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {
                    if (value == 20F) {
                        changeRadius(p0!!, crdHomeView, value)
                    }

                }

                override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {}

                override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
                    if (value == 0f) {
                        changeRadius(p0!!, crdHomeView, value)
                    }
                }

                override fun onTransitionTrigger(
                    p0: MotionLayout?,
                    p1: Int,
                    p2: Boolean,
                    p3: Float
                ) {
                }

            })
        }, 1)
    }

    private fun changeRadius(p0: MotionLayout, crdHomeView: CardView, value: Float) {
        val radius = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            value,
            p0.context.resources.displayMetrics
        )
        crdHomeView.radius = radius
    }

    fun onCustomeBackPress(id: Int, drawerHome: DrawerLayout) {
        if (!sideMenuFlag) {
            startMotionAnimAnimation(
                motion,
                R.id.endHomeAnim,
                R.id.startHomeAnim,
                crdView,
                0f
            )
            sideMenuFlag = true
        }
        refresh(id)
    }

    fun refresh(id: Int) {
        homeSideMenuList.forEachIndexed { index, homeSideMenuModel ->
            if (homeSideMenuModel.clickFlag) {
                preSideMenuPos = index
            }
        }

        when (id) {
            R.id.fragNewsList -> {
                sideMenuAdapter.refreshAdapter(preSideMenuPos, 1)
            }
            else -> {
                sideMenuAdapter.allFalse(preSideMenuPos, 0)
            }
        }
    }
}