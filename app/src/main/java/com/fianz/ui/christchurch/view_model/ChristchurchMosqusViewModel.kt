package com.fianz.ui.christchurch.view_model

import android.app.Application
import android.content.Context
import android.content.Intent
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.fianz.R
import com.fianz.api.repositories.ChristchurchMosqesRepository
import com.fianz.interfaces.AllApiResponse
import com.fianz.interfaces.SendSingleValueInterface
import com.fianz.model.get_mosques_list.MosqueListDataItem
import com.fianz.model.get_mosques_list.MosqueListResponse
import com.fianz.ui.christchurch.adapter.ChristchurchMosqusAdapter
import com.fianz.ui.load_pdf.LoadPdfImage
import com.fianz.utils.CommonUtils
import com.fianz.utils.ProgressLoader
import retrofit2.Response
import java.lang.Exception

class ChristchurchMosqusViewModel(application: Application) : AndroidViewModel(application) {
    var christchurchAdapter: ChristchurchMosqusAdapter = ChristchurchMosqusAdapter()

    var onBackPressClick: MutableLiveData<Boolean> = MutableLiveData()

    fun storeDataInModel(context: Context) {
        if (CommonUtils.isOnlinne(context)) {
            ProgressLoader.showLoader(context)
            ChristchurchMosqesRepository(context, object : AllApiResponse {
                override fun sendResponse(response: Response<*>?) {
                    ProgressLoader.closeLoader()
                    try {
                        val getResponse = response?.body() as MosqueListResponse
                        setDataInAdapter(context, getResponse.data!!)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }).getMosquesList(context)
        } else
            CommonUtils.showToast(context, context.getString(R.string.checkInternet))

    }

    private fun setDataInAdapter(context: Context, listOfData: ArrayList<MosqueListDataItem?>) {
        christchurchAdapter.addList(listOfData, object : SendSingleValueInterface {
            override fun sendPosition(position: Any) {
                context.startActivity(
                    Intent(context, LoadPdfImage::class.java).putExtra(
                        "URL",
                        listOfData[position as Int]!!.fileUrl
                    )
                )
            }
        })
        christchurchAdapter.notifyDataSetChanged()
    }

    fun seeList(): ChristchurchMosqusAdapter {
        return christchurchAdapter
    }

    fun clickOnBackPress() {
        onBackPressClick.postValue(true)
    }

}