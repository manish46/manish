package com.fianz.ui.christchurch.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.binder_class.loadWithPlaceHolderImages
import com.fianz.databinding.RowChristchurchMosqesBinding
import com.fianz.interfaces.SendSingleValueInterface
import com.fianz.model.get_mosques_list.MosqueListDataItem
import com.squareup.picasso.Picasso

class ChristchurchMosqusAdapter : RecyclerView.Adapter<ChristchurchMosqusAdapter.ViewHolder>() {
    var listOfData = ArrayList<MosqueListDataItem?>()
    lateinit var sendSingleValueInterface :SendSingleValueInterface

    fun addList(listOfData: ArrayList<MosqueListDataItem?>, sendSingleValueInterface: SendSingleValueInterface) {
        this.listOfData = listOfData
        this.sendSingleValueInterface = sendSingleValueInterface
    }

    class ViewHolder(val binding: RowChristchurchMosqesBinding,var sendSingleValueInterface:SendSingleValueInterface) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: MosqueListDataItem) {
            binding.christchurchM = data
            binding.executePendingBindings()

            if(data.image != null){
                if(data.image.isNotEmpty()){
                    Log.e("listImage",data.image)
                    Picasso.get().load(data.image).placeholder(R.drawable.christ_img).into(binding.imgCMLoad)
                }
            }

            binding.crdRCItem.setOnClickListener {
                sendSingleValueInterface.sendPosition(layoutPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: RowChristchurchMosqesBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_christchurch_mosqes, parent, false
        )

        return ViewHolder(binding,sendSingleValueInterface)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listOfData[position]!!)
    }

    override fun getItemCount(): Int {
        return listOfData.size
    }
}