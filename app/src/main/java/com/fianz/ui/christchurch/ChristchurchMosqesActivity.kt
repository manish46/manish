package com.fianz.ui.christchurch

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.fianz.R
import com.fianz.databinding.ActivityChristchurchMosqesBinding
import com.fianz.ui.christchurch.view_model.ChristchurchMosqusViewModel

class ChristchurchMosqesActivity : AppCompatActivity() {

    lateinit var binding: ActivityChristchurchMosqesBinding
    lateinit var christchurchMosqVM: ChristchurchMosqusViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_christchurch_mosqes)

        init()
        observers()
        setListener()
    }

    fun init() {
        christchurchMosqVM = ViewModelProvider(this).get(ChristchurchMosqusViewModel::class.java)
        christchurchMosqVM.storeDataInModel(this)
        binding.christchurchVM = christchurchMosqVM
    }

    fun observers() {
        christchurchMosqVM.onBackPressClick.observe(this, Observer<Boolean> {
            onBackPressed()
        })
    }

    fun setListener() {

    }
}