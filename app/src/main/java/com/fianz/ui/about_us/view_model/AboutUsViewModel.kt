package com.fianz.ui.about_us.view_model

import android.app.Application
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class AboutUsViewModel(application: Application) : AndroidViewModel(application) {

    var onBackPressObj: MutableLiveData<Boolean> = MutableLiveData()

    fun onBackPressButton() {
        onBackPressObj.postValue(true)
    }
}