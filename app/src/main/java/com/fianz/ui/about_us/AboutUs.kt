package com.fianz.ui.about_us

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.fianz.R
import com.fianz.databinding.ActivityAboutUsBinding
import com.fianz.ui.about_us.view_model.AboutUsViewModel

class AboutUs : AppCompatActivity() {
    lateinit var binding: ActivityAboutUsBinding
    lateinit var aboutUSVM: AboutUsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_about_us)

        init()
        observer()
    }

    fun init() {
        aboutUSVM = ViewModelProvider(this).get(AboutUsViewModel::class.java)
        binding.aboutUsVM = aboutUSVM
    }

    fun observer() {
        aboutUSVM.onBackPressObj.observe(this, object : Observer<Boolean> {
            override fun onChanged(t: Boolean?) {
                onBackPressed()
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

}