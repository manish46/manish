package com.fianz.ui.search_dialog.view_model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.fianz.R
import com.fianz.model.SearchFragModel
import com.fianz.ui.search_dialog.adapter.SearchFragAdapter

class SearchViewModel(application: Application) : AndroidViewModel(application) {

    private var listOfSearch: ArrayList<SearchFragModel> = arrayListOf()
    private var searchFragAdapter: SearchFragAdapter = SearchFragAdapter()
    private var context = application.resources

    fun storeData() {
        listOfSearch.add(
            SearchFragModel(
                R.drawable.wal_through,
                context.getString(R.string.northCenter)
            )
        )
        listOfSearch.add(
            SearchFragModel(
                R.drawable.wal_through,
                context.getString(R.string.northCenter)
            )
        )
        listOfSearch.add(
            SearchFragModel(
                R.drawable.wal_through,
                context.getString(R.string.northCenter)
            )
        )
        listOfSearch.add(
            SearchFragModel(
                R.drawable.wal_through,
                context.getString(R.string.northCenter)
            )
        )
        listOfSearch.add(
            SearchFragModel(
                R.drawable.wal_through,
                context.getString(R.string.northCenter)
            )
        )
        listOfSearch.add(
            SearchFragModel(
                R.drawable.wal_through,
                context.getString(R.string.northCenter)
            )
        )
        listOfSearch.add(
            SearchFragModel(
                R.drawable.wal_through,
                context.getString(R.string.northCenter)
            )
        )

        setDataInAdapter(listOfSearch)
    }

    private fun setDataInAdapter(listOfSearch: ArrayList<SearchFragModel>) {
        searchFragAdapter.addList(listOfSearch)
        searchFragAdapter.notifyDataSetChanged()
    }

    fun seeSearchList(): SearchFragAdapter {
        return searchFragAdapter
    }
}