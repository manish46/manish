package com.fianz.ui.search_dialog.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fianz.R
import com.fianz.databinding.RowSearchListBinding
import com.fianz.model.SearchFragModel

class SearchFragAdapter : RecyclerView.Adapter<SearchFragAdapter.ViewHolder>() {
    private var listOfData = ArrayList<SearchFragModel>()

    fun addList(listOfData: ArrayList<SearchFragModel>) {
        this.listOfData = listOfData
    }

    class ViewHolder(var binding: RowSearchListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(searchFragModel: SearchFragModel) {
            binding.searchFragM = searchFragModel
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: RowSearchListBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_search_list, parent, false
        )

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listOfData[position])
    }

    override fun getItemCount(): Int {
        return listOfData.size
    }
}