package com.fianz.ui.search_dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.fianz.R
import com.fianz.databinding.FragmentSearchBinding
import com.fianz.ui.search_dialog.view_model.SearchViewModel
import com.fianz.utils.CommonUtils

class SearchFragment : DialogFragment(), View.OnClickListener {

    lateinit var binding: FragmentSearchBinding
    private lateinit var searchVM: SearchViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(requireActivity()),
            R.layout.fragment_search,
            container,
            false
        )

        init()
        setListener()
        return binding.root
    }

    private fun init() {
        searchVM =
            ViewModelProvider(requireActivity()).get(SearchViewModel::class.java)

        searchVM.storeData()
        binding.searchVM = searchVM
    }

    private fun setListener() {
        binding.imgClose.setOnClickListener(this)
        binding.crdOk.setOnClickListener(this)
        binding.crdCancel.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgClose, R.id.crdOk, R.id.crdCancel -> {
                dialog?.dismiss()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
        CommonUtils.setWidhtAndHeight(requireActivity(), dialog!!)
    }
}