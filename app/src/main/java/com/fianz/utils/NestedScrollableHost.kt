package com.fianz.utils

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewConfiguration
import android.widget.FrameLayout
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.viewpager2.widget.ViewPager2

class NestedScrollableHost : FrameLayout {
    private var touchSlop = 0
    private var initialX = 0.0f
    private var initialY = 0.0f
    private fun parentViewPager(): ViewPager2? {
        var v: View? = this.getParent() as View?
        while (v != null && v !is ViewPager2) v = v.getParent() as View
        return v as ViewPager2?
    }

    private fun child(): View? {
        return if (this.getChildCount() > 0) this.getChildAt(0) else null
    }

    private fun init() {
        touchSlop = ViewConfiguration.get(this.getContext()).getScaledTouchSlop()
    }

    constructor(@NonNull context: Context?) : super(context!!) {
        init()
    }

    constructor(@NonNull context: Context?, @Nullable attrs: AttributeSet?) : super(
        context!!,
        attrs
    ) {
        init()
    }

    constructor(
        @NonNull context: Context?,
        @Nullable attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context!!, attrs, defStyleAttr) {
        init()
    }

    constructor(
        @NonNull context: Context?,
        @Nullable attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context!!, attrs, defStyleAttr, defStyleRes) {
        init()
    }

    private fun canChildScroll(orientation: Int, delta: Float): Boolean {
        val direction = Math.signum(-delta).toInt()
        val child: View = child() ?: return false
        if (orientation == 0) return child.canScrollHorizontally(direction)
        return if (orientation == 1) child.canScrollVertically(direction) else false
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        handleInterceptTouchEvent(ev)
        return super.onInterceptTouchEvent(ev)
    }

    private fun handleInterceptTouchEvent(ev: MotionEvent) {
        val vp = parentViewPager() ?: return
        val orientation = vp.orientation

        // Early return if child can't scroll in same direction as parent
        if (!canChildScroll(orientation, -1.0f) && !canChildScroll(orientation, 1.0f)) return
        if (ev.getAction() === MotionEvent.ACTION_DOWN) {
            initialX = ev.getX()
            initialY = ev.getY()
            this.getParent().requestDisallowInterceptTouchEvent(true)
        } else if (ev.getAction() === MotionEvent.ACTION_MOVE) {
            val dx: Float = ev.getX() - initialX
            val dy: Float = ev.getY() - initialY
            val isVpHorizontal = orientation == ViewPager2.ORIENTATION_HORIZONTAL

            // assuming ViewPager2 touch-slop is 2x touch-slop of child
            val scaleDx = Math.abs(dx) * if (isVpHorizontal) 0.5f else 1.0f
            val scaleDy = Math.abs(dy) * if (isVpHorizontal) 1.0f else 0.5f
            if (scaleDx > touchSlop || scaleDy > touchSlop) {
                if (isVpHorizontal == scaleDy > scaleDx) {
                    // Gesture is perpendicular, allow all parents to intercept
                    this.getParent().requestDisallowInterceptTouchEvent(false)
                } else {
                    // Gesture is parallel, query child if movement in that direction is possible
                    if (canChildScroll(orientation, if (isVpHorizontal) dx else dy)) {
                        this.getParent().requestDisallowInterceptTouchEvent(true)
                    } else {
                        // Child cannot scroll, allow all parents to intercept
                        this.getParent().requestDisallowInterceptTouchEvent(false)
                    }
                }
            }
        }
    }
}