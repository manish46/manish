package com.fianz.utils

import android.annotation.SuppressLint
import android.app.ActionBar
import android.app.AlertDialog
import android.app.Dialog
import android.content.ClipDescription
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.fianz.R
import com.fianz.api.web_url.ApiKey
import com.fianz.model.WeatherInPrayerTimeModel
import com.fianz.ui.home.HomeActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.lang.Exception
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.chrono.HijrahChronology
import java.time.chrono.HijrahDate
import java.time.chrono.IsoChronology
import java.time.format.DateTimeFormatter
import java.util.*
import androidx.core.content.ContextCompat.startActivity


object CommonUtils {

    fun showBsdFrgInFullScreen(context: Context): Dialog {
        val dialog = BottomSheetDialog(context)
        dialog.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            val parentLayout =
                bottomSheetDialog.findViewById<View>(R.id.design_bottom_sheet)
            parentLayout?.let {
                val behavior = BottomSheetBehavior.from(it)
                val layoutPar = it.layoutParams
                layoutPar.height = WindowManager.LayoutParams.MATCH_PARENT
                it.layoutParams = layoutPar
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
        return dialog
    }

    fun setWidhtAndHeight(context: Context, dialog: Dialog) {
        dialog.window?.setLayout(
            (context.resources.displayMetrics.widthPixels * 0.90).toInt(),
            (context.resources.displayMetrics.heightPixels * 0.80).toInt()
        )
        dialog.show()
    }

    fun setCustomeWidhtAndHeightDialog(
        context: Context,
        dialog: Dialog,
        width: Float,
        height: Float
    ) {
        dialog.window?.setLayout(
            (context.resources.displayMetrics.widthPixels * width).toInt(),
            (context.resources.displayMetrics.heightPixels * height).toInt()
        )
        dialog.show()
    }

    @SuppressLint("HardwareIds")
    fun getDeviceId(context: Context): String {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

    fun getDeviceName(context: Context): String {
        return Settings.Global.getString(context.contentResolver, "device_name")
    }

    fun getTokenInPref(context: Context): String {
        return context.getString(R.string.bearer) + " " + Preference(context).getData(ApiKey.device_token)
    }

    fun getCurrentWeekStartDate(c1: Calendar): String {

        //first day of week
        c1.set(Calendar.DAY_OF_WEEK, 1)

        val year: Int = c1.get(Calendar.YEAR)
        val month: Int = c1.get(Calendar.MONTH) + 1
        val day: Int = c1.get(Calendar.DAY_OF_MONTH)

        return year.toString() + "-" + month + "-" + day
    }

    fun getCurrentWeekLastDate(c1: Calendar): String {
        c1.set(Calendar.DAY_OF_WEEK, 7)

        val year: Int = c1.get(Calendar.YEAR)
        val month: Int = c1.get(Calendar.MONTH) + 1
        val day: Int = c1.get(Calendar.DAY_OF_MONTH)

        return year.toString() + "-" + month + "-" + day
    }

    fun getMonthStartDate(month: Int): String {
        val gc: Calendar = GregorianCalendar()
        gc[Calendar.MONTH] = month
        gc[Calendar.DAY_OF_MONTH] = 1
        val monthStart = gc.time
        gc.add(Calendar.MONTH, 1)
        gc.add(Calendar.DAY_OF_MONTH, -1)
        val format = SimpleDateFormat("yyyy-MM-dd")

        System.out.println("Calculated month start date : " + format.format(monthStart))

        return format.format(monthStart)
    }

    fun getMonthEndDate(month: Int): String {
        val gc: Calendar = GregorianCalendar()
        gc[Calendar.MONTH] = month
        gc[Calendar.DAY_OF_MONTH] = 1
        gc.add(Calendar.MONTH, 1)
        gc.add(Calendar.DAY_OF_MONTH, -1)
        val monthEnd = gc.time
        val format = SimpleDateFormat("yyyy-MM-dd")

        println("Calculated month end date : " + format.format(monthEnd))

        return format.format(monthEnd)
    }

    fun setDateAndMonth(date: String): String {
        val sdfIn = SimpleDateFormat("yyyy-MM-dd")
        val sdfOut = SimpleDateFormat("dd-MMM")
        val input = date
        val setDate = sdfIn.parse(input)

        return sdfOut.format(setDate)
    }

    fun getOnlyCurrentDate(): String {
        val date = Date() // to get the date
        val df = SimpleDateFormat("dd") // getting date in this format
        val formattedDate = df.format(date.time)
        return formattedDate
    }

    fun getFullCurrentDate(): String {
        val date = Date() // to get the date
        val df = SimpleDateFormat("dd-MM-yyyy") // getting date in this format
        val formattedDate = df.format(date.time)
        return formattedDate
    }

    fun setHourWithTimeFormate(time: String): String {
        val sdfIn = SimpleDateFormat("HH:mm:ss")
//        val sdfOut = SimpleDateFormat("HH:mm aaa")
//        val sdfOut = SimpleDateFormat("HH:mm")
        val sdfOut = SimpleDateFormat("hh:mm a")
        val input = time
        val setDate = sdfIn.parse(input)

        return sdfOut.format(setDate)
    }

    fun setOnlyHourFormate(time: String): String {
        val sdfIn = SimpleDateFormat("HH:mm:ss")
//        val sdfOut = SimpleDateFormat("HH:mm aaa")
        val sdfOut = SimpleDateFormat("HH")
        val input = time
        val setDate = sdfIn.parse(input)

        return sdfOut.format(setDate)
    }

    fun isOnlinne(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected
    }

    fun showToast(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun setCurrentHajriDates(): String {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-uuuu")
            val gregorianString = getFullCurrentDate()
            val gregorianDate = LocalDate.parse(gregorianString, dateFormatter)
            val islamicDate: HijrahDate = HijrahDate.from(gregorianDate)
            return islamicDate.toString()
        } else {
            return ""
        }
    }

    fun hideKeyboard(context: View) {
        try {
            val imm =
                context.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(
                context.getApplicationWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getCurrentTime(): String {
        val sdf = SimpleDateFormat("hh:mm a")
        val currentDate = sdf.format(Date())

        return currentDate
    }

    fun openGoogleMap(context: Context, text: String) {
        val map =
            "http://maps.google.co.in/maps?q=${text}"
        val i = Intent(Intent.ACTION_VIEW, Uri.parse(map))
        context.startActivity(i)
    }

    @Throws(ParseException::class)
    fun findNext(current: String?, listOfWeathers: ArrayList<WeatherInPrayerTimeModel>): String? {
        val fmt = SimpleDateFormat("hh:mm a")
        fmt.timeZone = TimeZone.getTimeZone("UTC")
        val currentMillis = fmt.parse(current).time
        var bestMillis: Long = 0
        var minMillis: Long = 0
        var bestTime: String? = null
        var minTime: String? = null
        for (time in listOfWeathers) {
            val millis = fmt.parse(time.time).time
            if (millis >= currentMillis && (bestTime == null || millis < bestMillis)) {
                bestMillis = millis
                bestTime = time.time
            }
            if (minTime == null || millis < minMillis) {
                minMillis = millis
                minTime = time.time
            }
        }
        return bestTime ?: minTime
    }

    fun showAlertPermision(context: Context, dialog: Dialog) {
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.custome_alert_dialog)

        val txtCADSetting: TextView = dialog.findViewById(R.id.txtCADSetting)
        txtCADSetting.setOnClickListener {
            val intent = Intent()
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri: Uri = Uri.fromParts("package", context.packageName, null)
            intent.setData(uri)
            context.startActivity(intent)
            dialog.dismiss()
        }

        dialog.window?.setLayout(
            (context.resources.displayMetrics.widthPixels * 0.90).toInt(),
            ActionBar.LayoutParams.WRAP_CONTENT
        )
        if (!dialog.isShowing) {
            dialog.show()
        }
    }

    fun dissmissDialog(dialog: Dialog) {
        if (dialog != null) {
            dialog.dismiss()
        }
    }

    fun goToGmail(context: Context, emailId: String) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = ClipDescription.MIMETYPE_TEXT_PLAIN
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(emailId))
        intent.putExtra(Intent.EXTRA_SUBJECT, "")
        intent.putExtra(Intent.EXTRA_TEXT, "")
        (context as AppCompatActivity).startActivity(Intent.createChooser(intent, "Send feedback"))

    }
}