package com.fianz.utils

import android.app.ProgressDialog
import android.content.Context
import com.fianz.R

object ProgressLoader {
    lateinit var progressDialog:ProgressDialog

    fun showLoader(context: Context){
        progressDialog = ProgressDialog(context)
        progressDialog.setMessage(context.getString(R.string.pleaseWait))
        progressDialog.setCancelable(false)
        progressDialog.setCanceledOnTouchOutside(false)

        if(!progressDialog.isShowing){
            progressDialog.show()
        }
    }

    fun closeLoader(){
        if(progressDialog.isShowing){
            progressDialog.dismiss()
        }
    }
}