package com.fianz.utils

import android.content.Context
import android.content.SharedPreferences

class Preference(var context: Context) {

    var sharedPreference: SharedPreferences? = null
    var editor: SharedPreferences.Editor? = null

    companion object {
        val MAIN_PREF = "main_pref"
        val MAIN_PREF_INT = 0
        val WALK_THROUGH = "WALK_THROUGH"
        val CURRENT_LATITUTE = "CURRENT_LATITUTE"
        val CURRENT_LOGITUTE = "CURRENT_LOGITUTE"
    }

    init {
        sharedPreference = context.getSharedPreferences(MAIN_PREF, MAIN_PREF_INT)
        editor = sharedPreference?.edit()
    }

    fun setData(key: String, value: String) {
        editor?.putString(key, value)
        editor?.apply()
    }

    fun getData(key: String): String {
        return sharedPreference?.getString(key, "")!!
    }

    fun clearAll() {
        editor?.apply()
        editor?.clear()
    }

    fun setFlag(key: String, value: Boolean) {
        editor?.putBoolean(key, value)
        editor?.apply()
    }

    fun getBoolen(key: String): Boolean {
        return sharedPreference?.getBoolean(key, false)!!
    }
}