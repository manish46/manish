package com.fianz.binder_class

import android.content.Context
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.fianz.R
import com.fianz.ui.home.HomeActivity
import com.fianz.ui.home.view_pager_adapter.ViewPagerAdapter
import com.fianz.ui.news_list.adapter.NewsViewPagerListAdapter
import com.fianz.ui.walk_through.adapter.WalkThroughAdapter
import com.fianz.utils.ZoomOutPageTransformer
import com.fianz.utils.pager_indicator.PageIndicator
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.squareup.picasso.Picasso
import java.time.LocalDate
import java.time.chrono.HijrahDate
import java.time.format.DateTimeFormatter

fun Context.toastMessage(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

/*
@BindingAdapter(value = ["setFragInAdapter", "tabLayout"])
fun ViewPager2.setFragInAdapterWithTabLayout(listOfFragment: ArrayList<Fragment>) {
    this.setPageTransformer(ZoomOutPageTransformer())
    this.adapter = ViewPagerAdapter(listOfFragment, (this.context as HomeActivity))
}
*/

@BindingAdapter("setFragInAdapter")
fun ViewPager2.setFragInAdapter(listOfFragment: ArrayList<Fragment>) {
//    this.setPageTransformer(ZoomOutPageTransformer())
    this.isUserInputEnabled = false
    this.adapter = ViewPagerAdapter(listOfFragment, (this.context as HomeActivity))
}

@BindingAdapter(value = ["setMonthAndYearAdapter", "tabLayout"])
fun ViewPager2.setMonthAndYearAdapter(listOfFragment: ArrayList<Fragment>, tabLayouts: TabLayout) {
    this.setPageTransformer(ZoomOutPageTransformer())
//    this.adapter = MotheAndYearAdapter(listOfFragment, (this.context as HomeActivity))
}

@BindingAdapter("showImage")
fun ImageView.showImage(image: Int) {
    this.setImageResource(image)
}

@BindingAdapter(value = ["setAdapterData", "setWithTabLayout"])
fun ViewPager2.setAdapterData(walkThroughAdapter: WalkThroughAdapter, tabLayout: TabLayout) {
    this.adapter = walkThroughAdapter

    TabLayoutMediator(tabLayout, this) { _, _ -> }.attach()
}

@BindingAdapter("setIndicator")
fun RecyclerView.setIndicator(pageIndicator: PageIndicator) {
    PagerSnapHelper().attachToRecyclerView(this)
    pageIndicator attachTo this
}

@BindingAdapter("changePos")
fun RecyclerView.changePos(pos:Int){
    this.smoothScrollToPosition(pos)
}
/*@BindingAdapter(value = ["setViewPagerAdapter", "viewpagerChangeListner"])
fun ViewPager2.setSwipeListner(motionLayout: MotionLayout) {

   val view = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            if (position == 1) {
                Handler().postDelayed({
                    motionLayout.setTransition(R.id.startPrayerIndicator, R.id.startHalalIndicator)
                    motionLayout.setTransitionDuration(300)
                    motionLayout.transitionToEnd()
                }, 1)
            }
        }
    }
}*/

/*fun TextView.setText() {
    val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-uuuu")
    val gregorianString = "01-08-1994"
    val gregorianDate = LocalDate.parse(gregorianString, dateFormatter)
    val islamicDate: HijrahDate = HijrahDate.from(gregorianDate)

}*/

/*
@BindingAdapter(value = ["sendImages", "sendStrings"])
fun TabLayout.setViewInTabLayout(listOfImg: ArrayList<Int>, listOfText: ArrayList<String>) {

    for (i in 0 until this.getTabCount()) {
        val view = LayoutInflater.from(this.context).inflate(R.layout.tablayout_item, null)
        val txtView: TextView = view?.findViewById(R.id.txtIconName)!!
        val imgIcon: ImageView = view.findViewById(R.id.imgIcon)!!
        val tab: TabLayout.Tab = this.getTabAt(i)!!

        txtView.setText(listOfText.get(i))
        imgIcon.setImageResource(listOfImg.get(i))
        tab.setCustomView(view)
    }
}
*/

@BindingAdapter("loadImages")
fun ImageView.loadImages(imgText: String) {
    if (imgText.isNotEmpty()) {
        Picasso.get().load(imgText).into(this)
    }
}
@BindingAdapter("loadWithPlaceHolderImages")
fun ImageView.loadWithPlaceHolderImages(imgText: String) {
    if (!imgText.equals(null) && imgText.isNotEmpty()) {
        Picasso.get().load(imgText).placeholder(R.drawable.christ_img).into(this)
    }
}