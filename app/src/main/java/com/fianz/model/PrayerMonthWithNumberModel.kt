package com.fianz.model

data class PrayerMonthWithNumberModel(var text:String, var monthNumber:Int, var selectedFlag:Boolean)