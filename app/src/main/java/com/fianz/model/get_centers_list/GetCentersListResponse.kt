package com.fianz.model.get_centers_list

import com.google.gson.annotations.SerializedName

data class GetCenterListResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: GetCentersData? = null,

    @field:SerializedName("success")
    val success: Boolean? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class GetCentersSubregion(

    @field:SerializedName("region_id")
    val regionId: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("title")
    val title: String? = null
)

data class GetCentersRegionsItem(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("subregions")
    val subregions: ArrayList<GetCentersSubregionsItem?>? = null,

    @field:SerializedName("title")
    val title: String? = null,

    var flag: Boolean = false
)

data class GetCentersCentresItem(

    @field:SerializedName("centers")
    val centers: ArrayList<GetCentersCentersItem?>? = null,

    @field:SerializedName("title")
    val title: String? = null,

    var flag: Boolean = false
)

data class GetCentersData(

    @field:SerializedName("regions")
    val regions: ArrayList<GetCentersRegionsItem?>? = null,

    @field:SerializedName("centreTypes")
    val centreTypes: ArrayList<GetCentersCentreTypesItem?>? = null,

    @field:SerializedName("centres")
    val centres: ArrayList<GetCentersCentresItem?>? = null
)

data class GetCentersCentersItem(

    @field:SerializedName("zip")
    val zip: String? = null,

    @field:SerializedName("center_type_id")
    val centerTypeId: String? = null,

    @field:SerializedName("country")
    val country: Any? = null,

    @field:SerializedName("image")
    val image: Any? = null,

    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("contact_person")
    val contactPerson: String? = null,

    @field:SerializedName("subregion")
    val subregion: GetCentersSubregion? = null,

    @field:SerializedName("latitude")
    val latitude: String? = null,

    @field:SerializedName("region_id")
    val regionId: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("deleted_at")
    val deletedAt: Any? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("phone")
    val phone: String? = null,

    @field:SerializedName("subregion_id")
    val subregionId: String? = null,

    @field:SerializedName("street1")
    val street1: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("street2")
    val street2: String? = null,

    @field:SerializedName("state")
    val state: String? = null,

    @field:SerializedName("region")
    val region: GetCentersRegion? = null,

    @field:SerializedName("email")
    val email: String? = null,

    @field:SerializedName("product")
    val product: String? = null,

    @field:SerializedName("longitude")
    val longitude: String? = null
)

data class GetCentersCentreTypesItem(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("image")
    val image: String? = null,

    var flag: Boolean = false
)

data class GetCentersRegion(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("title")
    val title: String? = null
)

data class GetCentersSubregionsItem(

    @field:SerializedName("region_id")
    val regionId: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("title")
    val title: String? = null,

    var flag:Boolean = false
)
