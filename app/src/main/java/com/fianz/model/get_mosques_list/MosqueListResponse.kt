package com.fianz.model.get_mosques_list

import com.google.gson.annotations.SerializedName

data class MosqueListResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: ArrayList<MosqueListDataItem?>? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class MosqueListDataItem(

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("file_url")
	val fileUrl: String? = null,

	@field:SerializedName("sorting")
	val sorting: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("title")
	val title: String? = ""
)
