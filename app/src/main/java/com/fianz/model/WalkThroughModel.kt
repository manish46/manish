package com.fianz.model

data class WalkThroughModel(var img: Int, var titleText: String, var textDescription: String)