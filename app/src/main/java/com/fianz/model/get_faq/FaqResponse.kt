package com.fianz.model.get_faq

import com.google.gson.annotations.SerializedName

data class FaqResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: ArrayList<FaqDataItem?>? = null,

    @field:SerializedName("success")
    val success: Boolean? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class FaqDataItem(

    @field:SerializedName("question")
    val question: String? = null,

    @field:SerializedName("answer")
    val answer: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    var hideFlag: Boolean = false
)
