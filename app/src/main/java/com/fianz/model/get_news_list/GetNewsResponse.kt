package com.fianz.model.get_news_list

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class GetNewsResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: ArrayList<GetNewsDataItem?>? = null,

    @field:SerializedName("success")
    val success: Boolean? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class GetNewsDataItem(

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("sorting")
    val sorting: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("is_featured")
    val isFeatured: String? = null
) : Serializable
