package com.fianz.model

data class TimesInPrayerTimeModel(var name: String, var time: String, var color: Int, var type: Int)