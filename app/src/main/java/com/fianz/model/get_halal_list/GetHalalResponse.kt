package com.fianz.model.get_halal_list

import com.google.gson.annotations.SerializedName

data class GetHalalResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: GetHalalData? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class GetHalalSubregion(

	@field:SerializedName("region_id")
	val regionId: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("title")
	val title: String? = null
)

data class GetHalalRegionsItem(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("subregions")
	val subregions: ArrayList<GetHalalSubregionsItem?>? = null,

	@field:SerializedName("title")
	val title: String? = null,

	var flag:Boolean = false
)

data class GetHalalData(

	@field:SerializedName("regions")
	val regions: ArrayList<GetHalalRegionsItem?>? = null,

	@field:SerializedName("halalTypes")
	val halalTypes: ArrayList<GetHalalHalalTypesItem?>? = null,

	@field:SerializedName("centres")
	val centres: ArrayList<GetHalalCentresItem?>? = null
)

data class GetHalalRegion(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("title")
	val title: String? = null
)

data class GetHalalCentresItem(

	@field:SerializedName("centers")
	val centers: ArrayList<GetHalalCentersItem?>? = null,

	@field:SerializedName("title")
	val title: String? = null,

	var flag:Boolean = false
)

data class GetHalalCentersItem(

	@field:SerializedName("zip")
	val zip: String? = null,

	@field:SerializedName("country")
	val country: Any? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("product")
	val product: Any? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("contact_person")
	val contactPerson: String? = null,

	@field:SerializedName("subregion")
	val subregion: GetHalalSubregion? = null,

	@field:SerializedName("latitude")
	val latitude: String? = null,

	@field:SerializedName("region_id")
	val regionId: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("deleted_at")
	val deletedAt: Any? = null,

	@field:SerializedName("halal_type_id")
	val halalTypeId: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("subregion_id")
	val subregionId: String? = null,

	@field:SerializedName("street1")
	val street1: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("street2")
	val street2: String? = null,

	@field:SerializedName("state")
	val state: String? = null,

	@field:SerializedName("region")
	val region: GetHalalRegion? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("longitude")
	val longitude: String? = null,

)

data class GetHalalHalalTypesItem(

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("description")
	val description: Any? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("title")
	val title: String? = null
)

data class GetHalalSubregionsItem(

	@field:SerializedName("region_id")
	val regionId: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	var flag: Boolean = false
)
