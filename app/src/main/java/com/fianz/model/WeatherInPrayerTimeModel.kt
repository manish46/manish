package com.fianz.model

data class WeatherInPrayerTimeModel(
    var image: Int,
    var name: String,
    var time: String,
    var amPm: String,
    var flag: Boolean
)