package com.fianz.model.prayer_model

data class CentresIdAndTitle(var id: Int, var title: String,var distanceKM:Double, var flag: Boolean = false) {
}