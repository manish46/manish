package com.fianz.model.prayer_model

import com.google.gson.annotations.SerializedName

data class PrayerTimingResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: PrayerTimingData? = null,

    @field:SerializedName("success")
    val success: Boolean? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class PrayerTimingData(

    @field:SerializedName("prayer_times")
    val prayerTimes: ArrayList<PrayerTimesItem?>? = null
)

data class PrayerTimesItem(

    @field:SerializedName("prayer_date")
    val prayerDate: String? = null,

    @field:SerializedName("maghrib_prayer_time")
    val maghribPrayerTime: String? = null,

    @field:SerializedName("asr_prayer_time")
    val asrPrayerTime: String? = null,

    @field:SerializedName("fajr_prayer_time")
    val fajrPrayerTime: String? = null,

    @field:SerializedName("zohr_prayer_time")
    val zohrPrayerTime: String? = null,

    @field:SerializedName("display_prayer_date")
    val displayPrayerDate: String? = null,

    @field:SerializedName("isha_prayer_time")
    val ishaPrayerTime: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("centre_id")
    val centreId: String? = null,

    @field:SerializedName("sunrise_prayer_time")
    val sunrisePrayerTime: String? = null,

    var flag: Boolean = false
)
