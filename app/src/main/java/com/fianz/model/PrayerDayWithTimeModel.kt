package com.fianz.model

data class PrayerDayWithTimeModel(var day:String,var date:String,var listOfTime:ArrayList<TimesInPrayerTimeModel>)