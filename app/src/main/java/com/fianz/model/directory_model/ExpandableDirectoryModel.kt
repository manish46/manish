package com.fianz.model.directory_model

class ExpandableDirectoryModel {
    companion object {
        const val PARENT = 1
        const val CHILD = 2
    }

    lateinit var headerParent: DirectoryHeaderModel
    var type: Int = 0
    lateinit var childParent: DirectoryItemModel
    var isExpanded: Boolean = false

    private var isCloseShow: Boolean = false

    constructor(
        type: Int,
        headerParent: DirectoryHeaderModel,
        isExpanded: Boolean = false,
        isCloseShow: Boolean = false
    ) {
        this.type = type
        this.headerParent = headerParent
        this.isExpanded = isExpanded
        this.isCloseShow = isCloseShow
    }

    constructor(
        type: Int,
        childParent: DirectoryItemModel,
        isExpanded: Boolean = false,
        isCloseShow: Boolean = false
    ) {
        this.type = type
        this.childParent = childParent
        this.isExpanded = isExpanded
        this.isCloseShow = isCloseShow
    }

}