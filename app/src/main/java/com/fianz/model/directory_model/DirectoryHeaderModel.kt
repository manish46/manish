package com.fianz.model.directory_model

data class DirectoryHeaderModel(
    var title: String,
    var listOfDirectoryItem: ArrayList<DirectoryItemModel>,
    var hideShow: Boolean
)