package com.fianz.interfaces

import retrofit2.Response

interface AllApiResponse {
    fun sendResponse(response: Response<*>?)
}