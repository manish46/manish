package com.fianz.api.web_url

object ApiKey {
    const val device_type = "device_type"
    const val device_token = "device_token"
    const val device_id = "device_id"
    const val device_name = "device_name"
    const val authorization = "Authorization"
    const val subregion_id = "subregion_id"
    const val subregion_ids = "subregion_id[]"
    const val s = "s"
    const val center_type_id = "center_type_id"
    const val centre_id = "centre_id"
    const val filter_dates = "filter_dates[][]"
    const val halal_type_id = "halal_type_id"
}