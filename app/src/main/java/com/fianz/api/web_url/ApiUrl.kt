package com.fianz.api.web_url

object ApiUrl {
    const val BASE_URL = "http://fianz.projectcafe.in/api/v1/"
    const val ADD_DEVICE_INFORMATION = "devices/add"
    const val FAQ_LIST = "faq/list"
    const val CENTERS_LIST = "centres/list"
    const val HALAL_LIST = "halals/list"
    const val PRAYER_LIST = "prayer_times/list"
    const val NEWS_LIST = "blog/list"
    const val MOSQUES_LIST = "mosques/list"
}