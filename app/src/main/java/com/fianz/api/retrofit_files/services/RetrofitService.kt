package com.fianz.api.retrofit_files.services

import com.fianz.api.web_url.ApiKey
import com.fianz.api.web_url.ApiUrl
import com.fianz.model.get_centers_list.GetCenterListResponse
import com.fianz.model.get_centers_list.GetCentersCentreTypesItem
import com.fianz.model.get_centers_list.GetCentersCentresItem
import com.fianz.model.get_device_information.SplashActivityModel
import com.fianz.model.get_faq.FaqResponse
import com.fianz.model.get_halal_list.GetHalalResponse
import com.fianz.model.get_mosques_list.MosqueListResponse
import com.fianz.model.get_news_list.GetNewsResponse
import com.fianz.model.prayer_model.PrayerTimingResponse
import com.google.gson.JsonObject
import retrofit2.http.*

interface RetrofitService {
    @POST(ApiUrl.ADD_DEVICE_INFORMATION)
    fun getAddDeviceInformation(
        @Header(ApiKey.authorization) token: String,
        @Body device_type: JsonObject,
    ): retrofit2.Call<SplashActivityModel>

    @GET(ApiUrl.FAQ_LIST)
    fun getFaqList(@Header(ApiKey.authorization) token: String): retrofit2.Call<FaqResponse>

    @GET(ApiUrl.CENTERS_LIST)
    fun getCenterListData(@Header(ApiKey.authorization) token: String): retrofit2.Call<GetCenterListResponse>

    @GET(ApiUrl.CENTERS_LIST)
    fun getCenterTypeListData(
        @Header(ApiKey.authorization) token: String,
        @Query(ApiKey.center_type_id) center_type_id: Int
    ): retrofit2.Call<GetCenterListResponse>

    @GET(ApiUrl.CENTERS_LIST)
    fun getFilterCenterList(
        @Header(ApiKey.authorization) token: String,
        @Query(ApiKey.subregion_ids) subId: ArrayList<Int>,
        @Query(ApiKey.center_type_id) center_type_id: String
    ): retrofit2.Call<GetCenterListResponse>

    @GET(ApiUrl.CENTERS_LIST)
    fun getSearchData(
        @Header(ApiKey.authorization) token: String,
        @Query(ApiKey.s) s: String
    ): retrofit2.Call<GetCenterListResponse>

    @GET(ApiUrl.HALAL_LIST)
    fun getHalalList(@Header(ApiKey.authorization) token: String): retrofit2.Call<GetHalalResponse>

    @GET(ApiUrl.HALAL_LIST)
    fun getHalalDetailList(
        @Header(ApiKey.authorization) token: String,
        @Query(ApiKey.halal_type_id) halal_type_id: String
    ): retrofit2.Call<GetHalalResponse>

    @GET(ApiUrl.HALAL_LIST)
    fun getHalalDetailSearchList(
        @Header(ApiKey.authorization) token: String,
        @Query(ApiKey.halal_type_id) halal_type_id: String,
        @Query(ApiKey.s) seachText: String
    ): retrofit2.Call<GetHalalResponse>

    @GET(ApiUrl.HALAL_LIST)
    fun getHalalFilterList(
        @Header(ApiKey.authorization) token: String,
        @Query(ApiKey.halal_type_id) halalToken: String,
        @Query(ApiKey.subregion_ids) subRegionId: ArrayList<Int>
    ): retrofit2.Call<GetHalalResponse>

    @GET(ApiUrl.CENTERS_LIST)
    fun getCentreTypeList(
        @Header(ApiKey.authorization) token: String,
        @Query(ApiKey.center_type_id) center_type_id: String
    ): retrofit2.Call<GetCenterListResponse>

    /*@GET(ApiUrl.PRAYER_LIST)
    fun getPrayerTime(
        @Header(ApiKey.authorization) token: String,
        @Query(ApiKey.centre_id) centre_id: String,
        @Query(ApiKey.filter_dates)filter_dates: ArrayList<ArrayList<String>>
    ):retrofit2.Call<PrayerTimingResponse>*/
    @GET(ApiUrl.PRAYER_LIST)
    fun getPrayerTime(
        @Header(ApiKey.authorization) token: String,
        @QueryMap map: HashMap<String, String>
    ): retrofit2.Call<PrayerTimingResponse>

    @GET(ApiUrl.NEWS_LIST)
    fun getNewsList(@Header(ApiKey.authorization) token: String): retrofit2.Call<GetNewsResponse>

    @GET(ApiUrl.MOSQUES_LIST)
    fun getMosquesList(@Header(ApiKey.authorization) token: String): retrofit2.Call<MosqueListResponse>
}