package com.fianz.api.retrofit_files.client

import com.fianz.api.retrofit_files.services.RetrofitService
import com.fianz.api.web_url.ApiUrl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {
    val retrofit =
        Retrofit.Builder().baseUrl(ApiUrl.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
//            .client(okHttps)
            .build()
            .create(RetrofitService::class.java)
}