package com.fianz.api.repositories

import android.content.Context
import com.fianz.api.retrofit_files.client.RetrofitClient
import com.fianz.interfaces.AllApiResponse
import com.fianz.model.get_halal_list.GetHalalResponse
import com.fianz.utils.CommonUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HalalListRepository(var context: Context, var allApiResponse: AllApiResponse) {

    fun callHalalList() {
        RetrofitClient.retrofit.getHalalList(CommonUtils.getTokenInPref(context)).enqueue(object :
            Callback<GetHalalResponse> {
            override fun onResponse(
                call: Call<GetHalalResponse>,
                response: Response<GetHalalResponse>
            ) {
                allApiResponse.sendResponse(response)
            }

            override fun onFailure(call: Call<GetHalalResponse>, t: Throwable) {
                allApiResponse.sendResponse(null)
            }
        })
    }
}