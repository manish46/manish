package com.fianz.api.repositories

import android.content.Context
import com.fianz.R
import com.fianz.api.retrofit_files.client.RetrofitClient
import com.fianz.api.web_url.ApiKey
import com.fianz.interfaces.AllApiResponse
import com.fianz.model.get_faq.FaqResponse
import com.fianz.utils.CommonUtils
import com.fianz.utils.Preference
import com.fianz.utils.ProgressLoader
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FaqRepository(var context: Context, var allApiResponse: AllApiResponse) {

    fun getFaq() {
        val token =
            CommonUtils.getTokenInPref(context)

        RetrofitClient.retrofit.getFaqList(token).enqueue(object : Callback<FaqResponse> {
            override fun onResponse(call: Call<FaqResponse>, response: Response<FaqResponse>) {
                ProgressLoader.closeLoader()
                allApiResponse.sendResponse(response)
            }

            override fun onFailure(call: Call<FaqResponse>, t: Throwable) {
                ProgressLoader.closeLoader()
                allApiResponse.sendResponse(null)
            }

        })
    }
}