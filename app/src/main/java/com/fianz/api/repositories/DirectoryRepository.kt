package com.fianz.api.repositories

import android.content.Context
import com.fianz.api.retrofit_files.client.RetrofitClient
import com.fianz.interfaces.AllApiResponse
import com.fianz.model.get_centers_list.GetCenterListResponse
import com.fianz.utils.CommonUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DirectoryRepository(var context: Context, var allApiResponse: AllApiResponse) {

    fun callDirectoryApi() {
        RetrofitClient.retrofit.getCenterListData(CommonUtils.getTokenInPref(context))
            .enqueue(object : Callback<GetCenterListResponse> {
                override fun onResponse(
                    call: Call<GetCenterListResponse>,
                    response: Response<GetCenterListResponse>
                ) {
                    allApiResponse.sendResponse(response)
                }

                override fun onFailure(call: Call<GetCenterListResponse>, t: Throwable) {
                    allApiResponse.sendResponse(null)
                }
            })
    }

    fun callCenterTypeDirectoryApi(id: Int) {
        RetrofitClient.retrofit.getCenterTypeListData(CommonUtils.getTokenInPref(context), id)
            .enqueue(object : Callback<GetCenterListResponse> {
                override fun onResponse(
                    call: Call<GetCenterListResponse>,
                    response: Response<GetCenterListResponse>
                ) {
                    allApiResponse.sendResponse(response)
                }

                override fun onFailure(call: Call<GetCenterListResponse>, t: Throwable) {
                    allApiResponse.sendResponse(null)
                }
            })
    }

    fun callFilterTypeDirectoryApi(ids: ArrayList<Int>, center_type_id: String) {
        RetrofitClient.retrofit.getFilterCenterList(
            CommonUtils.getTokenInPref(context),
            ids,
            center_type_id
        )
            .enqueue(object : Callback<GetCenterListResponse> {
                override fun onResponse(
                    call: Call<GetCenterListResponse>,
                    response: Response<GetCenterListResponse>
                ) {
                    allApiResponse.sendResponse(response)
                }

                override fun onFailure(call: Call<GetCenterListResponse>, t: Throwable) {
                    allApiResponse.sendResponse(null)
                }

            })
    }

    fun callCenterList(text: String) {
        RetrofitClient.retrofit.getSearchData(
            CommonUtils.getTokenInPref(context),
            text
        ).enqueue(object : Callback<GetCenterListResponse> {
                override fun onResponse(
                    call: Call<GetCenterListResponse>,
                    response: Response<GetCenterListResponse>
                ) {
                    allApiResponse.sendResponse(response)
                }

                override fun onFailure(call: Call<GetCenterListResponse>, t: Throwable) {
                    allApiResponse.sendResponse(null)
                }

            })
    }
}