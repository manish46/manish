package com.fianz.api.repositories

import android.content.Context
import com.fianz.api.retrofit_files.client.RetrofitClient
import com.fianz.interfaces.AllApiResponse
import com.fianz.model.get_centers_list.GetCenterListResponse
import com.fianz.model.prayer_model.PrayerTimingResponse
import com.fianz.utils.CommonUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class PrayerTimingRepository(var context: Context, var allApiResponse: AllApiResponse) {
    fun getCentreTypeList() {
        RetrofitClient.retrofit.getCentreTypeList(CommonUtils.getTokenInPref(context), "2")
            .enqueue(object : Callback<GetCenterListResponse> {
                override fun onResponse(
                    call: Call<GetCenterListResponse>,
                    response: Response<GetCenterListResponse>
                ) {
                    allApiResponse.sendResponse(response)
                }

                override fun onFailure(call: Call<GetCenterListResponse>, t: Throwable) {
                    allApiResponse.sendResponse(null)
                }
            })
    }

    fun getPrayerTimingList(filter_dates: HashMap<String, String>) {
        RetrofitClient.retrofit.getPrayerTime(
            CommonUtils.getTokenInPref(context),
            filter_dates
        )
            .enqueue(object : Callback<PrayerTimingResponse> {
                override fun onResponse(
                    call: Call<PrayerTimingResponse>,
                    response: Response<PrayerTimingResponse>
                ) {
                    allApiResponse.sendResponse(response)
                }

                override fun onFailure(call: Call<PrayerTimingResponse>, t: Throwable) {
                    allApiResponse.sendResponse(null)
                }

            })
    }
}