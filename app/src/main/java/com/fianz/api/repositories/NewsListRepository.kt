package com.fianz.api.repositories

import android.content.Context
import com.fianz.api.retrofit_files.client.RetrofitClient
import com.fianz.interfaces.AllApiResponse
import com.fianz.model.get_news_list.GetNewsResponse
import com.fianz.utils.CommonUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewsListRepository(var context: Context, var allApiResponse: AllApiResponse) {

    fun callNewsList(context: Context) {
        RetrofitClient.retrofit.getNewsList(CommonUtils.getTokenInPref(context))
            .enqueue(object : Callback<GetNewsResponse> {
                override fun onResponse(
                    call: Call<GetNewsResponse>,
                    response: Response<GetNewsResponse>
                ) {
                    allApiResponse.sendResponse(response)
                }

                override fun onFailure(call: Call<GetNewsResponse>, t: Throwable) {
                    allApiResponse.sendResponse(null)
                }

            })

    }
}