package com.fianz.api.repositories

import android.content.Context
import com.fianz.api.retrofit_files.client.RetrofitClient
import com.fianz.api.retrofit_files.services.RetrofitService
import com.fianz.interfaces.AllApiResponse
import com.fianz.model.get_mosques_list.MosqueListResponse
import com.fianz.utils.CommonUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ChristchurchMosqesRepository(var context: Context, var allApiResponse: AllApiResponse) {

    fun getMosquesList(context: Context) {
        RetrofitClient.retrofit.getMosquesList(CommonUtils.getTokenInPref(context)).enqueue(object :Callback<MosqueListResponse>{
            override fun onResponse(
                call: Call<MosqueListResponse>,
                response: Response<MosqueListResponse>
            ) {
                allApiResponse.sendResponse(response)
            }

            override fun onFailure(call: Call<MosqueListResponse>, t: Throwable) {
                allApiResponse.sendResponse(null)
            }

        })
    }
}