package com.fianz.api.repositories

import android.content.Context
import com.fianz.api.retrofit_files.client.RetrofitClient
import com.fianz.api.web_url.ApiKey
import com.fianz.interfaces.AllApiResponse
import com.fianz.model.get_halal_list.GetHalalResponse
import com.fianz.utils.CommonUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HalalDetailRepository(var context: Context, var allApiResponse: AllApiResponse) {
    fun callHalalDetailApi(id: String) {
        RetrofitClient.retrofit.getHalalDetailList(CommonUtils.getTokenInPref(context), id)
            .enqueue(object : Callback<GetHalalResponse> {
                override fun onResponse(
                    call: Call<GetHalalResponse>,
                    response: Response<GetHalalResponse>
                ) {
                    allApiResponse.sendResponse(response)
                }

                override fun onFailure(call: Call<GetHalalResponse>, t: Throwable) {
                    allApiResponse.sendResponse(null)
                }

            })
    }

    fun callHalalDetailSearchApi(id: String, text: String) {
        RetrofitClient.retrofit.getHalalDetailSearchList(
            CommonUtils.getTokenInPref(context),
            id,
            text
        )
            .enqueue(object : Callback<GetHalalResponse> {
                override fun onResponse(
                    call: Call<GetHalalResponse>,
                    response: Response<GetHalalResponse>
                ) {
                    allApiResponse.sendResponse(response)
                }

                override fun onFailure(call: Call<GetHalalResponse>, t: Throwable) {
                    allApiResponse.sendResponse(null)
                }

            })
    }

    fun callFilterHalalList(ids: ArrayList<Int>, halal_type_id: String) {
        RetrofitClient.retrofit.getHalalFilterList(
            CommonUtils.getTokenInPref(context),
            halal_type_id,
            ids
        ).enqueue(object : Callback<GetHalalResponse> {
            override fun onResponse(
                call: Call<GetHalalResponse>,
                response: Response<GetHalalResponse>
            ) {
                allApiResponse.sendResponse(response)
            }

            override fun onFailure(call: Call<GetHalalResponse>, t: Throwable) {
                allApiResponse.sendResponse(null)
            }

        })
    }
}