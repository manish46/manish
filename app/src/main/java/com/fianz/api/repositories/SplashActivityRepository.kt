package com.fianz.api.repositories

import android.content.Context
import com.fianz.R
import com.fianz.api.retrofit_files.client.RetrofitClient
import com.fianz.api.web_url.ApiKey
import com.fianz.interfaces.AllApiResponse
import com.fianz.model.get_device_information.SplashActivityModel
import com.fianz.utils.CommonUtils
import com.fianz.utils.Preference
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Response

class SplashActivityRepository(var context: Context, var allApiResponse: AllApiResponse) {

    fun getAddDeviceInformation() {
        val deviceToken = "test10"
        val jsonData = JsonObject()
        jsonData.addProperty(ApiKey.device_id, CommonUtils.getDeviceId(context))
        jsonData.addProperty(ApiKey.device_name, CommonUtils.getDeviceName(context))
        jsonData.addProperty(ApiKey.device_token, deviceToken)
        jsonData.addProperty(ApiKey.device_type, "android")
        val token = context.getString(R.string.bearer) + context.getString(R.string.staticToken)

        val callDevieInfoApi = RetrofitClient.retrofit.getAddDeviceInformation(token, jsonData)
        callDevieInfoApi.enqueue(object : retrofit2.Callback<SplashActivityModel> {
            override fun onResponse(
                call: Call<SplashActivityModel>,
                response: Response<SplashActivityModel>
            ) {
                Preference(context).setData(ApiKey.device_token, deviceToken)
                allApiResponse.sendResponse(response)
            }

            override fun onFailure(call: Call<SplashActivityModel>, t: Throwable) {
                allApiResponse.sendResponse(null)
            }
        })
    }
}